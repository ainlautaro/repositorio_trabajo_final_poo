package modelo;

public class ExcepcionFormatoCorreoIncorrecto extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1384842214500320181L;

	public ExcepcionFormatoCorreoIncorrecto() {
		super();
	}

	@Override
	public String getMessage() {
		String mensaje = "Formato de correo incorrecto, por favor vuelva a ingresar";
		return mensaje;
	}

	
}
