package modelo;

import java.util.ArrayList;
import java.util.List;

public class Ranking {
	private Integer id_ranking;
	private Integer id_usuario;
	private Integer partidas_ganadas;
	private Integer partidas_perdidas;
	private String categoria;
	
	private RankingDAO rkgDao = new RankingDAO();
	
	
	public Ranking(Integer id_ranking, Integer id_usuario, Integer partidas_ganadas, Integer partidas_perdidas,
			String categoria) {
		super();
		this.id_ranking = id_ranking;
		this.id_usuario = id_usuario;
		this.partidas_ganadas = partidas_ganadas;
		this.partidas_perdidas = partidas_perdidas;
		this.categoria = categoria;
	}
	
	
	public Ranking(Integer id_usuario, Integer partidas_ganadas, Integer partidas_perdidas,
			String categoria) {
	
		this.id_usuario = id_usuario;
		this.partidas_ganadas = partidas_ganadas;
		this.partidas_perdidas = partidas_perdidas;
		this.categoria = categoria;
	}
	
	//--------------------------------------------------METODOS-------------------------------------------------------------
	//--------------------------------------------------METODO RANKING ACUMULA PARTIDAS
	public void registrarPartida(Ranking rank) {
		List<Ranking> usuarioRanking = new ArrayList<>();
		usuarioRanking = getRkgDao().getAll();
		int contador = 0;

		for (Ranking rankings : usuarioRanking) {
		if(rankings.getId_usuario().equals(rank.getId_usuario())) {
			if(rankings.getCategoria().equals(rank.getCategoria()) || rankings.getCategoria().equals("No inicializo")) {
				rank.setId_ranking(rankings.getId_ranking());			
				rank.setPartidas_ganadas(rankings.partidas_ganadas+rank.getPartidas_ganadas());
				rank.setPartidas_perdidas(rankings.partidas_perdidas+rank.getPartidas_perdidas());
				rkgDao.update(rank);
				contador++;
			}
		}
	}
		if(contador < 1) {
			rkgDao.save(rank);
		}
	}
	
	
	//-------------------GETTER Y SETTER-----------------------------------------
	public Integer getId_ranking() {
		return id_ranking;
	}
	public void setId_ranking(Integer id_ranking) {
		this.id_ranking = id_ranking;
	}
	public Integer getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}
	public Integer getPartidas_ganadas() {
		return partidas_ganadas;
	}
	public void setPartidas_ganadas(Integer partidas_ganadas) {
		this.partidas_ganadas = partidas_ganadas;
	}
	public Integer getPartidas_perdidas() {
		return partidas_perdidas;
	}
	public void setPartidas_perdidas(Integer partidas_perdidas) {
		this.partidas_perdidas = partidas_perdidas;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}


	public RankingDAO getRkgDao() {
		return rkgDao;
	}


	public void setRkgDao(RankingDAO rkgDao) {
		this.rkgDao = rkgDao;
	}
	
	
	
}
