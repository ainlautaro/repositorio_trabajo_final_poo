package modelo;

public class UsuarioSesion{
	
	private static Usuario sesionUsuario;

	public void sesionIniciada(Usuario usuario) {
		sesionUsuario = usuario;
	}
	
	public static Usuario getInstance() {
		return sesionUsuario != null? sesionUsuario: new Usuario();
	}
	
}
