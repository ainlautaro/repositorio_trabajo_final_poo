package modelo;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

public class PreguntaDAO implements Dao<Pregunta>{
	
	private Database db = Database.getInstance();	//llamamos al metodo de la clase que establece la conexion

	@Override
	public Optional<Pregunta> get(Integer id) {
		Pregunta preg = null;
		ResultSet rs = getDb().query("SELECT * FROM pregunta WHERE pregunta.id=" + id + " LIMIT 1");
		
		try {
			while (rs.next()) {
				String pregunta = rs.getString(2);
				String categoria = rs.getString(3);
				Boolean validada =  rs.getBoolean(4);
				
				preg = new Pregunta(id, pregunta, categoria, validada);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return Optional.ofNullable(preg);
	}

	@Override
	public ArrayList<Pregunta> getAll() {
		ArrayList<Pregunta> preguntas = new ArrayList<>();
		ResultSet rs = getDb().query("SELECT * FROM pregunta");

		try {
			while (rs.next()) {
				Integer id = rs.getInt(1);
				String pregunta = rs.getString(2);
				String categoria = rs.getString(3);
				Boolean validada =  rs.getBoolean(4);
				
				Pregunta preg = new Pregunta(id, pregunta, categoria, validada);
				preguntas.add(preg);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return preguntas;
	}
	
	public ArrayList<Pregunta> getAllPorCategoria(String categ) {
		ArrayList<Pregunta> preguntas = new ArrayList<>();
		ResultSet rs;
		
		if(categ != "Random") {
		      rs = getDb().query("SELECT *, random() FROM pregunta WHERE pregunta.categoria='"+categ+"' and pregunta.validada = true ORDER BY random() LIMIT 10");
		    }else {
		      rs = getDb().query("SELECT *, random() FROM pregunta WHERE pregunta.validada = true ORDER BY random() LIMIT 10");
		    }
		try {
			while (rs.next()) {
				Integer id = rs.getInt(1);
				String pregunta = rs.getString(2);
				Boolean validada =  rs.getBoolean(4);
				
				Pregunta preg = new Pregunta(id, pregunta, categ, validada);
				preguntas.add(preg);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return preguntas;
}
	
	public ArrayList<Pregunta> getAllPorValidada(Boolean validada){
		ArrayList<Pregunta> preguntas = new ArrayList<>();
		ResultSet rs;
	    rs = getDb().query("SELECT * FROM pregunta WHERE pregunta.validada="+validada+" ORDER BY pregunta.categoria");
	    try {
	    	while (rs.next()) {
	    		Integer id = rs.getInt(1);
	    		String pregunta = rs.getString(2);
	    		String categoria =  rs.getString(3);
			
	    		Pregunta preg = new Pregunta(id, pregunta, categoria, validada);
	    		preguntas.add(preg);
	    	}
	    } catch (SQLException e) {
	    	e.printStackTrace();
	    }

	    return preguntas;
	}
	
	@Override
	public void save(Pregunta t) {
		getDb().query("INSERT INTO public.pregunta (desarrollo_pregunta,categoria,validada) VALUES (" + "'" 
											+ t.getDesarrolloPregunta() + "'::character varying, '" 
											+ t.getCategoria() + "'::character varying, " 
											+ t.getValidada()  + "::boolean)"
											+ "returning id;");
	}

	@Override
	public void update(Pregunta t) {

		getDb().insertar(("UPDATE public.pregunta\r\n"
				     + "  SET desarrollo_pregunta= '" + t.getDesarrolloPregunta() + "'"
				     + ", categoria= '"  + t.getCategoria() + "'"
				     + ", validada= "   + t.getValidada() +"\r\n"
				     + "   WHERE id = " + t.getId_pregunta() + " ;"
				));
	}

	@Override
	public void delete(Pregunta t) {
		getDb().insertar("DELETE FROM public.pregunta WHERE id IN ("+ t.getId_pregunta() + ");");
	}
	
	public Integer maxID() {
		Integer id = 1;
		ResultSet rs = getDb().query("SELECT id FROM public.pregunta ORDER BY id DESC LIMIT 1;");
		
			try {
				if (rs.next()) {
					id = rs.getInt(1);
				};
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return id;
	}
	
	public Integer maxIdModoJuego(String modoJuego) {
		Integer id = 1;
		ResultSet rs = getDb().query("SELECT id FROM pregunta WHERE pregunta.categoria='" + modoJuego + "' ORDER BY id DESC LIMIT 1;");
		
			try {
				if (rs.next()) {
					id = rs.getInt(1);
				};
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return id;
	}

	public Database getDb() {
		return db;
	}

	public void setDb(Database db) {
		this.db = db;
	}
	
	
}
