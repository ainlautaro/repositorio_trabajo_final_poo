package modelo;

public class Pregunta {
	
	private Integer id_pregunta;
	private String desarrolloPregunta;
	private String categoria;
	private Boolean validada;

	public Pregunta(Integer id_pregunta, String desarrolloPregunta, String categoria, Boolean validada) {
		super();
		this.setId_pregunta(id_pregunta);
		this.desarrolloPregunta = desarrolloPregunta;
		this.categoria = categoria;
		this.validada = validada;
	}

	public String getDesarrolloPregunta() {
		return desarrolloPregunta;
	}

	public void setDesarrolloPregunta(String desarrolloPregunta) {
		this.desarrolloPregunta = desarrolloPregunta;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public Boolean getValidada() {
		return validada;
	}

	public void setValidada(Boolean validada) {
		this.validada = validada;
	}

	public Integer getId_pregunta() {
		return id_pregunta;
	}

	public void setId_pregunta(Integer id_pregunta) {
		this.id_pregunta = id_pregunta;
	}

}
