package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class Database {
	
	private Connection conexion;
	private static Database db = null;
	private final String USER = "postgres";
	private final String PASS = "112358"; //root
	private final String DBNAME = "Preguntados2020";
	private final String PORT = "5432";
	private final String HOST = "localhost";
	
	private Database() {
		try {
			this.setConexion(DriverManager.getConnection("jdbc:postgresql://" + HOST + ":" + PORT + "/" + DBNAME, USER, PASS));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static Database getInstance() {
		return db != null? db: new Database();
	}
	
	public ResultSet query(String query) {
		ResultSet rs = null;
		
		try {
			rs = getConexion().createStatement().executeQuery(query);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}
	
	public int insertar(String query) {
		int resultado = 0;	
		try {
			resultado = conexion.prepareStatement(query).executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultado;
	}
	
	public void jasper(String path) {
		try {
			JasperReport report = (JasperReport) JRLoader.loadObjectFromFile(path);
			JasperPrint reportFilled = JasperFillManager.fillReport(report, null, getConexion());
			JasperViewer.viewReport(reportFilled, false);
		} catch (JRException e) {
			e.printStackTrace();
		} 
	}
	
	public void cerrarConexion() {
		try { 
			conexion.close(); 
		} catch (SQLException e) { 
			e.printStackTrace(); 
		} 
	}
	
	private Connection getConexion() {
		return conexion;
	}

	private void setConexion(Connection conexion) {
		this.conexion = conexion;
	}
	

}
