package modelo;

import java.util.ArrayList;

public class UsuarioRanking {
	private String nombre;
	private Integer partidas_ganadas;
	private Integer partidas_perdidas;
	private String modo_juego;
	private Integer ranking;
	private RankingDAO rkgDao = new RankingDAO();
	
	public UsuarioRanking(String nombre, Integer partidas_ganadas, Integer partidas_perdidas, String modo_juego,
			Integer ranking) {
		super();
		this.nombre = nombre;
		this.partidas_ganadas = partidas_ganadas;
		this.partidas_perdidas = partidas_perdidas;
		this.modo_juego = modo_juego;
		this.ranking = ranking;
	}
	
	public UsuarioRanking(String nombre) {
		this.nombre = nombre;
	}
	
	
	public UsuarioRanking() {	
	}
	
	
	//--------------------------------------------METODOS
	public ArrayList<UsuarioRanking> posicionRanking(String nombre_usr) {
		
	ArrayList<UsuarioRanking> UsuarioRanking = new ArrayList<>();
	ArrayList<UsuarioRanking> rankingGeneral = new ArrayList<>();
	
	rankingGeneral = getRkgDao().getAllRanking();

	for (UsuarioRanking rankings : rankingGeneral) {
		if(rankings.getNombre().equals(nombre_usr)) {
			UsuarioRanking.add(rankings);
		}
	}
	
	return UsuarioRanking;
}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getPartidas_ganadas() {
		return partidas_ganadas;
	}
	public void setPartidas_ganadas(Integer partidas_ganadas) {
		this.partidas_ganadas = partidas_ganadas;
	}
	public Integer getPartidas_perdidas() {
		return partidas_perdidas;
	}
	public void setPartidas_perdidas(Integer partidas_perdidas) {
		this.partidas_perdidas = partidas_perdidas;
	}
	public String getModo_juego() {
		return modo_juego;
	}
	public void setModo_juego(String modo_juego) {
		this.modo_juego = modo_juego;
	}
	public Integer getRanking() {
		return ranking;
	}
	public void setRanking(Integer ranking) {
		this.ranking = ranking;
	}

	public RankingDAO getRkgDao() {
		return rkgDao;
	}

	public void setRkgDao(RankingDAO rkgDao) {
		this.rkgDao = rkgDao;
	}
	
}
