package modelo;

public class ExcepcionUsuarioYaRegistrado extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3004035559215547247L;
	public ExcepcionUsuarioYaRegistrado() {
		super();
	}
	@Override
	public String getMessage() {
		return "Ya Existe";
	}
}
