package modelo;

public class ExcepcionUsuarioNoRegistrado extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1369860243296562968L;
	public ExcepcionUsuarioNoRegistrado() {
		super();
	}
	@Override
	public String getMessage() {
		String msj = "Usuario no registrado";
		return msj;
	}
}