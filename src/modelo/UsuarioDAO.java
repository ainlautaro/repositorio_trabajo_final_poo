package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsuarioDAO implements Dao<Usuario>{

	private Database db = Database.getInstance();
	
	@Override
	public Optional<Usuario> get(Integer id) {
		Usuario usr = null;
		ResultSet rs = getDb().query("SELECT * FROM usuario WHERE usuario.id=" + id + " LIMIT 1");
		
		try {
			while (rs.next()) {
				String nombre = rs.getString(2);
				String password =  rs.getString(3);
				String email =  rs.getString(4);
				String fotoPerfil =  rs.getString(5);
				Boolean admin = rs.getBoolean(6);
				
				usr = new Usuario(id, nombre, password, email, fotoPerfil, admin);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return Optional.ofNullable(usr);
	}

	@Override
	public List<Usuario> getAll() {
		List<Usuario> usuarios = new ArrayList<>();
		ResultSet rs = getDb().query("SELECT * FROM usuario");

		try {
			while (rs.next()) {
				Integer id = rs.getInt(1);
				String nombre = rs.getString(2);
				String password =  rs.getString(3);
				String email =  rs.getString(4);
				String fotoPerfil =  rs.getString(5);
				Boolean admin = rs.getBoolean(6);
				
				Usuario usr = new Usuario(id, nombre, password, email, fotoPerfil, admin);
				usuarios.add(usr);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return usuarios;
	}


	@Override
	public void save(Usuario t) {
		getDb().query("INSERT INTO public.usuario (nombre,password,email, foto_perfil, admin) VALUES (" + "'" 
									+ t.getNombre()  + "'::character varying, '" 
									+ t.getPassword() + "'::character varying, '"
									+ t.getEmail() + "'::character varying, '"
									+ t.getFotoPerfil() + "'::character varying, '" 
									+ t.getAdmin()  + "'::boolean)"
									+ "returning id;");
	}

	@Override
	public void update(Usuario t) {
		getDb().insertar("UPDATE public.usuario\r\n"
					+ "SET nombre='"  + t.getNombre() 	   + "', "
					+ "password='"    + t.getPassword()    + "', "
					+ "email='" 	  + t.getEmail()	   + "', "
					+ "foto_perfil='" + t.getFotoPerfil()  + "', "
					+ "admin= "       + t.getAdmin() 	   + " \r\n"
					+ "	WHERE id = "  + t.getId_usuario()  + ";");
	}

	@Override
	public void delete(Usuario t) {
		getDb().insertar("DELETE FROM public.usuario WHERE id IN ("+ t.getId_usuario() + ");");
	}
	
	public Integer maxID() {
		Integer id = 1;
		ResultSet rs = getDb().query("SELECT id FROM public.usuario ORDER BY id DESC LIMIT 1;");
		
		
			try {
				if (rs.next()) {
					id = rs.getInt(1);
				};
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return id;
	}

	public Database getDb() {
		return db;
	}

	public void setDb(Database db) {
		this.db = db;
	}

}
