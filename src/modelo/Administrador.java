package modelo;

public class Administrador extends Usuario{
	
	private PreguntaDAO pregDAO = new PreguntaDAO();
	private OpcionPreguntaDAO opcPregDAO = new OpcionPreguntaDAO();

	public Administrador(Integer id_usuario, String nombre, String password, String email, String fotoPerfil, Boolean admin) {
		super(id_usuario, nombre, password, email, fotoPerfil, admin);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void cargarPregunta(Pregunta pregunta, OpcionPregunta op1, OpcionPregunta op2, OpcionPregunta op3, OpcionPregunta op4) {	
		pregunta.setValidada(true);
		pregDAO.save(pregunta);
		opcPregDAO.save(op1);
		opcPregDAO.save(op2);
		opcPregDAO.save(op3);
		opcPregDAO.save(op4);
	}
	
	public void borrarPregunta(Pregunta pregunta, OpcionPregunta op1, OpcionPregunta op2, OpcionPregunta op3, OpcionPregunta op4) {
		pregDAO.delete(pregunta);
		opcPregDAO.delete(op1);
		opcPregDAO.delete(op2);
		opcPregDAO.delete(op3);
		opcPregDAO.delete(op4);
	}
	
	public void actualizarPregunta(Pregunta pregunta, OpcionPregunta op1, OpcionPregunta op2, OpcionPregunta op3, OpcionPregunta op4) {
		
		getPregDAO().update(pregunta);
		
		opcPregDAO.update(op1);
		opcPregDAO.update(op2);
		opcPregDAO.update(op3);
		opcPregDAO.update(op4);
	}
	
	public void cargarUsuario() {
		
	}
	
	public void borrarUsuario() {
		
	}

	public PreguntaDAO getPregDAO() {
		return pregDAO;
	}

	public void setPregDAO(PreguntaDAO pregDAO) {
		this.pregDAO = pregDAO;
	}

	public OpcionPreguntaDAO getOpcPregDAO() {
		return opcPregDAO;
	}

	public void setOpcPregDAO(OpcionPreguntaDAO opcPregDAO) {
		this.opcPregDAO = opcPregDAO;
	}
	
}
