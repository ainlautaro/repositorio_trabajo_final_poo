package modelo;

public class Reportes {
	private Database db = Database.getInstance();
	
	public void reporteJuegosCategoria() {
		//JasperReports.crearReporte("assets\\reporte\\juegosCategoria.jasper");
		getDb().jasper("assets\\reporte\\juegosCategoria.jasper");
	}
	
	public void reportePreguntasPorModo() {
		//JasperReports.crearReporte("assets\\reporte\\cantidadPreguntasPorModo.jasper");
		getDb().jasper("assets\\reporte\\cantidadPreguntasPorModo.jasper");
	}
	
	public void reporteMejoresJugadores() {
		//JasperReports.crearReporte("assets\\reporte\\mejoresJugadores.jasper");
		getDb().jasper("assets\\reporte\\mejoresJugadores.jasper");
	}

	public Database getDb() {
		return db;
	}

	public void setDb(Database db) {
		this.db = db;
	}

	

}
