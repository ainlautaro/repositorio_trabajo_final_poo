package modelo;

import java.util.List;
import java.util.Optional;

public interface Dao<T> {

	public Optional<T> get(Integer id);	//T: devuelve un tipo no definido; Optional: contenedor que puede se null o no
	public List<T> getAll();
	
	public void save(T t);
	public void update(T t);
	public void delete(T t);
	
}
