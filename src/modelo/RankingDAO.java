package modelo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RankingDAO implements Dao<Ranking>{
	private Database db = Database.getInstance();

	@Override
	public Optional<Ranking> get(Integer id) {
		Ranking ran = null;
		ResultSet rs = getDb().query("SELECT * FROM usuario WHERE ranking.id=" + id + " LIMIT 1");		
		
		try {
			while (rs.next()) {
				Integer id_usuario = rs.getInt(2);
				Integer partidas_ganadas =  rs.getInt(3);
				Integer partidas_perdidas =  rs.getInt(4);
				String modo_juego =  rs.getString(5);			
				ran = new Ranking(id, id_usuario, partidas_ganadas, partidas_perdidas, modo_juego);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		
		return Optional.ofNullable(ran);
	}


	@Override
	public List<Ranking> getAll() {
		List<Ranking> ranking = new ArrayList<>();
		ResultSet rs = getDb().query("SELECT * FROM ranking");

		try {
			while (rs.next()) {
				Integer id = rs.getInt(1);
				Integer id_usuario = rs.getInt(2);
				Integer partidas_ganadas =  rs.getInt(3);
				Integer partidas_perdidas =  rs.getInt(4);
				String modo_juego =  rs.getString(5);	
				
				Ranking ran = new Ranking(id, id_usuario, partidas_ganadas, partidas_perdidas, modo_juego);
				ranking.add(ran);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ranking;
	}
	
	public ArrayList<UsuarioRanking> getAllRanking() {
		ArrayList<UsuarioRanking> usuarioRanking = new ArrayList<>();
		ResultSet rs = getDb().query("SELECT u.nombre, r.partidas_ganadas,  r.partidas_perdidas, r.modo_juego, "
								  + " RANK () OVER ( PARTITION BY r.modo_juego ORDER BY r.partidas_ganadas DESC) ranking " 
								  + " FROM ranking as r INNER JOIN usuario as u ON r.usuario_id = u.id ");

		try {
			while (rs.next()) {
				String  nombre 				= rs.getString(1);
				Integer partidas_ganadas 	= rs.getInt(2);
				Integer partidas_perdidas 	=  rs.getInt(3);
				String  modo_juego 			=  rs.getString(4);
				Integer ranking 			= rs.getInt(5);
				
				UsuarioRanking ran = new UsuarioRanking(nombre, partidas_ganadas, partidas_perdidas, modo_juego, ranking);
				usuarioRanking.add(ran);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return usuarioRanking;
	}
	
	public ArrayList<UsuarioRanking> getCategoria(String categoria) {
		ArrayList<UsuarioRanking> usuarioRanking = new ArrayList<>();
		ResultSet rs = getDb().query("SELECT u.nombre, r.partidas_ganadas,  r.partidas_perdidas, r.modo_juego, "
								  + " RANK () OVER ( PARTITION BY r.modo_juego ORDER BY r.partidas_ganadas DESC) ranking " 
								  + " FROM ranking as r INNER JOIN usuario as u ON r.usuario_id = u.id WHERE r.modo_juego = '"+categoria+"'");

		try {
			while (rs.next()) {
				String  nombre 				= rs.getString(1);
				Integer partidas_ganadas 	= rs.getInt(2);
				Integer partidas_perdidas 	=  rs.getInt(3);
				String  modo_juego 			=  rs.getString(4);
				Integer ranking 			= rs.getInt(5);
				
				UsuarioRanking ran = new UsuarioRanking(nombre, partidas_ganadas, partidas_perdidas, modo_juego, ranking);
				usuarioRanking.add(ran);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return usuarioRanking;
	}

	@Override
	public void save(Ranking t) {
		getDb().query("INSERT INTO public.ranking (id,usuario_id,partidas_ganadas,partidas_perdidas,modo_juego) VALUES (" 
				+ maxID()  + "::integer, " 
				+ t.getId_usuario()  + "::integer, " 
				+ t.getPartidas_ganadas() + "::integer, "
				+ t.getPartidas_perdidas() + "::integer, '"
				+ t.getCategoria() + "'::character varying)"
				+ "returning id;");
		
	}

	@Override
	public void update(Ranking t) {
		getDb().insertar("UPDATE public.ranking\r\n"
				+ "SET usuario_id="  + t.getId_usuario()	   + ", "
				+ "partidas_ganadas="    + t.getPartidas_ganadas()    + ", "
				+ "partidas_perdidas=" 	  + t.getPartidas_perdidas()	   + ", "
				+ "modo_juego='" + t.getCategoria()  + "' "
				+ "	WHERE id = "  + t.getId_ranking()  + ";");
		
	}

	@Override
	public void delete(Ranking t) {
		getDb().insertar("DELETE FROM public.ranking WHERE id IN ("+ t.getId_usuario() + ");");
		
	}
	

	
	public Integer maxID() {
		Integer id = 1;
		ResultSet rs = getDb().query("SELECT id FROM public.ranking ORDER BY id DESC LIMIT 1;");
		
		
			try {
				if (rs.next()) {
					id = rs.getInt(1);
				};
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return id+1;
	}

	public Database getDb() {
		return db;
	}

	public void setDb(Database db) {
		this.db = db;
	}
	

}
