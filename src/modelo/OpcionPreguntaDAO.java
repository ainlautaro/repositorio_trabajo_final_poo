package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OpcionPreguntaDAO implements Dao<OpcionPregunta>{

	private Database db = Database.getInstance();	//llamamos al metodo de la clase que establece la conexion
	
	@Override
	public Optional<OpcionPregunta> get(Integer pregunta_id) {
		OpcionPregunta opcPreg = null;
		ResultSet rs = getDb().query("SELECT * FROM opciones_pregunta WHERE opciones_pregunta.pregunta_id=" + pregunta_id + " LIMIT 1");
		
		try {
			while (rs.next()) {
				Integer id = rs.getInt(1);
				String opcion_pregunta = rs.getString(3);
				Boolean correcta =  rs.getBoolean(4);
				
				opcPreg = new OpcionPregunta(id, pregunta_id, opcion_pregunta, correcta);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Optional.ofNullable(opcPreg);
	}

	@Override
	public List<OpcionPregunta> getAll() {
		List<OpcionPregunta> opciones_preguntas = new ArrayList<>();
		ResultSet rs = getDb().query("SELECT * FROM opciones_pregunta");

		try {
			while (rs.next()) {
				Integer id = rs.getInt(1);
				Integer pregunta_id = rs.getInt(2);
				String opcion_pregunta = rs.getString(3);
				Boolean correcta =  rs.getBoolean(4);
				
				OpcionPregunta opcPregunta = new OpcionPregunta(id, pregunta_id, opcion_pregunta, correcta);
				opciones_preguntas.add(opcPregunta);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return opciones_preguntas;
	}
	
	public ArrayList<OpcionPregunta> getAllWhereIdPregunta(Integer pregunta_id) {
		ArrayList<OpcionPregunta> opciones_preguntas = new ArrayList<>();
		ResultSet rs = getDb().query("SELECT * FROM opciones_pregunta WHERE opciones_pregunta.pregunta_id="+pregunta_id);

		try {
			while (rs.next()) {
				Integer id = rs.getInt(1);
				String opcion_pregunta = rs.getString(3);
				Boolean correcta =  rs.getBoolean(4);
				
				OpcionPregunta opcPregunta = new OpcionPregunta(id, pregunta_id, opcion_pregunta, correcta);
				opciones_preguntas.add(opcPregunta);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return opciones_preguntas;
	}

	@Override
	public void save(OpcionPregunta t) {
		getDb().query("INSERT INTO public.opciones_pregunta (pregunta_id,opcion_pregunta,correcta) VALUES (" + "'" 
				+ t.getPregunta_id()  + "'::integer, '" 
				+ t.getOpcion_pregunta() + "'::character varying, '" 
				+ t.getCorrecta()  + "'::boolean)"
				+ "returning id;");
	}

	@Override
	public void update(OpcionPregunta t) {
		getDb().insertar("UPDATE public.opciones_pregunta\r\n"
				 + "   SET pregunta_id= " + t.getPregunta_id()
				 + " , opcion_pregunta= '" + t.getOpcion_pregunta() + "'"
				 + " , correcta= " + t.getCorrecta() + " \r\n"
				 + "   WHERE id = " + t.getId_opcion() + " ;");
	}

	@Override
	public void delete(OpcionPregunta t) {
		getDb().query("DELETE FROM public.opciones_pregunta WHERE pregunta_id IN ("+ t.getPregunta_id() + ");");
	}
	
	public Integer maxID() {
		Integer id = 1;
		ResultSet rs = getDb().query("SELECT id FROM public.opciones_pregunta ORDER BY id DESC LIMIT 1;");
		
			try {
				if (rs.next()) {
					id = rs.getInt(1);
				};
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return id;
	}
	
	public Database getDb() {
		return db;
	}

	public void setDb(Database db) {
		this.db = db;
	}

}
