//paginas con preguntas de ejemplos para cargar
//https://ganaenpreguntados.blogspot.com/2014/05/ciencia.html
package modelo;

public class OpcionPregunta {

	private Integer id_opcion;
	private Integer pregunta_id;
	private String opcion_pregunta;
	private Boolean correcta;
	
	public OpcionPregunta(Integer id_opcion, Integer pregunta_id, String opcion_pregunta, Boolean correcta) {
		super();
		this.setId_opcion(id_opcion);
		this.pregunta_id = pregunta_id;
		this.opcion_pregunta = opcion_pregunta;
		this.correcta = correcta;
	}

	public final Integer getId_opcion() {
		return id_opcion;
	}

	public final void setId_opcion(Integer id_opcion) {
		this.id_opcion = id_opcion;
	}

	public final Integer getPregunta_id() {
		return pregunta_id;
	}

	public final void setPregunta_id(Integer pregunta_id) {
		this.pregunta_id = pregunta_id;
	}

	public final String getOpcion_pregunta() {
		return opcion_pregunta;
	}

	public final void setOpcion_pregunta(String opcion_pregunta) {
		this.opcion_pregunta = opcion_pregunta;
	}

	public final Boolean getCorrecta() {
		return correcta;
	}

	public final void setCorrecta(Boolean correcta) {
		this.correcta = correcta;
	}
	
	
	
}
