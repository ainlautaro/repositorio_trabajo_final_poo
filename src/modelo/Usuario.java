package modelo;
import java.util.ArrayList;
import java.util.List;

public class Usuario{

	private Integer id_usuario;
	private String nombre, password, email, fotoPerfil;
	private Boolean admin;

	private UsuarioDAO usrDAO = new UsuarioDAO();
	private PreguntaDAO pregDAO = new PreguntaDAO();
	private OpcionPreguntaDAO opcPregDAO = new OpcionPreguntaDAO();

	public Usuario(Integer id_usuario, String nombre, String password, String email, String fotoPerfil, Boolean admin){
		super();

		this.id_usuario = id_usuario;
		this.nombre 	= nombre;
		this.password 	= password;
		this.email 		= email;
		this.fotoPerfil = fotoPerfil;
		this.admin 		= admin;		
	}

	//---------------------------------------------------CONSTRUCTOR USUARIO PARA EL LOGEO
	public Usuario(String nombre, String password) throws ExcepcionUsuarioNoRegistrado {
		List<Usuario> usuariosRegistrados = new ArrayList<>();
		usuariosRegistrados = getUsrDAO().getAll();
		Boolean encontrado = false;

		for (Usuario usuarioRegistrado : usuariosRegistrados) {
			if (usuarioRegistrado.getNombre().equals(nombre) && usuarioRegistrado.getPassword().equals(password)) {
				encontrado = true;
				this.setId_usuario(usuarioRegistrado.getId_usuario());
				this.setNombre(nombre);
				this.setPassword(password);
				try {             //Como el usuario a loguearse ya posee el correo correcto se realiza el try y catch aca
					this.setEmail(usuarioRegistrado.getEmail());
				} catch (ExcepcionFormatoCorreoIncorrecto e) {
					e.getMessage();
				}
				this.setFotoPerfil(usuarioRegistrado.getFotoPerfil());
				this.setAdmin(usuarioRegistrado.getAdmin());
			}
		}

		if (encontrado == false) {
			throw new ExcepcionUsuarioNoRegistrado();
		}

	}
	
	//---------------------------------------------------CONSTRUCTOR USUARIO PARA LA EXCEPCION USUARIO YA REGISTRADO EN REGISTRO
	public Usuario (String nombre, String password, String email) throws ExcepcionUsuarioYaRegistrado, ExcepcionFormatoCorreoIncorrecto {
		List<Usuario> usuariosRegistrados = new ArrayList<>();
		usuariosRegistrados = getUsrDAO().getAll();
		Boolean existe = false;
		
		for (Usuario usuarioRegistrado : usuariosRegistrados) {
			if (usuarioRegistrado.getNombre().equals(nombre) || usuarioRegistrado.getEmail().equals(email)) {
				throw new ExcepcionUsuarioYaRegistrado();
			}
		}
		
		if (existe == false) {
			this.setNombre(nombre);
			this.setPassword(password);
			this.setEmail(email);
			this.setFotoPerfil("");
			this.setAdmin(false);
		}	
	}

	//---------------------------------------------------CONSTRUCTOR USUARIO PARA USUARIOSESION

	public Usuario() {		
	}
	
	//----------------------------------------METODOS----------------------------------------//
	//----------------------------------------CARGAR PREGUNTA
	public void cargarPregunta(Pregunta pregunta, OpcionPregunta op1, OpcionPregunta op2, OpcionPregunta op3, OpcionPregunta op4) {
		pregDAO.save(pregunta);
		opcPregDAO.save(op1);
		opcPregDAO.save(op2);
		opcPregDAO.save(op3);
		opcPregDAO.save(op4);
	}

	//--------------------------------------------------METODO ACTUALIZAR PERFIL USUARIO
	public void actualizaDatos(Usuario usr) {
		this.getUsrDAO().update(usr);
	}
	
	//--------------------------------------------------METODO BUSCA ID PARA REGISTRAR EN RANKING
	public Integer buscaID(String nombre) {
		List<Usuario> usuariosRegistrados = new ArrayList<>();
		usuariosRegistrados = getUsrDAO().getAll();
		
		for (Usuario usuarioRegistrado : usuariosRegistrados) {
			if (usuarioRegistrado.getNombre().equals(nombre)) {
				System.out.println(usuarioRegistrado.getId_usuario());
				return usuarioRegistrado.getId_usuario();
				
			}
		}	
		return null;
	}
		
		
	@Override
	public String toString() {
		return "Usuario [nombre=" + nombre + ", password=" + password + ", email=" + email + ", fotoPerfil="
				+ fotoPerfil + "]";
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) throws ExcepcionFormatoCorreoIncorrecto {
		if (email.toString().indexOf("@gmail.com") == -1 & email.toString().indexOf("@hotmail.com") == -1 & email.toString().indexOf("@yahoo.com") == -1 & email.toString().indexOf("@outlook.com") == -1) {
			throw new ExcepcionFormatoCorreoIncorrecto();
		}
		this.email = email;
	}

	public String getFotoPerfil() {
		return fotoPerfil;
	}

	public void setFotoPerfil(String fotoPerfil) {
		this.fotoPerfil = fotoPerfil;
	}

	public Integer getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}

	public Boolean getAdmin() {
		return admin;
	}

	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}
	
	public OpcionPreguntaDAO getOpcPregDAO() {
		return opcPregDAO;
	}

	public void setOpcPregDAO(OpcionPreguntaDAO opcPregDAO) {
		this.opcPregDAO = opcPregDAO;
	}

	public UsuarioDAO getUsrDAO() {
		return usrDAO;
	}

	public void setUsrDAO(UsuarioDAO usrDAO) {
		this.usrDAO = usrDAO;
	}

	public PreguntaDAO getPregDAO() {
		return pregDAO;
	}

	public void setPregDAO(PreguntaDAO pregDAO) {
		this.pregDAO = pregDAO;
	}

}
