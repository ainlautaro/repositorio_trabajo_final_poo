package vista;

import javax.swing.JDialog;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import controlador.ControladorModalRanking;
import modelo.UsuarioRanking;


public class ModalRanking extends JDialog{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ControladorModalRanking controlador;
	
	private DefaultTableModel modeloCategoriaArte;
	private DefaultTableModel modeloCategoriaDeporte;
	private DefaultTableModel modeloCategoriaHistoria;
	private DefaultTableModel modeloCategoriaGeografia;
	private DefaultTableModel modeloCategoriaCiencia;
	private DefaultTableModel modeloCategoriaEntretenimiento;
	private DefaultTableModel modeloCategoriaRandom;
	
	JTabbedPane tabbedPane;
	JPanel jpArte;
	JPanel jpDeporte;
	JPanel jpHistoria;
	JPanel jpGeografia;
	JPanel jpCiencia; 
	JPanel jpEntretenimiento; 
	JPanel jpRandom;
	private JTable tablaDeporte;
	private JTable tablaHistoria;
	private JTable tablaGeografia;
	private JTable tablaCiencia;
	private JTable tablaEntretenimiento;
	private JTable tablaRandom;
	private JTable tablaArte;
	
	
	public ModalRanking(ControladorModalRanking controlador) {
		setModal(true);
		this.setControlador(controlador);
		setBounds(420, 200, 530, 300);
		setTitle("Ranking");
		setLocationRelativeTo(null);
		setResizable(false);
		
		DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
		tcr.setHorizontalAlignment(SwingConstants.CENTER);		
		
		String header[] = {"Jugador","Pertidas Ganadas", "Partidas Perdidas", "Categoria", "Puestos"};
		this.setModeloCategoriaArte(new DefaultTableModel(null, header));
		this.setModeloCategoriaDeporte(new DefaultTableModel(null, header));
		this.setModeloCategoriaHistoria(new DefaultTableModel(null, header));
		this.setModeloCategoriaGeografia(new DefaultTableModel(null, header));
		this.setModeloCategoriaEntretenimiento(new DefaultTableModel(null, header));
		this.setModeloCategoriaRandom(new DefaultTableModel(null, header));
		this.setModeloCategoriaCiencia(new DefaultTableModel(null, header));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		getContentPane().add(tabbedPane, BorderLayout.CENTER);
		tabbedPane.setSize(530, 400);

		
		JPanel jpArte_1 = new JPanel();
		tabbedPane.addTab("Arte", null, jpArte_1, null);
		jpArte_1.setLayout(null);

		
		JScrollPane scrollPane_6 = new JScrollPane();
		scrollPane_6.setBounds(0, 0, 509, 233);
		jpArte_1.add(scrollPane_6);
		
		tablaArte = new JTable(getModeloCategoriaArte());
		tablaArte.setVisible(true);
		tablaArte.setDefaultEditor(Object.class, null);
		
		tablaArte.getColumnModel().getColumn(3).setMaxWidth(0); 
		tablaArte.getColumnModel().getColumn(3).setMinWidth(0);
		tablaArte.getColumnModel().getColumn(3).setPreferredWidth(0);  
		tablaArte.getColumnModel().getColumn(1).setCellRenderer(tcr);
		tablaArte.getColumnModel().getColumn(2).setCellRenderer(tcr);
		tablaArte.getColumnModel().getColumn(4).setCellRenderer(tcr);

		scrollPane_6.setViewportView(tablaArte);
		
		JPanel jpDeporte = new JPanel();
		tabbedPane.addTab("Deporte", null, jpDeporte, null);
		jpDeporte.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 509, 233);
		jpDeporte.add(scrollPane);
		
		tablaDeporte = new JTable(getModeloCategoriaDeporte());
		tablaDeporte.setVisible(true);
		tablaDeporte.setDefaultEditor(Object.class, null);
		
		tablaDeporte.getColumnModel().getColumn(3).setMaxWidth(0); 
		tablaDeporte.getColumnModel().getColumn(3).setMinWidth(0);
		tablaDeporte.getColumnModel().getColumn(3).setPreferredWidth(0);  
		
		tablaDeporte.getColumnModel().getColumn(1).setCellRenderer(tcr);
		tablaDeporte.getColumnModel().getColumn(2).setCellRenderer(tcr);
		tablaDeporte.getColumnModel().getColumn(4).setCellRenderer(tcr);
		
		scrollPane.setViewportView(tablaDeporte);
		
		JPanel jpHistoria = new JPanel();
		tabbedPane.addTab("Historia", null, jpHistoria, null);
		jpHistoria.setLayout(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(0, 0, 509, 233);
		jpHistoria.add(scrollPane_1);
		
		tablaHistoria = new JTable(getModeloCategoriaHistoria());
		tablaHistoria.setVisible(true);
		tablaHistoria.setDefaultEditor(Object.class, null);
		
		tablaHistoria.getColumnModel().getColumn(3).setMaxWidth(0); 
		tablaHistoria.getColumnModel().getColumn(3).setMinWidth(0);
		tablaHistoria.getColumnModel().getColumn(3).setPreferredWidth(0);  
		
		tablaHistoria.getColumnModel().getColumn(1).setCellRenderer(tcr);
		tablaHistoria.getColumnModel().getColumn(2).setCellRenderer(tcr);
		tablaHistoria.getColumnModel().getColumn(4).setCellRenderer(tcr);


		scrollPane_1.setViewportView(tablaHistoria);
		
		JPanel jpGeografia = new JPanel();
		tabbedPane.addTab("Geografia", null, jpGeografia, null);
		jpGeografia.setLayout(null);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(0, 0, 509, 233);
		jpGeografia.add(scrollPane_2);
		
		tablaGeografia = new JTable(getModeloCategoriaGeografia());
		tablaGeografia.setVisible(true);
		tablaGeografia.setDefaultEditor(Object.class, null);
		
		tablaGeografia.getColumnModel().getColumn(3).setMaxWidth(0); 
		tablaGeografia.getColumnModel().getColumn(3).setMinWidth(0);
		tablaGeografia.getColumnModel().getColumn(3).setPreferredWidth(0);  
		
		tablaGeografia.getColumnModel().getColumn(1).setCellRenderer(tcr);
		tablaGeografia.getColumnModel().getColumn(2).setCellRenderer(tcr);
		tablaGeografia.getColumnModel().getColumn(4).setCellRenderer(tcr);


		scrollPane_2.setViewportView(tablaGeografia);
		
		JPanel jpCiencia = new JPanel();
		tabbedPane.addTab("Ciencia", null, jpCiencia, null);
		jpCiencia.setLayout(null);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(0, 0, 509, 233);
		jpCiencia.add(scrollPane_3);
		
		tablaCiencia = new JTable(getModeloCategoriaCiencia());
		tablaCiencia.setVisible(true);
		tablaCiencia.setDefaultEditor(Object.class, null);
		
		tablaCiencia.getColumnModel().getColumn(3).setMaxWidth(0); 
		tablaCiencia.getColumnModel().getColumn(3).setMinWidth(0);
		tablaCiencia.getColumnModel().getColumn(3).setPreferredWidth(0);  
		
		tablaCiencia.getColumnModel().getColumn(1).setCellRenderer(tcr);
		tablaCiencia.getColumnModel().getColumn(2).setCellRenderer(tcr);
		tablaCiencia.getColumnModel().getColumn(4).setCellRenderer(tcr);


		scrollPane_3.setViewportView(tablaCiencia);
		
		JPanel jpEntretenimiento = new JPanel();
		tabbedPane.addTab("Entretenimiento", null, jpEntretenimiento, null);
		jpEntretenimiento.setLayout(null);
		
		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(0, 0, 509, 233);
		jpEntretenimiento.add(scrollPane_4);
		
		tablaEntretenimiento = new JTable(getModeloCategoriaEntretenimiento());
		tablaEntretenimiento.setVisible(true);
		tablaEntretenimiento.setDefaultEditor(Object.class, null);
		
		tablaEntretenimiento.getColumnModel().getColumn(3).setMaxWidth(0); 
		tablaEntretenimiento.getColumnModel().getColumn(3).setMinWidth(0);
		tablaEntretenimiento.getColumnModel().getColumn(3).setPreferredWidth(0);  
		
		tablaEntretenimiento.getColumnModel().getColumn(1).setCellRenderer(tcr);
		tablaEntretenimiento.getColumnModel().getColumn(2).setCellRenderer(tcr);
		tablaEntretenimiento.getColumnModel().getColumn(4).setCellRenderer(tcr);

		scrollPane_4.setViewportView(tablaEntretenimiento);
		
		JPanel jpRandom = new JPanel();
		tabbedPane.addTab("Random", null, jpRandom, null);
		jpRandom.setLayout(null);
		
		JScrollPane scrollPane_5 = new JScrollPane();
		scrollPane_5.setBounds(0, 0, 509, 233);
		jpRandom.add(scrollPane_5);
		
		tablaRandom = new JTable(getModeloCategoriaRandom());
		tablaRandom.setVisible(true);
		tablaRandom.setDefaultEditor(Object.class, null);
		
		tablaRandom.getColumnModel().getColumn(3).setMaxWidth(0); 
		tablaRandom.getColumnModel().getColumn(3).setMinWidth(0);
		tablaRandom.getColumnModel().getColumn(3).setPreferredWidth(0);  
		
		tablaRandom.getColumnModel().getColumn(1).setCellRenderer(tcr);
		tablaRandom.getColumnModel().getColumn(2).setCellRenderer(tcr);
		tablaRandom.getColumnModel().getColumn(4).setCellRenderer(tcr);
		
		scrollPane_5.setViewportView(tablaRandom);
		
		
		
	}
	
	
	//-----------------------------------------------METODOS----------------------------------------------------------
	public void setearTablaArte(ArrayList<UsuarioRanking> categoriaArte) {
		for (UsuarioRanking categoria : categoriaArte) {
			Object [] row = {String.valueOf(categoria.getNombre()), Integer.toString(categoria.getPartidas_ganadas()), Integer.toString(categoria.getPartidas_perdidas()), categoria.getModo_juego(), Integer.toString(categoria.getRanking())};
			this.getModeloCategoriaArte().addRow(row);
		}
	}
	public void setearTablaEntretenimiento(ArrayList<UsuarioRanking> categoriaEntretenimiento) {
		for (UsuarioRanking categoria : categoriaEntretenimiento) {
			Object [] row = {String.valueOf(categoria.getNombre()), Integer.toString(categoria.getPartidas_ganadas()), Integer.toString(categoria.getPartidas_perdidas()), categoria.getModo_juego(), Integer.toString(categoria.getRanking())};
			this.getModeloCategoriaEntretenimiento().addRow(row);
		}
	}
	public void setearTablaCiencia(ArrayList<UsuarioRanking> categoriaCiencia) {
		for (UsuarioRanking categoria : categoriaCiencia) {
			Object [] row = {String.valueOf(categoria.getNombre()), Integer.toString(categoria.getPartidas_ganadas()), Integer.toString(categoria.getPartidas_perdidas()), categoria.getModo_juego(), Integer.toString(categoria.getRanking())};
			this.getModeloCategoriaCiencia().addRow(row);
		}
	}
	public void setearTablaDeporte(ArrayList<UsuarioRanking> categoriaDeporte) {
		for (UsuarioRanking categoria : categoriaDeporte) {
			Object [] row = {String.valueOf(categoria.getNombre()), Integer.toString(categoria.getPartidas_ganadas()), Integer.toString(categoria.getPartidas_perdidas()), categoria.getModo_juego(), Integer.toString(categoria.getRanking())};
			this.getModeloCategoriaDeporte().addRow(row);
		}
	}
	public void setearTablaHistoria(ArrayList<UsuarioRanking> categoriaHistoria) {
		for (UsuarioRanking categoria : categoriaHistoria) {
			Object [] row = {String.valueOf(categoria.getNombre()), Integer.toString(categoria.getPartidas_ganadas()), Integer.toString(categoria.getPartidas_perdidas()), categoria.getModo_juego(), Integer.toString(categoria.getRanking())};
			this.getModeloCategoriaHistoria().addRow(row);
		}
	}
	public void setearTablaGeografia(ArrayList<UsuarioRanking> categoriaGeografia) {
		for (UsuarioRanking categoria : categoriaGeografia) {
			Object [] row = {String.valueOf(categoria.getNombre()), Integer.toString(categoria.getPartidas_ganadas()), Integer.toString(categoria.getPartidas_perdidas()), categoria.getModo_juego(), Integer.toString(categoria.getRanking())};
			this.getModeloCategoriaGeografia().addRow(row);
		}
	}
	public void setearTablaRandom(ArrayList<UsuarioRanking> categoriaRandom) {
		for (UsuarioRanking categoria : categoriaRandom) {
			Object [] row = {String.valueOf(categoria.getNombre()), Integer.toString(categoria.getPartidas_ganadas()), Integer.toString(categoria.getPartidas_perdidas()), categoria.getModo_juego(), Integer.toString(categoria.getRanking())};
			this.getModeloCategoriaRandom().addRow(row);
		}
	}

	//---------------------------------------------------------GETTERS Y SETTERS---------------------------------------------
	
	
	public ControladorModalRanking getControlador() {
		return controlador;
	}

	public void setControlador(ControladorModalRanking controlador) {
		this.controlador = controlador;
	}

	public DefaultTableModel getModeloCategoriaArte() {
		return modeloCategoriaArte;
	}

	public void setModeloCategoriaArte(DefaultTableModel modeloCategoriaArte) {
		this.modeloCategoriaArte = modeloCategoriaArte;
	}

	public DefaultTableModel getModeloCategoriaDeporte() {
		return modeloCategoriaDeporte;
	}

	public void setModeloCategoriaDeporte(DefaultTableModel modeloCategoriaDeporte) {
		this.modeloCategoriaDeporte = modeloCategoriaDeporte;
	}

	public DefaultTableModel getModeloCategoriaHistoria() {
		return modeloCategoriaHistoria;
	}

	public void setModeloCategoriaHistoria(DefaultTableModel modeloCategoriaHistoria) {
		this.modeloCategoriaHistoria = modeloCategoriaHistoria;
	}

	public DefaultTableModel getModeloCategoriaGeografia() {
		return modeloCategoriaGeografia;
	}

	public void setModeloCategoriaGeografia(DefaultTableModel modeloCategoriaGeografia) {
		this.modeloCategoriaGeografia = modeloCategoriaGeografia;
	}

	public DefaultTableModel getModeloCategoriaCiencia() {
		return modeloCategoriaCiencia;
	}

	public void setModeloCategoriaCiencia(DefaultTableModel modeloCategoriaCiencia) {
		this.modeloCategoriaCiencia = modeloCategoriaCiencia;
	}

	public DefaultTableModel getModeloCategoriaEntretenimiento() {
		return modeloCategoriaEntretenimiento;
	}

	public void setModeloCategoriaEntretenimiento(DefaultTableModel modeloCategoriaEntretenimiento) {
		this.modeloCategoriaEntretenimiento = modeloCategoriaEntretenimiento;
	}

	public DefaultTableModel getModeloCategoriaRandom() {
		return modeloCategoriaRandom;
	}

	public void setModeloCategoriaRandom(DefaultTableModel modeloCategoriaRandom) {
		this.modeloCategoriaRandom = modeloCategoriaRandom;
	}
	
	
}
