package vista;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import controlador.ControladorModalModificarPregunta;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JDialog;

public class ModalModificarPregunta extends JDialog {


	private static final long serialVersionUID = -6781278072283001675L;
	private JPanel contentPane;
	private JTextField txtPregunta;
	private JTextField txtOp1;
	private JTextField txtOp2;
	private JTextField txtOp4;
	private JTextField txtOp3;
	private ControladorModalModificarPregunta cpmp;
	private JButton btnGuardar;
	private JButton btnCancelar;
	
	
	public ModalModificarPregunta(ControladorModalModificarPregunta cmpm) {
		setModal(true);
		this.setCpmp(cmpm);
		setTitle("Modificar");
		
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		setResizable(false);
		
		JLabel lblPregunta = new JLabel("Pregunta: ");
		lblPregunta.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblPregunta.setBounds(35, 11, 64, 22);
		contentPane.add(lblPregunta);
		
		txtPregunta = new JTextField();
		txtPregunta.setBounds(138, 12, 253, 20);
		contentPane.add(txtPregunta);
		txtPregunta.setColumns(10);
		
		JLabel lblOpcion1 = new JLabel("Opcion 1:");
		lblOpcion1.setBounds(35, 68, 64, 14);
		contentPane.add(lblOpcion1);
		
		JLabel lblOpcion2 = new JLabel("Opcion 2: ");
		lblOpcion2.setBounds(35, 106, 64, 14);
		contentPane.add(lblOpcion2);
		
		JLabel lblOpcion3 = new JLabel("Opcion 3:");
		lblOpcion3.setBounds(35, 146, 64, 14);
		contentPane.add(lblOpcion3);
		
		JLabel lblOpcion4 = new JLabel("Opcion 4:");
		lblOpcion4.setBounds(35, 183, 64, 14);
		contentPane.add(lblOpcion4);
		
		txtOp1 = new JTextField();
		txtOp1.setBounds(138, 65, 253, 20);
		contentPane.add(txtOp1);
		txtOp1.setColumns(10);
		
		txtOp2 = new JTextField();
		txtOp2.setBounds(138, 103, 253, 20);
		contentPane.add(txtOp2);
		txtOp2.setColumns(10);
		
		txtOp4 = new JTextField();
		txtOp4.setBounds(138, 180, 253, 20);
		contentPane.add(txtOp4);
		txtOp4.setColumns(10);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(this.getCpmp());
		btnGuardar.setBounds(220, 227, 89, 23);
		contentPane.add(btnGuardar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(this.getCpmp());
		btnCancelar.setBounds(121, 227, 89, 23);
		contentPane.add(btnCancelar);
		
		txtOp3 = new JTextField();
		txtOp3.setBounds(138, 143, 253, 20);
		contentPane.add(txtOp3);
		txtOp3.setColumns(10);
	}

	public JTextField getTxtPregunta() {
		return txtPregunta;
	}

	public void setTxtPregunta(JTextField txtPregunta) {
		this.txtPregunta = txtPregunta;
	}

	public JTextField getTxtOp1() {
		return txtOp1;
	}

	public void setTxtOp1(JTextField txtOp1) {
		this.txtOp1 = txtOp1;
	}

	public JTextField getTxtOp2() {
		return txtOp2;
	}

	public void setTxtOp2(JTextField txtOp2) {
		this.txtOp2 = txtOp2;
	}

	public JTextField getTxtOp4() {
		return txtOp4;
	}

	public void setTxtOp4(JTextField txtOp4) {
		this.txtOp4 = txtOp4;
	}

	public JTextField getTxtOp3() {
		return txtOp3;
	}

	public void setTxtOp3(JTextField txtOp3) {
		this.txtOp3 = txtOp3;
	}

	public ControladorModalModificarPregunta getCpmp() {
		return cpmp;
	}

	public void setCpmp(ControladorModalModificarPregunta cpmp) {
		this.cpmp = cpmp;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}
}
