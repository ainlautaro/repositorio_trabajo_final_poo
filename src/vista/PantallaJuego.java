//http://www.respuestaspreguntados.es/

package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import controlador.ControladorPantallaJuego;
import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerException;
import modelo.OpcionPregunta;

import javax.swing.SwingConstants;
import javax.swing.JTextPane;

public class PantallaJuego extends JFrame {
	
	private static final long serialVersionUID = 7L;
	
	private JPanel contentPane;
	private JLabel lblPersonajesArriba;

	private JButton btnOpcion1;
	private String opcion1;
	
	private JButton btnOpcion2;
	private String opcion2;
	
	private JButton btnOpcion3;
	private String opcion3;
	
	private JButton btnOpcion4;
	private String opcion4;

	private ControladorPantallaJuego controlador;
	private JTextPane textPregunta;
	
	private JLabel CartelCorrecto;
	private JLabel CartelIncorrecto;
	
	private JPanel panelJuego;
	
	private BasicPlayer sonidoCorrecto = new BasicPlayer();
	private BasicPlayer sonidoIncorrecto = new BasicPlayer();
	
	private JLabel lblFinPartida;
	private JPanel panelFinPartida;
	
	private JLabel lblRespuestasCorrectas;
	private JLabel lblRespuestas2;
	
	private JLabel lblContadorCorrectas;
	private JLabel lblContadorRespuestas2;
	
	private JLabel btnVolverInicio;
	
	private JLabel vida1;
	private JLabel vida2;
	private JLabel vida3;
	
	private JLabel btnMinimizar;
	private JLabel btnCerrar;

	public PantallaJuego(ControladorPantallaJuego controlador) {
		this.setControlador(controlador);
		
		try {
			getSonidoCorrecto().open(new File("assets/sonidos/respuestaCorrecta.mp3"));
			getSonidoIncorrecto().open(new File("assets/sonidos/respuestaIncorrecta.mp3"));
		} catch (BasicPlayerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setTitle("PREGUNTADOS - PARTIDA");
		setIconImage(Toolkit.getDefaultToolkit().getImage("assets\\icono.png"));
		setBounds(100, 100, 870, 590);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		setResizable(false);
		setUndecorated(true);
		contentPane.setLayout(null);
		
		btnMinimizar = new JLabel("\u2212");
		btnMinimizar.setHorizontalAlignment(SwingConstants.CENTER);
		btnMinimizar.setForeground(Color.WHITE);
		btnMinimizar.setFont(new Font("Arial", Font.BOLD, 24));
		btnMinimizar.setBounds(770, 0, 49, 39);
		btnMinimizar.addMouseListener((MouseListener) controlador);
		contentPane.add(btnMinimizar);
		
		btnCerrar = new JLabel("X");
		btnCerrar.setBackground(new Color(204, 51, 51));
		btnCerrar.setFont(new Font("Arial", Font.BOLD, 14));
		btnCerrar.setForeground(Color.WHITE);
		btnCerrar.setHorizontalAlignment(SwingConstants.CENTER);
		btnCerrar.setBounds(821, 0, 49, 39);
		btnCerrar.addMouseListener((MouseListener) controlador);
		contentPane.add(btnCerrar);
		
		//-----------------------------------------------PANEL FIN PARTIDA------------------------------------------------//
		panelFinPartida = new JPanel();
		panelFinPartida.setBackground(new Color(0, 0, 0, 0));
		panelFinPartida.setBounds(140, 40, 557, 489);
		panelFinPartida.setVisible(false);
		contentPane.add(panelFinPartida);
		panelFinPartida.setLayout(null);
		
		lblFinPartida = new JLabel();
		lblFinPartida.setForeground(new Color(255, 255, 255));
		lblFinPartida.setBounds(108, 100, 341, 61);
		lblFinPartida.setHorizontalAlignment(SwingConstants.LEFT);
		lblFinPartida.setFont(new Font("Segoe UI", Font.BOLD, 45));
		panelFinPartida.add(lblFinPartida);
		
		lblRespuestasCorrectas = new JLabel("Respuestas Correctas:");
		lblRespuestasCorrectas.setForeground(new Color(255, 255, 255));
		lblRespuestasCorrectas.setFont(new Font("Segoe UI", Font.BOLD, 20));
		lblRespuestasCorrectas.setBounds(108, 172, 205, 34);
		panelFinPartida.add(lblRespuestasCorrectas);
		
		lblRespuestas2 = new JLabel();
		lblRespuestas2.setForeground(new Color(255, 255, 255));
		lblRespuestas2.setFont(new Font("Segoe UI", Font.BOLD, 20));
		lblRespuestas2.setBounds(108, 217, 221, 34);
		panelFinPartida.add(lblRespuestas2);
		
		lblContadorCorrectas = new JLabel();
		lblContadorCorrectas.setForeground(new Color(255, 255, 255));
		lblContadorCorrectas.setHorizontalAlignment(SwingConstants.CENTER);
		lblContadorCorrectas.setFont(new Font("Segoe UI", Font.BOLD, 20));
		lblContadorCorrectas.setBounds(350, 172, 46, 34);
		panelFinPartida.add(lblContadorCorrectas);
		
		lblContadorRespuestas2 = new JLabel();
		lblContadorRespuestas2.setForeground(new Color(255, 255, 255));
		lblContadorRespuestas2.setHorizontalAlignment(SwingConstants.CENTER);
		lblContadorRespuestas2.setFont(new Font("Segoe UI", Font.BOLD, 20));
		lblContadorRespuestas2.setBounds(350, 217, 46, 34);
		panelFinPartida.add(lblContadorRespuestas2);
		
		btnVolverInicio = new JLabel("");
		btnVolverInicio.setIcon(new ImageIcon("assets\\botones\\pantallaJuego\\btnVolverInicio.png"));
		btnVolverInicio.setHorizontalAlignment(SwingConstants.CENTER);
		btnVolverInicio.setBounds(198, 360, 160, 30);
		btnVolverInicio.addMouseListener((MouseListener) getControlador());
		panelFinPartida.add(btnVolverInicio);
		
		//-----------------------------------------------PANEL DE LA PARTIDA------------------------------------------------//
		panelJuego = new JPanel();
		panelJuego.setBorder(null);
		panelJuego.setBackground(new Color(255,255,255,0));
		panelJuego.setBounds(140, 50, 557, 489);
		contentPane.add(panelJuego);
		panelJuego.setLayout(null);
		
		//-----------------------------------------------CARTELES CORRECTO/INCORRECTO------------------------------------------------//
		CartelCorrecto = new JLabel("Correcto");
		CartelCorrecto.setForeground(new Color(0, 153, 51));
		CartelCorrecto.setHorizontalAlignment(SwingConstants.CENTER);
		CartelCorrecto.setFont(new Font("Segoe UI", Font.BOLD, 45));
		CartelCorrecto.setBounds(105, 160, 350, 125);
		CartelCorrecto.setVisible(false);
		
		vida1 = new JLabel("");
		vida1.setIcon(new ImageIcon("assets\\iconos\\vida.png"));
		vida1.setBackground(Color.WHITE);
		vida1.setHorizontalAlignment(SwingConstants.CENTER);
		vida1.setBounds(495, 20, 30, 30);
		panelJuego.add(vida1);
		
		vida2 = new JLabel("");
		vida2.setIcon(new ImageIcon("assets\\iconos\\vida.png"));
		vida2.setHorizontalAlignment(SwingConstants.CENTER);
		vida2.setBounds(445, 20, 30, 30);
		panelJuego.add(vida2);
		
		vida3 = new JLabel("");
		vida3.setIcon(new ImageIcon("assets\\iconos\\vida.png"));
		vida3.setHorizontalAlignment(SwingConstants.CENTER);
		vida3.setBounds(395, 20, 30, 30);
		panelJuego.add(vida3);
		panelJuego.add(CartelCorrecto);
		
		CartelIncorrecto = new JLabel("Incorrecto");
		CartelIncorrecto.setForeground(new Color(204, 51, 51));
		CartelIncorrecto.setHorizontalAlignment(SwingConstants.CENTER);
		CartelIncorrecto.setFont(new Font("Segoe UI", Font.BOLD, 45));
		CartelIncorrecto.setBounds(105, 160, 350, 125);
		CartelIncorrecto.setVisible(false);
		panelJuego.add(CartelIncorrecto);
		
		//-----------------------------------------------PERSONAJE DE ARRIBA------------------------------------------------//
		lblPersonajesArriba = new JLabel("");
		lblPersonajesArriba.setHorizontalAlignment(SwingConstants.CENTER);
		lblPersonajesArriba.setIcon(new ImageIcon("assets\\personajes\\personajes_inicio.png"));
		lblPersonajesArriba.setBounds(135, 20, 300, 112);
		panelJuego.add(lblPersonajesArriba);
		
		//-----------------------------------------------AREA DONDE IRA LA PREGUNTA------------------------------------------------//
		textPregunta = new JTextPane();
		textPregunta.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		textPregunta.setEditable(false);
		textPregunta.setBackground(Color.WHITE);
		textPregunta.setContentType("text/html");
		textPregunta.setBounds(105, 160, 350, 125);
		panelJuego.add(textPregunta);
		
		
		//-----------------------------------------------BOTONES OPCIONES------------------------------------------------//
		btnOpcion1 = new JButton();
		btnOpcion1.addActionListener(getControlador());
		btnOpcion1.setForeground(new Color(0, 0, 0));
		btnOpcion1.setBackground(new Color(255, 255, 255));
		btnOpcion1.setBounds(135, 315, 300, 30);
		btnOpcion1.setFont(new Font("Segoe UI", Font.BOLD, 11));
		panelJuego.add(btnOpcion1);
		
		btnOpcion2 = new JButton();
		btnOpcion2.addActionListener(getControlador());
		btnOpcion2.setForeground(new Color(0, 0, 0));
		btnOpcion2.setBackground(new Color(255, 255, 255));
		btnOpcion2.setBounds(135, 355, 300, 30);
		btnOpcion2.setFont(new Font("Segoe UI", Font.BOLD, 11));
		panelJuego.add(btnOpcion2);
		
		btnOpcion3 = new JButton();
		btnOpcion3.addActionListener(getControlador());
		btnOpcion3.setForeground(new Color(0, 0, 0));
		btnOpcion3.setBackground(new Color(255, 255, 255));
		btnOpcion3.setBounds(135, 395, 300, 30);
		btnOpcion3.setFont(new Font("Segoe UI", Font.BOLD, 11));
		panelJuego.add(btnOpcion3);
		
		btnOpcion4 = new JButton();
		btnOpcion4.addActionListener(getControlador());
		btnOpcion4.setForeground(new Color(0, 0, 0));
		btnOpcion4.setBackground(new Color(255, 255, 255));
		btnOpcion4.setBounds(135, 435, 300, 30);
		btnOpcion4.setFont(new Font("Segoe UI", Font.BOLD, 11));
		panelJuego.add(btnOpcion4);
		
		//-----------------------------------------------FONDO DEL PANEL PARTIDA JUEGO------------------------------------------------//
		JLabel imgFondoPanelJuego = new JLabel("");
		imgFondoPanelJuego.setBackground(Color.BLACK);
		imgFondoPanelJuego.setIcon(new ImageIcon("assets\\fondoPanel.png"));
		imgFondoPanelJuego.setBounds(0, 0, 557, 489);
		panelJuego.add(imgFondoPanelJuego);
		
		//-----------------------------------------------FONDO DEL PANEL------------------------------------------------//
		JPanel panelFondo = new JPanel();
		panelFondo.setBackground(Color.LIGHT_GRAY);
		panelFondo.setBounds(0, 0, 870, 590);
		contentPane.add(panelFondo);
		panelFondo.setLayout(null);
		
		JLabel imgFondo = new JLabel("");
		imgFondo.setIcon(new ImageIcon("assets\\fondo-pantalla-juego.jpg"));
		imgFondo.setBounds(0, 0, 870, 590);
		panelFondo.add(imgFondo);
		
		JPanel panelContenidoJuego = new JPanel();
		panelContenidoJuego.setBounds(138, 31, 557, 489);
		panelFondo.add(panelContenidoJuego);
		
		JLabel mouseTouched = new JLabel("");
		mouseTouched.setBounds(0, 0, 870, 590);
		contentPane.add(mouseTouched);
		
		mouseTouched.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				dragged(e);
			}
		});
		mouseTouched.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				pressed(e);
			}
		});
	
	}

	//-----------------------------------------------METODOS------------------------------------------------//
	public void setearPregunta(String desarrolloPregunta) {
		getTextPregunta().setText(
				"<p style='text-align:center;font-family:Segoe UI;border-radius:2rem 2rem 2rem 2rem;padding:5%;'>"+
						"<br>" + desarrolloPregunta + "<br>" +
				"</p>");
	}
	
	public void opcionCorrecta(JButton correcta) {
		
		correcta.setBackground(new Color(53,212,82));
		
		try {
			this.getCartelCorrecto().setVisible(true);
			getSonidoCorrecto().play(); 
		} catch (BasicPlayerException e) {
			e.printStackTrace();
		}
		
		Thread pintaLaCorrectaDeBlanco = new Thread() {
			public void run() {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				getCartelCorrecto().setVisible(false);
				correcta.setBackground(new Color(255,255,255));
			}
		};
		
		pintaLaCorrectaDeBlanco.start();
	}
	
	public void opcionIncorrecta(ArrayList<OpcionPregunta> oPregunta, Integer cantVidas) {
		
		btnOpcion1.setBackground(new Color(212,79,53));
		btnOpcion2.setBackground(new Color(212,79,53));
		btnOpcion3.setBackground(new Color(212,79,53));
		btnOpcion4.setBackground(new Color(212,79,53));
	
		if(oPregunta.get(0).getCorrecta()) {
			btnOpcion1.setBackground(new Color(53,212,82));
		}
		if(oPregunta.get(1).getCorrecta()) {
			btnOpcion2.setBackground(new Color(53,212,82));
		}
		if(oPregunta.get(2).getCorrecta()) {
			btnOpcion3.setBackground(new Color(53,212,82));
		}
		if(oPregunta.get(3).getCorrecta()) {
			btnOpcion4.setBackground(new Color(53,212,82));
		}
		
		if (cantVidas == 2) {
			this.getVida3().setVisible(false);
		}else if(cantVidas == 1){
			this.getVida2().setVisible(false);
		}else {
			this.getVida1().setVisible(false);
		}
		
		try {
			this.getCartelIncorrecto().setVisible(true);
			sonidoIncorrecto.play(); 
		} catch (BasicPlayerException e) {
			e.printStackTrace();
		}
		
		Thread pintaDeBlancoTodasLasOpciones = new Thread() {
			public void run() {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				getCartelIncorrecto().setVisible(false);
				btnOpcion1.setBackground(new Color(255,255,255));
				btnOpcion2.setBackground(new Color(255,255,255));
				btnOpcion3.setBackground(new Color(255,255,255));
				btnOpcion4.setBackground(new Color(255,255,255));
			}
		};
		
		pintaDeBlancoTodasLasOpciones.start();
		
	}

	public void setearPuntajeFinal(Integer cantCorrectas, Integer cantIncorrectas, Integer cantFaltantes, Integer cantVidas) {
		getLblContadorCorrectas().setText(cantCorrectas.toString());
		
		if (cantVidas > 0) {
			getLblFinPartida().setText("Felicidades!!!");
			getLblRespuestas2().setText("Respuestas Incorrectas:");
			getLblContadorRespuestas2().setText(cantIncorrectas.toString());
		}else {
			getLblFinPartida().setText("Perdiste");
			getLblRespuestas2().setText("Preguntas Faltantes:");
			getLblContadorRespuestas2().setText(cantFaltantes.toString());
		}
	}

	int xx, xy;
	private void pressed(MouseEvent e) {
			xx = e.getX();
			xy = e.getY();
		}
	
	private void dragged(MouseEvent e) {
			int x = e.getXOnScreen();
			int y = e.getYOnScreen();
			
			this.setLocation(x-xx, y-xy);
	}
	
	public ControladorPantallaJuego getControlador() {
		return controlador;
	}

	public void setControlador(ControladorPantallaJuego controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnOpcion1() {
		return btnOpcion1;
	}

	public void setBtnOpcion1(JButton btnOpcion1) {
		this.btnOpcion1 = btnOpcion1;
	}

	public JButton getBtnOpcion2() {
		return btnOpcion2;
	}

	public void setBtnOpcion2(JButton btnOpcion2) {
		this.btnOpcion2 = btnOpcion2;
	}

	public JButton getBtnOpcion3() {
		return btnOpcion3;
	}

	public void setBtnOpcion3(JButton btnOpcion3) {
		this.btnOpcion3 = btnOpcion3;
	}

	public JButton getBtnOpcion4() {
		return btnOpcion4;
	}

	public void setBtnOpcion4(JButton btnOpcion4) {
		this.btnOpcion4 = btnOpcion4;
	}

	public String getOpcion1() {
		return opcion1;
	}

	public void setOpcion1(String opcion1) {
		this.opcion1 = opcion1;
	}

	public String getOpcion2() {
		return opcion2;
	}

	public void setOpcion2(String opcion2) {
		this.opcion2 = opcion2;
	}

	public String getOpcion3() {
		return opcion3;
	}

	public void setOpcion3(String opcion3) {
		this.opcion3 = opcion3;
	}

	public String getOpcion4() {
		return opcion4;
	}

	public void setOpcion4(String opcion4) {
		this.opcion4 = opcion4;
	}
	
	public JTextPane getTextPregunta() {
		return textPregunta;
	}

	public void setTextPregunta(JTextPane textPregunta) {
		this.textPregunta = textPregunta;
	}
	
	public JLabel getCartelCorrecto() {
		return CartelCorrecto;
	}

	public void setCartelCorrecto(JLabel cartelCorrecto) {
		CartelCorrecto = cartelCorrecto;
	}
	
	public JLabel getCartelIncorrecto() {
		return CartelIncorrecto;
	}

	public void setCartelIncorrecto(JLabel cartelIncorrecto) {
		CartelIncorrecto = cartelIncorrecto;
	}
	
	public JPanel getPanelJuego() {
		return panelJuego;
	}

	public void setPanelJuego(JPanel panelJuego) {
		this.panelJuego = panelJuego;
	}

	public JLabel getVida1() {
		return vida1;
	}

	public void setVida1(JLabel vida1) {
		this.vida1 = vida1;
	}

	public JLabel getVida2() {
		return vida2;
	}

	public void setVida2(JLabel vida2) {
		this.vida2 = vida2;
	}

	public JLabel getVida3() {
		return vida3;
	}

	public void setVida3(JLabel vida3) {
		this.vida3 = vida3;
	}

	public BasicPlayer getSonidoCorrecto() {
		return sonidoCorrecto;
	}

	public void setSonidoCorrecto(BasicPlayer sonidoCorrecto) {
		this.sonidoCorrecto = sonidoCorrecto;
	}

	public BasicPlayer getSonidoIncorrecto() {
		return sonidoIncorrecto;
	}

	public void setSonidoIncorrecto(BasicPlayer sonidoIncorrecto) {
		this.sonidoIncorrecto = sonidoIncorrecto;
	}
	
	//--------------------------------------------------------------------GETER'S Y SETTER'S PANEL FIN PARTIDA
	public JPanel getPanelFinPartida() {
		return panelFinPartida;
	}

	public void setPanelFinPartida(JPanel panelFinPartida) {
		this.panelFinPartida = panelFinPartida;
	}
	
	public JLabel getLblFinPartida() {
		return lblFinPartida;
	}

	public void setLblFinPartida(JLabel lblFinPartida) {
		this.lblFinPartida = lblFinPartida;
	}

	public JLabel getLblContadorCorrectas() {
		return lblContadorCorrectas;
	}

	public void setLblContadorCorrectas(JLabel lblContadorCorrectas) {
		this.lblContadorCorrectas = lblContadorCorrectas;
	}

	public JLabel getLblRespuestas2() {
		return lblRespuestas2;
	}

	public void setLblRespuestas2(JLabel lblRespuestas2) {
		this.lblRespuestas2 = lblRespuestas2;
	}

	public JLabel getLblContadorRespuestas2() {
		return lblContadorRespuestas2;
	}

	public void setLblContadorRespuestas2(JLabel lblContadorRespuestas2) {
		this.lblContadorRespuestas2 = lblContadorRespuestas2;
	}

	public JLabel getBtnVolverInicio() {
		return btnVolverInicio;
	}

	public void setBtnVolverInicio(JLabel btnVolverInicio) {
		this.btnVolverInicio = btnVolverInicio;
	}

	public JLabel getBtnMinimizar() {
		return btnMinimizar;
	}

	public void setBtnMinimizar(JLabel btnMinimizar) {
		this.btnMinimizar = btnMinimizar;
	}

	public JLabel getBtnCerrar() {
		return btnCerrar;
	}

	public void setBtnCerrar(JLabel btnCerrar) {
		this.btnCerrar = btnCerrar;
	}
}