package vista;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import controlador.ControladorPanelInicio;

public class PanelInicio extends Panel{

	private static final long serialVersionUID = 2L;
	
	private JLabel btnRanking;
	private JLabel lblPersonajeIzquierda;
	private JLabel lblPersonajesArriba;
	private JLabel btnCine;
	private JLabel btnArte;
	private JLabel btnDeporte;
	private JLabel btnGeografia;
	private JLabel btnHistoria;
	private JLabel btnCiencia;
	private JLabel btnRandom;
	
	private ControladorPanelInicio controlador;
	/**
	 * Create the panel.
	 */
	public PanelInicio(ControladorPanelInicio controlador) {
		this.setControlador(controlador);
		
		
		
		  btnRanking = new JLabel("Ranking");
		  btnRanking.setHorizontalAlignment(SwingConstants.CENTER);
		  btnRanking.setBackground(new Color(0,0,0,0));
		  btnRanking.setBounds(450, 20, 100, 30);
		  btnRanking.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		  btnRanking.addMouseListener(getControlador());
		  add(btnRanking);
		
		//-----------------------------------------------------------------PERSONAJES ESQUINAS---------------------------------------------------------//
		lblPersonajesArriba = new JLabel("");
		lblPersonajesArriba.setIcon(new ImageIcon("assets\\personajes\\personajes_inicio.png"));
		lblPersonajesArriba.setBounds(155, 11, 247, 112);
		add(lblPersonajesArriba);
		
		lblPersonajeIzquierda = new JLabel("");
		lblPersonajeIzquierda.setIcon(new ImageIcon("assets\\personajes\\ReyAlex.png"));
		lblPersonajeIzquierda.setBounds(10, 336, 194, 142);
		add(lblPersonajeIzquierda);
		
		JLabel lblPersonajeDerecha = new JLabel("");
		lblPersonajeDerecha.setIcon(new ImageIcon("assets\\personajes\\capitan_skagg.png"));
		lblPersonajeDerecha.setBounds(352, 336, 195, 142);
		add(lblPersonajeDerecha);
		
		//---------------------------------------------------------BOTONES---------------------------------------------------------//
		btnArte = new JLabel("");
		btnArte.setIcon(new ImageIcon("assets\\botones\\btnArte.png"));
		btnArte.setBounds(28, 133, 140, 80);
		btnArte.addMouseListener(getControlador());
		add(btnArte);
		
		btnCine = new JLabel("");
		btnCine.setIcon(new ImageIcon("assets\\botones\\btnEntretenimiento.png"));
		btnCine.setBounds(208, 133, 140, 80);
		btnCine.addMouseListener(getControlador());
		add(btnCine);
		
		btnDeporte = new JLabel("");
		btnDeporte.setIcon(new ImageIcon("assets\\botones\\btnDeporte.png"));
		btnDeporte.setBounds(389, 134, 140, 80);
		btnDeporte.addMouseListener(getControlador());
		add(btnDeporte);
		
		btnGeografia = new JLabel("");
		btnGeografia.setIcon(new ImageIcon("assets\\botones\\btnGeografia.png"));
		btnGeografia.setBounds(28, 232, 140, 80);
		btnGeografia.addMouseListener(getControlador());
		add(btnGeografia);
		
		btnHistoria = new JLabel("");
		btnHistoria.setIcon(new ImageIcon("assets\\botones\\btnHistoria.png"));
		btnHistoria.setBounds(208, 232, 140, 80);
		btnHistoria.addMouseListener(getControlador());
		add(btnHistoria);
		
		btnCiencia = new JLabel("");
		btnCiencia.setIcon(new ImageIcon("assets\\botones\\btnCiencia.png"));
		btnCiencia.setBounds(389, 232, 140, 80);
		btnCiencia.addMouseListener(getControlador());
		add(btnCiencia);
		
		btnRandom = new JLabel("");
		btnRandom.setIcon(new ImageIcon("assets\\botones\\btnRandom.png"));
		btnRandom.setBounds(208, 332, 140, 80);
		btnRandom.addMouseListener(getControlador());
		add(btnRandom);
		
		add(getFondoPanel());
	}
	
	public ControladorPanelInicio getControlador() {
		return controlador;
	}
	public void setControlador(ControladorPanelInicio controlador) {
		this.controlador = controlador;
	}

	public JLabel getBtnCine() {
		return btnCine;
	}

	public void setBtnCine(JLabel btnCine) {
		this.btnCine = btnCine;
	}

	public JLabel getBtnArte() {
		return btnArte;
	}

	public void setBtnArte(JLabel btnArte) {
		this.btnArte = btnArte;
	}

	public JLabel getBtnDeporte() {
		return btnDeporte;
	}

	public void setBtnDeporte(JLabel btnDeporte) {
		this.btnDeporte = btnDeporte;
	}

	public JLabel getBtnGeografia() {
		return btnGeografia;
	}

	public void setBtnGeografia(JLabel btnGeografia) {
		this.btnGeografia = btnGeografia;
	}

	public JLabel getBtnHistoria() {
		return btnHistoria;
	}

	public void setBtnHistoria(JLabel btnHistoria) {
		this.btnHistoria = btnHistoria;
	}

	public JLabel getBtnCiencia() {
		return btnCiencia;
	}

	public void setBtnCiencia(JLabel btnCiencia) {
		this.btnCiencia = btnCiencia;
	}

	public JLabel getBtnRandom() {
		return btnRandom;
	}

	public void setBtnRandom(JLabel btnRandom) {
		this.btnRandom = btnRandom;
	}

	public JLabel getBtnRanking() {
		return btnRanking;
	}

	public void setBtnRanking(JLabel btnRanking) {
		this.btnRanking = btnRanking;
	}
	

}
