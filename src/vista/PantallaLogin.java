package vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPantallaLogin;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;

public class PantallaLogin extends JFrame {

	private static final long serialVersionUID = 5L;
	
	private JPanel contentPane;
	private JTextField textFieldUsuario;
	private JTextField textFieldPassword;
	private ControladorPantallaLogin controlador;
	private JButton btnIniciarSesion;
	private JButton btnRegistrarse;
	private JLabel lblNombreObligatorio;
	private JLabel lblPasswordObligatorio;
	
	private JLabel btnMinimizar;
	private JLabel btnCerrar;
	/**
	 * Create the frame.
	 * @param controladorPantallaLogin 
	 */
	public PantallaLogin(ControladorPantallaLogin controlador) {
		this.setControlador(controlador);
		
		setTitle("PREGUNTADOS - LOGIN");
		setIconImage(Toolkit.getDefaultToolkit().getImage("assets\\icono.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 870, 590);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setUndecorated(true);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		setResizable(false);
		
		btnMinimizar = new JLabel("\u2212");
		btnMinimizar.setHorizontalAlignment(SwingConstants.CENTER);
		btnMinimizar.setForeground(Color.WHITE);
		btnMinimizar.setFont(new Font("Arial", Font.BOLD, 24));
		btnMinimizar.setBounds(770, 0, 49, 39);
		btnMinimizar.addMouseListener((MouseListener) controlador);
		contentPane.add(btnMinimizar);
		
		btnCerrar = new JLabel("X");
		btnCerrar.setBackground(new Color(204, 51, 51));
		btnCerrar.setFont(new Font("Arial", Font.BOLD, 14));
		btnCerrar.setForeground(Color.WHITE);
		btnCerrar.setHorizontalAlignment(SwingConstants.CENTER);
		btnCerrar.setBounds(821, 0, 49, 39);
		btnCerrar.addMouseListener((MouseListener) controlador);
		contentPane.add(btnCerrar);
		

		  //-------------------------------------------------------------------------------------------------------------------------- --------//
		 //---------------------------------------------------------SECTOR PANEL LOGIN--------------------------------------------------------//
	    //-----------------------------------------------------------------------------------------------------------------------------------//
		JPanel panelLogin = new JPanel();
		panelLogin.setBackground(Color.WHITE);
		panelLogin.setBounds(0, 0, 327, 590);
		contentPane.add(panelLogin);
		panelLogin.setLayout(null);
		
		//---------------------------------------------------------IMG ISOLOGO PREGUNTADOS---------------------------------------------------------//
		JLabel imgTituloPreguntados = new JLabel("");
		imgTituloPreguntados.setBackground(Color.LIGHT_GRAY);
		imgTituloPreguntados.setBounds(40, 50, 247, 32);
		ImageIcon tituloPreguntados = new ImageIcon(new ImageIcon("assets\\titulo_preguntados.png").getImage().getScaledInstance(imgTituloPreguntados.getWidth(),imgTituloPreguntados.getHeight(), Image.SCALE_DEFAULT));	
		imgTituloPreguntados.setIcon(tituloPreguntados);
		panelLogin.add(imgTituloPreguntados);
		
		//--------------------------------------------------------------------INGRESO NOMBRE USUARIO---------------------------------------------------------//
		//---------------------------------------------------------LABEL USUARIO
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setForeground(new Color(112, 112, 112));
		lblUsuario.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		lblUsuario.setBounds(40, 140, 247, 14);
		panelLogin.add(lblUsuario);
		
		//---------------------------------------------------------INPUT USUARIO
		textFieldUsuario = new JTextField();
		lblUsuario.setLabelFor(textFieldUsuario);
		textFieldUsuario.setBounds(40, 161, 247, 30);
		panelLogin.add(textFieldUsuario);
		textFieldUsuario.setColumns(10);
		
		//-----------------------------------------------------------------INGRESO PASSWORD USUARIO---------------------------------------------------------//
		//---------------------------------------------------------LABEL PASSWORD
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setForeground(new Color(112, 112, 112));
		lblPassword.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		lblPassword.setBounds(40, 202, 247, 14);
		panelLogin.add(lblPassword);
		
		//---------------------------------------------------------INPUT PASSWORD
		textFieldPassword = new JPasswordField();
		lblPassword.setLabelFor(textFieldPassword);
		textFieldPassword.setColumns(10);
		textFieldPassword.setBounds(40, 219, 247, 30);
		panelLogin.add(textFieldPassword);
		
		//-----------------------------------------------------------------LBL "*OBLIGATORIO"---------------------------------------------------------//
		lblNombreObligatorio = new JLabel("Ingrese un nombre de usuario*");
		lblNombreObligatorio.setForeground(new Color(255, 0, 0));
		lblNombreObligatorio.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		lblNombreObligatorio.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNombreObligatorio.setBounds(75, 191, 212, 14);
		lblNombreObligatorio.setVisible(false);
		panelLogin.add(lblNombreObligatorio);
		
		lblPasswordObligatorio = new JLabel("Ingrese una contrase\u00F1a*");
		lblPasswordObligatorio.setForeground(new Color(255, 0, 0));
		lblPasswordObligatorio.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		lblPasswordObligatorio.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPasswordObligatorio.setBounds(40, 248, 247, 14);
		lblPasswordObligatorio.setVisible(false);
		panelLogin.add(lblPasswordObligatorio);
		
		//---------------------------------------------------------------------BTN INICIAR SESION---------------------------------------------------------//
		btnIniciarSesion = new JButton("INICIAR SESION");
		
		btnIniciarSesion.addActionListener(getControlador());
		
		btnIniciarSesion.setForeground(new Color(255, 255, 255));
		btnIniciarSesion.setBackground(new Color(229, 85, 159));
		btnIniciarSesion.setFont(new Font("Segoe UI Black", Font.PLAIN, 11));
		btnIniciarSesion.setBounds(40, 280, 120, 34);
		btnIniciarSesion.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));	//elimino los bordes
		panelLogin.add(btnIniciarSesion);
		
		//-----------------------------------------------------------------BTN REGISTRAR USUARIO---------------------------------------------------------//
		btnRegistrarse = new JButton("REGISTRARSE");
			
		btnRegistrarse.addActionListener(getControlador());
		
		btnRegistrarse.setForeground(new Color(255, 255, 255));
		btnRegistrarse.setBackground(new Color(229, 85, 159));
		btnRegistrarse.setFont(new Font("Segoe UI Black", Font.PLAIN, 11));
		btnRegistrarse.setBounds(167, 280, 120, 34);
		btnRegistrarse.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));	//elimino los bordes
		panelLogin.add(btnRegistrarse);
		
		//-----------------------------------------------------------------IMG PERSONAJES PREGUNTADOS---------------------------------------------------------//
		JLabel imgPersonajesPreguntados = new JLabel("");
		imgPersonajesPreguntados.setBounds(40, 388, 247, 133);
		imgPersonajesPreguntados.setIcon(new ImageIcon("assets\\personajes\\personajes_inicio.png"));
		panelLogin.add(imgPersonajesPreguntados);
		
		
		  //----------------------------------------------------------------------------------------------------------------------------------------------//
		 //--------------------------------------------------------SECTOR PANEL CON IMG DE FONDO---------------------------------------------------------//
		//----------------------------------------------------------------------------------------------------------------------------------------------//
		JPanel panelFondo = new JPanel();
		panelFondo.setBackground(Color.LIGHT_GRAY);
		panelFondo.setBounds(326, 0, 544, 590);
		contentPane.add(panelFondo);
		panelFondo.setLayout(null);
		
		//---------------------------------------------------------IMG DE FONDO---------------------------------------------------------//
		JLabel imgFondo = new JLabel("");
		imgFondo.setIcon(new ImageIcon("assets\\fondo.png"));
		imgFondo.setBackground(Color.BLACK);
		imgFondo.setBounds(0, 0, 544, 590);
		panelFondo.add(imgFondo);
		
		JLabel mousetouched = new JLabel("");
		mousetouched.setBounds(0, 0, 854, 551);
		contentPane.add(mousetouched);
		
		mousetouched.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				dragged(e);
			}
		});
		mousetouched.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				pressed(e);
			}
		});
	}

	int xx, xy;
	protected void pressed(MouseEvent e) {
			xx = e.getX();
			xy = e.getY();
		}
	
	protected void dragged(MouseEvent e) {
			int x = e.getXOnScreen();
			int y = e.getYOnScreen();
			
			this.setLocation(x-xx, y-xy);
	}
	
	public ControladorPantallaLogin getControlador() {
		return controlador;
	}
	public void setControlador(ControladorPantallaLogin controlador) {
		this.controlador = controlador;
	}
	
	public JButton getBtnIniciarSesion() {
		return btnIniciarSesion;
	}
	public void setBtnIniciarSesion(JButton btnIniciarSesion) {
		this.btnIniciarSesion = btnIniciarSesion;
	}

	public JButton getBtnRegistrarse() {
		return btnRegistrarse;
	}
	public void setBtnRegistrarse(JButton btnRegistrarse) {
		this.btnRegistrarse = btnRegistrarse;
	}

	public JTextField getTextFieldUsuario() {
		return textFieldUsuario;
	}
	public JTextField getTextFieldPassword() {
		return textFieldPassword;
	}

	public JLabel getLblNombreObligatorio() {
		return lblNombreObligatorio;
	}

	public void setLblNombreObligatorio(JLabel lblNombreObligatorio) {
		this.lblNombreObligatorio = lblNombreObligatorio;
	}

	public JLabel getLblPasswordObligatorio() {
		return lblPasswordObligatorio;
	}

	public void setLblPasswordObligatorio(JLabel lblPasswordObligatorio) {
		this.lblPasswordObligatorio = lblPasswordObligatorio;
	}

	public JLabel getBtnMinimizar() {
		return btnMinimizar;
	}

	public void setBtnMinimizar(JLabel btnMinimizar) {
		this.btnMinimizar = btnMinimizar;
	}

	public JLabel getBtnCerrar() {
		return btnCerrar;
	}

	public void setBtnCerrar(JLabel btnCerrar) {
		this.btnCerrar = btnCerrar;
	}
	
	
}
