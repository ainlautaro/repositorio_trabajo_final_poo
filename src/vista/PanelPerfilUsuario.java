package vista;

import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import controlador.ControladorPanelPerfilUsuario;
import modelo.UsuarioSesion;
import javax.swing.SwingConstants;

public class PanelPerfilUsuario extends Panel{

	private static final long serialVersionUID = 3L;
	
	private JTextField textFieldNombreUsuario;
	private JPasswordField textFieldPassword;
	private JPasswordField textFieldConfirmarPassword;
	private JTextField textFieldEmail; 
	private JTextField textFieldConfirmarEmail;
	private JLabel lblNombreDeUsuario;
	
	private ControladorPanelPerfilUsuario controlador;
	private JButton btnActualizarDatos;
	
	private UsuarioSesion usuarioSesion;
	
	public PanelPerfilUsuario(ControladorPanelPerfilUsuario controlador) { 
		this.setControlador(controlador);
	
		
	//-------------------------------------LBL DONDE IRA EL NOMBRE DEL USUARIO-------------------------------------//
	lblNombreDeUsuario = new JLabel(UsuarioSesion.getInstance().getNombre().toUpperCase());
	lblNombreDeUsuario.setForeground(new Color(112,112,112));
	lblNombreDeUsuario.setHorizontalAlignment(SwingConstants.CENTER);
	lblNombreDeUsuario.setFont(new Font("Segoe UI", Font.BOLD, 24));
	lblNombreDeUsuario.setToolTipText("");
	lblNombreDeUsuario.setBounds(0, 30, 557, 40);
	add(lblNombreDeUsuario);
	
	//-------------------------------------FOTO DE PERFIL-------------------------------------//
	JLabel fotoPerfilUsuario = new JLabel("");
	fotoPerfilUsuario.setIcon(new ImageIcon(UsuarioSesion.getInstance().getFotoPerfil()));
	fotoPerfilUsuario.setBackground(Color.BLACK);
	fotoPerfilUsuario.setBounds(213, 80, 115, 115);
	add(fotoPerfilUsuario);
	
	//-------------------------------------TEXTFIELD NOMBRE DE USUARIO-------------------------------------//
	JLabel lblNombreUsuario = new JLabel("Nombre de Usuario");
	lblNombreUsuario.setForeground(new Color(112, 112, 112));
	lblNombreUsuario.setFont(new Font("Segoe UI", Font.PLAIN, 11));
	lblNombreUsuario.setBounds(146, 210, 264, 14);
	add(lblNombreUsuario);
	
	textFieldNombreUsuario = new JTextField();
	textFieldNombreUsuario.setText(UsuarioSesion.getInstance().getNombre());
	textFieldNombreUsuario.setFont(new Font("Segoe UI", Font.PLAIN, 11));
	lblNombreUsuario.setLabelFor(textFieldNombreUsuario);
	textFieldNombreUsuario.setBounds(146, 230, 264, 30);
	add(textFieldNombreUsuario); 	
	
	//-------------------------------------TEXTFIELD EMAIL-------------------------------------//
	JLabel lblEmail = new JLabel("Email");
	lblEmail.setForeground(new Color(112, 112, 112));
	lblEmail.setFont(new Font("Segoe UI", Font.PLAIN, 11));
	lblEmail.setBounds(146, 270, 120, 14);
	add(lblEmail);
	
	textFieldEmail = new JTextField();
	textFieldEmail.setHorizontalAlignment(SwingConstants.LEFT);
	textFieldEmail.setText(UsuarioSesion.getInstance().getEmail());
	textFieldEmail.setFont(new Font("Segoe UI", Font.PLAIN, 11));
	lblEmail.setLabelFor(textFieldEmail);
	textFieldEmail.setColumns(10);
	textFieldEmail.setBounds(146, 290, 120, 30);
	add(textFieldEmail);
	
	//-------------------------------------CONFIRMAR EMAIL
	JLabel lblConfirmarEmail = new JLabel("Confirmar Email");
	lblConfirmarEmail.setForeground(new Color(112, 112, 112));
	lblConfirmarEmail.setFont(new Font("Segoe UI", Font.PLAIN, 11));
	lblConfirmarEmail.setBounds(290, 270, 120, 14);
	add(lblConfirmarEmail);
	
	textFieldConfirmarEmail = new JTextField();
	textFieldConfirmarEmail.setHorizontalAlignment(SwingConstants.LEFT);
	textFieldConfirmarEmail.setText(UsuarioSesion.getInstance().getEmail());
	textFieldConfirmarEmail.setFont(new Font("Segoe UI", Font.PLAIN, 11));
	lblConfirmarEmail.setLabelFor(textFieldConfirmarEmail);
	textFieldConfirmarEmail.setColumns(10);
	textFieldConfirmarEmail.setBounds(290, 290, 120, 30);
	add(textFieldConfirmarEmail);
	
	//------------------------------------CONTRASEŅA
	
	JLabel lblPassword = new JLabel("Contraseņa");
	lblPassword.setForeground(new Color(112, 112, 112));
	lblPassword.setFont(new Font("Segoe UI", Font.PLAIN, 11));
	lblPassword.setBounds(146, 330, 120, 14);
	add(lblPassword);
	
	textFieldPassword = new JPasswordField();
	textFieldPassword.setHorizontalAlignment(SwingConstants.LEFT);
	textFieldPassword.setText(UsuarioSesion.getInstance().getPassword());
	textFieldPassword.setFont(new Font("Segoe UI", Font.PLAIN, 11));
	lblConfirmarEmail.setLabelFor(textFieldPassword);
	textFieldPassword.setColumns(10);
	textFieldPassword.setBounds(146, 350, 120, 30);
	add(textFieldPassword);
	
	JLabel lblConfirmarPassword = new JLabel("Confirmar Contraseņa");
	lblConfirmarPassword.setForeground(new Color(112, 112, 112));
	lblConfirmarPassword.setFont(new Font("Segoe UI", Font.PLAIN, 11));
	lblConfirmarPassword.setBounds(290, 330, 120, 14);
	add(lblConfirmarPassword);
	
	textFieldConfirmarPassword = new JPasswordField();
	textFieldConfirmarPassword.setHorizontalAlignment(SwingConstants.LEFT);
	textFieldConfirmarPassword.setText(UsuarioSesion.getInstance().getPassword());
	textFieldConfirmarPassword.setFont(new Font("Segoe UI", Font.PLAIN, 11));
	lblConfirmarEmail.setLabelFor(textFieldPassword);
	textFieldConfirmarPassword.setColumns(10);
	textFieldConfirmarPassword.setBounds(290, 350, 120, 30);
	add(textFieldConfirmarPassword);
	
	//-------------------------------------BOTON ACTUALIZAR DATOS-------------------------------------//
	JLabel lblMensajeConfirmacion = new JLabel("*Inicia sesion luego de actualizar");
	lblMensajeConfirmacion.setForeground(new Color(112, 112, 112));
	lblMensajeConfirmacion.setFont(new Font("Segoe UI", Font.PLAIN, 11));
	lblMensajeConfirmacion.setBounds(193, 385, 200, 20);
	add(lblMensajeConfirmacion);
	
	btnActualizarDatos = new JButton("");
	btnActualizarDatos.setBackground(new Color(0,0,0,0));
	btnActualizarDatos.setIcon(new ImageIcon("assets\\botones\\btnActualizarDatos.png"));
	btnActualizarDatos.setBounds(198, 410, 160, 30);
	btnActualizarDatos.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
	btnActualizarDatos.addActionListener(getControlador());
	btnActualizarDatos.addMouseListener(getControlador());
	add(btnActualizarDatos);
	
	//-------------------------------------FONDO PANEL-------------------------------------//
	add(getFondoPanel());
	
	}
	public String panelAviso() {
	
		JPasswordField pass = new JPasswordField();
		int seleccion = JOptionPane.showConfirmDialog(null,  pass, "Introduzca su contraseņa actual", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

		if (seleccion == JOptionPane.OK_OPTION) {
		  String password = new String(pass.getPassword());
		  return password;
		}
		
		return "nulo";
	}
	
	//------GETTERS
	public JTextField getTextFieldNombreUsuario() {
		return textFieldNombreUsuario;
	}

	public void setTextFieldNombreUsuario(JTextField textFieldNombreUsuario) {
		this.textFieldNombreUsuario = textFieldNombreUsuario;
	}

	public JTextField getTextFieldPassword() {
		return textFieldPassword;
	}

	public JTextField getTextFieldConfirmarPassword() {
		return textFieldConfirmarPassword;
	}

	public void setTextFieldPassword(JPasswordField textFieldPassword) {
		this.textFieldPassword = textFieldPassword;
	}

	public void setTextFieldConfirmarPassword(JPasswordField textFieldConfirmarPassword) {
		this.textFieldConfirmarPassword = textFieldConfirmarPassword;
	}

	public JTextField getTextFieldEmail() {
		return textFieldEmail;
	}

	public void setTextFieldEmail(JTextField textFieldEmail) {
		this.textFieldEmail = textFieldEmail;
	}

	public JTextField getTextFieldConfirmarEmail() {
		return textFieldConfirmarEmail;
	}

	public void setTextFieldConfirmarEmail(JTextField textFieldConfirmarEmail) {
		this.textFieldConfirmarEmail = textFieldConfirmarEmail;
	}

	public JButton getBtnActualizarDatos() {
		return btnActualizarDatos;
	}

	public void setBtnActualizarDatos(JButton btnActualizarDatos) {
		this.btnActualizarDatos = btnActualizarDatos;
	}



	public ControladorPanelPerfilUsuario getControlador() {
		return controlador;
	}



	public void setControlador(ControladorPanelPerfilUsuario controlador) {
		this.controlador = controlador;
	}

	public UsuarioSesion getUsuarioSesion() {
		return usuarioSesion;
	}

	public void setUsuarioSesion(UsuarioSesion usuarioSesion) {
		this.usuarioSesion = usuarioSesion;
	}
	
	
	}

