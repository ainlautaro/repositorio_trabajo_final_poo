package vista;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import java.awt.Font;
import javax.swing.SwingConstants;
import controlador.ControladorPanelSugerirPregunta;
import modelo.UsuarioSesion;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;

public class PanelSugerirPregunta extends Panel {
	
	private static final long serialVersionUID = 4L;
	
	private JTextField textFieldPreguntaSugerida;
	private JLabel lblPreguntaSugerida;
	private JLabel lblCategoriaPregunta;
	private JComboBox<String> CategoriaPregunta;
	
	private JLabel lblOpcion1;
	private JTextField textFieldOpcion1;
	private JRadioButton checkOP1;
	
	private JLabel lblOpcion2;
	private JTextField textFieldOpcion2;
	private JRadioButton checkOP2;
	
	private JLabel lblOpcion3;
	private JTextField textFieldOpcion3;
	private JRadioButton checkOP3;
	
	private JLabel lblOpcion4;
	private JTextField textFieldOpcion4;
	private JRadioButton checkOP4;
	
	private JLabel btnSugerirPregunta;
	private ControladorPanelSugerirPregunta controlador;
	private JLabel btnListarPreguntas;

	public PanelSugerirPregunta(ControladorPanelSugerirPregunta controlador) {
		this.setControlador(controlador);
		
		//---------------------------------------------------------BOTONES---------------------------------------------------------//
		JLabel lblTituloPanel = new JLabel("SUGERIR PREGUNTA");
		lblTituloPanel.setForeground(new Color(112,112,112));
		lblTituloPanel.setHorizontalAlignment(SwingConstants.CENTER);
		lblTituloPanel.setFont(new Font("Segoe UI", Font.BOLD, 24));
		lblTituloPanel.setBounds(0, 30, 557, 40);
		add(lblTituloPanel);
		
		//---------------------------------------------------------PREGUNTA A SUGERIR---------------------------------------------------------//
		lblPreguntaSugerida = new JLabel("Pregunta");
		lblPreguntaSugerida.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		lblPreguntaSugerida.setForeground(new Color(112,112,112));
		lblPreguntaSugerida.setBounds(93, 95, 370, 21);
		add(lblPreguntaSugerida);
		
		textFieldPreguntaSugerida = new JTextField();
		textFieldPreguntaSugerida.setBounds(93, 120, 370, 40);
		add(textFieldPreguntaSugerida);
		textFieldPreguntaSugerida.setColumns(10);
		
		//---------------------------------------------------------CATEGORIA PREGUNTA---------------------------------------------------------//
		lblCategoriaPregunta = new JLabel("Categor�a Pregunta");
		lblCategoriaPregunta.setForeground(new Color(112, 112, 112));
		lblCategoriaPregunta.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		lblCategoriaPregunta.setBounds(93, 177, 370, 21);
		add(lblCategoriaPregunta);		
		
		CategoriaPregunta = new JComboBox<String>();
		CategoriaPregunta.setModel(new DefaultComboBoxModel<String>(new String[] {"Seleccione la categor\u00EDa de la pregunta", "Arte", "Entretenimiento", "Deportes", "Geografia", "Historia", "Ciencia"}));
		CategoriaPregunta.setToolTipText("");
		CategoriaPregunta.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		CategoriaPregunta.setBounds(93, 203, 370, 40);
		add(CategoriaPregunta);
		
		//---------------------------------------------------------OPCIONES DE LA PREGUNTA---------------------------------------------------------//
		JLabel lblOpcionesPreguntas = new JLabel("Opciones de las pregunta:");
		lblOpcionesPreguntas.setForeground(new Color(112, 112, 112));
		lblOpcionesPreguntas.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		lblOpcionesPreguntas.setBounds(93, 260, 370, 21);
		add(lblOpcionesPreguntas);
		
		//--------------------------------------------------------OPCION 1
		lblOpcion1 = new JLabel("Opc. 1");
		lblOpcion1.setHorizontalAlignment(SwingConstants.LEFT);
		lblOpcion1.setForeground(new Color(112, 112, 112));
		lblOpcion1.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		lblOpcion1.setBounds(93, 288, 40, 21);
		add(lblOpcion1);
		
		textFieldOpcion1 = new JTextField();
		textFieldOpcion1.setColumns(10);
		textFieldOpcion1.setBounds(143, 289, 234, 20);
		add(textFieldOpcion1);
		
		//--------------------------------------------------------OPCION 2
		lblOpcion2 = new JLabel("Opc. 2");
		lblOpcion2.setHorizontalAlignment(SwingConstants.LEFT);
		lblOpcion2.setForeground(new Color(112, 112, 112));
		lblOpcion2.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		lblOpcion2.setBounds(93, 319, 40, 21);
		add(lblOpcion2);
		
		textFieldOpcion2 = new JTextField();
		textFieldOpcion2.setColumns(10);
		textFieldOpcion2.setBounds(143, 320, 234, 20);
		add(textFieldOpcion2);		
		
		//--------------------------------------------------------OPCION 3
		lblOpcion3 = new JLabel("Opc. 3");
		lblOpcion3.setHorizontalAlignment(SwingConstants.LEFT);
		lblOpcion3.setForeground(new Color(112, 112, 112));
		lblOpcion3.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		lblOpcion3.setBounds(93, 350, 40, 21);
		add(lblOpcion3);
		
		textFieldOpcion3 = new JTextField();
		textFieldOpcion3.setColumns(10);
		textFieldOpcion3.setBounds(143, 351, 234, 20);
		add(textFieldOpcion3);
		
		//--------------------------------------------------------OPCION 4
		lblOpcion4 = new JLabel("Opc. 4");
		lblOpcion4.setHorizontalAlignment(SwingConstants.LEFT);
		lblOpcion4.setForeground(new Color(112, 112, 112));
		lblOpcion4.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		lblOpcion4.setBounds(93, 381, 40, 21);
		add(lblOpcion4);
		
		textFieldOpcion4 = new JTextField();
		textFieldOpcion4.setColumns(10);
		textFieldOpcion4.setBounds(143, 382, 234, 20);
		add(textFieldOpcion4);
		
		//--------------------------------------------------------CHECKBOX'S: OP1, OP2, OP3, OP4
		ButtonGroup grupo1 = new ButtonGroup();
		
		checkOP1 = new JRadioButton("correcta");
		checkOP1.setBounds(380, 288, 109, 23);
		add(checkOP1);
		
		checkOP2 = new JRadioButton("correcta");
		checkOP2.setBounds(380, 319, 109, 23);
		add(checkOP2);
		
		checkOP3 = new JRadioButton("correcta");
		checkOP3.setBounds(380, 350, 109, 23);
		add(checkOP3);
		
		checkOP4 = new JRadioButton("correcta");
		checkOP4.setBounds(380, 381, 109, 23);
		add(checkOP4);
		
		grupo1.add(checkOP1);
		grupo1.add(checkOP2);
		grupo1.add(checkOP3);
		grupo1.add(checkOP4);
		
		//---------------------------------------------------------BTN "SUGERIR" PREGUNTA---------------------------------------------------------//
		String urlImgBoton;
		if (!UsuarioSesion.getInstance().getAdmin()) {
			urlImgBoton = "assets\\botones\\btnSugerirPregunta.png";
		}else {
			urlImgBoton = "assets\\botones\\btnCargarPregunta.png";
		}
		
		btnSugerirPregunta = new JLabel("");
		btnSugerirPregunta.setIcon(new ImageIcon(urlImgBoton));
		btnSugerirPregunta.setBounds(199, 420, 160, 30);
		btnSugerirPregunta.addMouseListener(getControlador());
		add(btnSugerirPregunta);
		
		//---------------------------------------------------------BTN "LISTAR" PREGUNTAS---------------------------------------------------------//
		btnListarPreguntas = new JLabel("Listar preguntas");
		btnListarPreguntas.setHorizontalAlignment(SwingConstants.CENTER);
		btnListarPreguntas.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		btnListarPreguntas.setBounds(429, 11, 118, 23);
		btnListarPreguntas.addMouseListener(getControlador());
		btnListarPreguntas.setVisible(false);
		add(btnListarPreguntas);
		
		//---------------------------------------------------------FONDO PANEL---------------------------------------------------------//
		add(getFondoPanel());

	}

	public final JTextField getTextFieldOpcion1() {
		return textFieldOpcion1;
	}

	public final void setTextFieldOpcion1(JTextField textFieldOpcion1) {
		this.textFieldOpcion1 = textFieldOpcion1;
	}

	public final JTextField getTextFieldOpcion2() {
		return textFieldOpcion2;
	}

	public final void setTextFieldOpcion2(JTextField textFieldOpcion2) {
		this.textFieldOpcion2 = textFieldOpcion2;
	}

	public final void setTextFieldOpcion3(JTextField textFieldOpcion3) {
		this.textFieldOpcion3 = textFieldOpcion3;
	}
	
	public final JTextField getTextFieldOpcion3() {
		return textFieldOpcion3;
	}
	
	public final JTextField getTextFieldOpcion4() {
		return textFieldOpcion4;
	}

	public final void setTextFieldOpcion4(JTextField textFieldOpcion4) {
		this.textFieldOpcion4 = textFieldOpcion4;
	}

	public JTextField getTextFieldPreguntaSugerida() {
		return textFieldPreguntaSugerida;
	}

	public void setTextFieldPreguntaSugerida(JTextField textFieldPreguntaSugerida) {
		this.textFieldPreguntaSugerida = textFieldPreguntaSugerida;
	}

	public JLabel getBtnSugerirPregunta() {
		return btnSugerirPregunta;
	}

	public void setBtnSugerirPregunta(JLabel btnSugerirPregunta) {
		this.btnSugerirPregunta = btnSugerirPregunta;
	}

	public JComboBox<String> getCategoriaPregunta() {
		return CategoriaPregunta;
	}

	public void setCategoriaPregunta(JComboBox<String> categoriaPregunta) {
		CategoriaPregunta = categoriaPregunta;
	}

	public ControladorPanelSugerirPregunta getControlador() {
		return controlador;
	}

	public void setControlador(ControladorPanelSugerirPregunta controlador) {
		this.controlador = controlador;
	}
	
	public JRadioButton getCheckOP1() {
		return checkOP1;
	}

	public void setCheckOP1(JRadioButton checkOP1) {
		this.checkOP1 = checkOP1;
	}

	public JRadioButton getCheckOP2() {
		return checkOP2;
	}

	public void setCheckOP2(JRadioButton checkOP2) {
		this.checkOP2 = checkOP2;
	}

	public JRadioButton getCheckOP3() {
		return checkOP3;
	}

	public void setCheckOP3(JRadioButton checkOP3) {
		this.checkOP3 = checkOP3;
	}

	public JRadioButton getCheckOP4() {
		return checkOP4;
	}

	public void setCheckOP4(JRadioButton checkOP4) {
		this.checkOP4 = checkOP4;
	}

	public JLabel getBtnListarPreguntas() {
		return btnListarPreguntas;
	}

	public void setBtnListarPreguntas(JLabel btnListarPreguntas) {
		this.btnListarPreguntas = btnListarPreguntas;
	}
}
