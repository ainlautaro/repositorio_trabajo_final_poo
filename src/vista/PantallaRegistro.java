package vista;

import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.Color;

import controlador.ControladorPantallaRegistro;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class PantallaRegistro extends JFrame {

	private static final long serialVersionUID = 7L;
	
	private JPanel contentPane;
	private JTextField textFieldUsuario;
	private JTextField textFieldEmail;
	private JTextField textFieldConfirmarEmail;
	private JTextField textFieldPassword;
	private JTextField textFieldConfirmarPassword;
	private ControladorPantallaRegistro controlador;
	private JButton btnRegistrarse;
	private JLabel emailDistinto;
	private JLabel passwordDistinto;
	private JLabel usuarioObligatorio;
	private JButton btnVolverLogin;
	private JLabel errorEmail;
	private JLabel mouseTouched;
	private JLabel btnMinimizar;
	private JLabel btnCerrar;

	/**
	 * Create the frame.
	 */
	public PantallaRegistro(ControladorPantallaRegistro controlador) {
		this.setControlador(controlador);
		
		setTitle("PREGUNTADOS - REGISTRO SEBA");
		setIconImage(Toolkit.getDefaultToolkit().getImage("assets\\icono.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 870, 590);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setUndecorated(true);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnMinimizar = new JLabel("\u2212");
		btnMinimizar.setHorizontalAlignment(SwingConstants.CENTER);
		btnMinimizar.setForeground(Color.WHITE);
		btnMinimizar.setFont(new Font("Arial", Font.BOLD, 24));
		btnMinimizar.setBounds(770, 0, 49, 39);
		btnMinimizar.addMouseListener((MouseListener) controlador);
		contentPane.add(btnMinimizar);
		
		btnCerrar = new JLabel("X");
		btnCerrar.setFont(new Font("Arial", Font.BOLD, 14));
		btnCerrar.setForeground(Color.WHITE);
		btnCerrar.setHorizontalAlignment(SwingConstants.CENTER);
		btnCerrar.setBounds(821, 0, 49, 39);
		btnCerrar.addMouseListener((MouseListener) controlador);
		contentPane.add(btnCerrar);
		
		//------------------------------------------PANEL DE REGISTRO------------------------------------------//
		JPanel panelRegistro = new JPanel();
		panelRegistro.setBorder(null);
		panelRegistro.setBackground(new Color(255,255,255,255)); //255,255,255,230 ideal
		panelRegistro.setBounds(228, 30, 398, 529);
		contentPane.add(panelRegistro);
		panelRegistro.setLayout(null);
		
		//------------------------------------------IMG TITULO PREGUNTADOS------------------------------------------//
		JLabel imgTituloPreguntados = new JLabel("");
		imgTituloPreguntados.setBackground(Color.LIGHT_GRAY);
		imgTituloPreguntados.setBounds(67, 65, 264, 36);
		ImageIcon tituloPreguntados = new ImageIcon(new ImageIcon("assets\\titulo_preguntados.png").getImage().getScaledInstance(imgTituloPreguntados.getWidth(),imgTituloPreguntados.getHeight(), Image.SCALE_DEFAULT));	
		imgTituloPreguntados.setIcon(tituloPreguntados);
		panelRegistro.add(imgTituloPreguntados);
		
		
		//------------------------------------------NOMBRE USUARIO------------------------------------------//
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setForeground(new Color(112, 112, 112));
		lblUsuario.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		lblUsuario.setBounds(67, 137, 264, 14);
		panelRegistro.add(lblUsuario);
		
		textFieldUsuario = new JTextField();
		textFieldUsuario.setBounds(67, 156, 264, 30);
		panelRegistro.add(textFieldUsuario);
		textFieldUsuario.setColumns(10);
		
		//------------------------------------------EMAIL------------------------------------------//
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setForeground(new Color(112, 112, 112));
		lblEmail.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		lblEmail.setBounds(67, 203, 120, 14);
		panelRegistro.add(lblEmail);
		
		textFieldEmail = new JTextField();
		textFieldEmail.setColumns(10);
		textFieldEmail.setBounds(67, 221, 120, 30);
		panelRegistro.add(textFieldEmail);
		
		//------------------------------------------CONFIRMAR EMAIL------------------------------------------//
		JLabel lblConfirmarEmail = new JLabel("Confirmar Email");
		lblConfirmarEmail.setForeground(new Color(112, 112, 112));
		lblConfirmarEmail.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		lblConfirmarEmail.setBounds(211, 203, 120, 14);
		panelRegistro.add(lblConfirmarEmail);
		
		textFieldConfirmarEmail = new JTextField();
		textFieldConfirmarEmail.setColumns(10);
		textFieldConfirmarEmail.setBounds(211, 221, 120, 30);
		panelRegistro.add(textFieldConfirmarEmail);
		
		//------------------------------------------PASSWORD------------------------------------------//
		JLabel lblPassword = new JLabel("Contrase\u00F1a");
		lblPassword.setForeground(new Color(112, 112, 112));
		lblPassword.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		lblPassword.setBounds(67, 262, 120, 14);
		panelRegistro.add(lblPassword);
		
		textFieldPassword = new JPasswordField();
		textFieldPassword.setColumns(10);
		textFieldPassword.setBounds(67, 280, 120, 30);
		panelRegistro.add(textFieldPassword);
		
		//------------------------------------------CONFIRMAR PASSWORD------------------------------------------//
		JLabel lblConfirmarPassword = new JLabel("Confirmar Contrase\u00F1a");
		lblConfirmarPassword.setForeground(new Color(112, 112, 112));
		lblConfirmarPassword.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		lblConfirmarPassword.setBounds(211, 262, 120, 14);
		panelRegistro.add(lblConfirmarPassword);
		
		textFieldConfirmarPassword = new JPasswordField();
		textFieldConfirmarPassword.setColumns(10);
		textFieldConfirmarPassword.setBounds(211, 280, 120, 30);
		panelRegistro.add(textFieldConfirmarPassword);
		
		//------------------------------------------BOTON "REGISTRARSE"------------------------------------------//
		btnRegistrarse = new JButton("REGISTRARSE");
		btnRegistrarse.addActionListener(getControlador());
		
		btnRegistrarse.setForeground(new Color(255, 255, 255));
		btnRegistrarse.setBackground(new Color(229, 85, 159));
		btnRegistrarse.setBounds(140, 351, 120, 30);
		btnRegistrarse.setFont(new Font("Segoe UI Black", Font.PLAIN, 11));
		btnRegistrarse.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));	//elimino los bordes
		panelRegistro.add(btnRegistrarse);
		
		//------------------------------------------BOTON "VOLVER"------------------------------------------//
		btnVolverLogin = new JButton("");
		btnVolverLogin.setIcon(new ImageIcon("assets\\iconos\\volver.png"));
		btnVolverLogin.addActionListener(getControlador());
		btnVolverLogin.setBackground(new Color(255,255,255,255));
		btnVolverLogin.setBounds(20, 20, 45, 25);
		btnVolverLogin.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));	//elimino los bordes
		panelRegistro.add(btnVolverLogin);
		
		//------------------------------------------LBL'S DATOS DISTINTOS | OBLIGATORIOS------------------------------------------//
		usuarioObligatorio = new JLabel("");
		usuarioObligatorio.setIcon(new ImageIcon("assets\\iconos\\error.png"));
		usuarioObligatorio.setBounds(335, 160, 20, 20);
		usuarioObligatorio.setVisible(false);
		panelRegistro.add(usuarioObligatorio);
		
		emailDistinto = new JLabel("");
		emailDistinto.setIcon(new ImageIcon("assets\\iconos\\error.png"));
		emailDistinto.setBackground(new Color(0,0,0,0));
		emailDistinto.setHorizontalAlignment(SwingConstants.CENTER);
		emailDistinto.setBounds(335, 225, 20, 20);
		emailDistinto.setVisible(false);
		panelRegistro.add(emailDistinto);
		
		passwordDistinto = new JLabel("");
		passwordDistinto.setBackground(new Color(255,255,255,0));
		passwordDistinto.setIcon(new ImageIcon("assets\\iconos\\error.png"));
		passwordDistinto.setHorizontalAlignment(SwingConstants.CENTER);
		passwordDistinto.setBounds(335, 285, 20, 20);
		passwordDistinto.setVisible(false);
		panelRegistro.add(passwordDistinto);
		
		//------------------------------------------IMAGEN DE LOS PERSONAJES------------------------------------------//
		JLabel PersonajesPreguntados = new JLabel("");
		PersonajesPreguntados.setIcon(new ImageIcon("assets\\personajes.png"));
		PersonajesPreguntados.setBounds(10, 415, 378, 88);
		panelRegistro.add(PersonajesPreguntados);
		
		//-----------------------------------------ERROR EN EL FORMATO DEL EMAIL----------------------------------//
		errorEmail = new JLabel("*error en el formato del Email");
		errorEmail.setForeground(Color.RED);
		errorEmail.setFont(new Font("Segoe UI", Font.PLAIN, 11));
		errorEmail.setBounds(122, 326, 159, 14);
		errorEmail.setVisible(false);
		panelRegistro.add(errorEmail);
		
		//------------------------------------------FONDO DE PANTALLA------------------------------------------//
		JLabel imgFondo = new JLabel("");
		imgFondo.setIcon(new ImageIcon("assets\\fondo-full.jpg"));
		imgFondo.setBounds(0, 0, 870, 590);
		contentPane.add(imgFondo);
		
		mouseTouched = new JLabel("");
		mouseTouched.setBounds(0, 0, 870, 590);
		contentPane.add(mouseTouched);
		setLocationRelativeTo(null);
		setResizable(false);
		
		mouseTouched.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				dragged(e);
			}
		});
		mouseTouched.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				pressed(e);
			}
		});
	}
	
	int xx, xy;
	protected void pressed(MouseEvent e) {
			xx = e.getX();
			xy = e.getY();
		}
	
	protected void dragged(MouseEvent e) {
			int x = e.getXOnScreen();
			int y = e.getYOnScreen();
			
			this.setLocation(x-xx, y-xy);
	}

	public JButton getBtnVolverLogin() {
		return btnVolverLogin;
	}

	public void setBtnVolverLogin(JButton btnVolverLogin) {
		this.btnVolverLogin = btnVolverLogin;
	}

	public JButton getBtnRegistrarse() {
		return btnRegistrarse;
	}

	public void setBtnRegistrarse(JButton btnRegistrarse) {
		this.btnRegistrarse = btnRegistrarse;
	}

	public ControladorPantallaRegistro getControlador() {
		return controlador;
	}

	public void setControlador(ControladorPantallaRegistro controladorPantallaRegistro) {
		this.controlador = controladorPantallaRegistro;
	}

	public JTextField getTextFieldUsuario() {
		return textFieldUsuario;
	}

	public JTextField getTextFieldEmail() {
		return textFieldEmail;
	}

	public JTextField getTextFieldConfirmarEmail() {
		return textFieldConfirmarEmail;
	}

	public JTextField getTextFieldPassword() {
		return textFieldPassword;
	}

	public JTextField getTextFieldConfirmarPassword() {
		return textFieldConfirmarPassword;
	}

	public JLabel getEmailDistinto() {
		return emailDistinto;
	}

	public void setEmailDistinto(JLabel emailDistinto) {
		this.emailDistinto = emailDistinto;
	}

	public JLabel getPasswordDistinto() {
		return passwordDistinto;
	}

	public void setPasswordDistinto(JLabel passwordDistinto) {
		this.passwordDistinto = passwordDistinto;
	}

	public JLabel getUsuarioObligatorio() {
		return usuarioObligatorio;
	}

	public void setUsuarioObligatorio(JLabel usuarioObligatorio) {
		this.usuarioObligatorio = usuarioObligatorio;
	}

	public JLabel getErrorEmail() {
		return errorEmail;
	}

	public void setErrorEmail(JLabel errorEmail) {
		this.errorEmail = errorEmail;
	}

	public JLabel getBtnMinimizar() {
		return btnMinimizar;
	}

	public void setBtnMinimizar(JLabel btnMinimizar) {
		this.btnMinimizar = btnMinimizar;
	}

	public JLabel getBtnCerrar() {
		return btnCerrar;
	}

	public void setBtnCerrar(JLabel btnCerrar) {
		this.btnCerrar = btnCerrar;
	}
	
	
}
