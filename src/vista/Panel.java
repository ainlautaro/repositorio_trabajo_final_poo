package vista;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Panel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private JLabel fondoPanel;
	
	public Panel() {

		setBackground(new Color(0, 0, 0, 0));
		setBounds(255, 50, 557, 489);
		setLayout(null);
		
		fondoPanel = new JLabel("");
		fondoPanel.setIcon(new ImageIcon("assets\\fondoPanel.png"));
		fondoPanel.setBounds(0, 0, 557, 489);
		
	}

	public JLabel getFondoPanel() {
		return fondoPanel;
	}

	public void setFondoPanel(JLabel fondoPanel) {
		this.fondoPanel = fondoPanel;
	}

}
