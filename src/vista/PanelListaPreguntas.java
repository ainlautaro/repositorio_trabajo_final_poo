/*
 * objetivo
 */
package vista;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import controlador.ControladorPanelListaPreguntas;
import modelo.Pregunta;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;

public class PanelListaPreguntas extends Panel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7418567401566486114L;
	private JTable tablaSugeridas;
	private DefaultTableModel modeloTablaSugerida;
	private DefaultTableModel modeloTablaCargadas;
	private JTable tablaCargadas;
	private JLabel btnBorrarPregunta;
	private JLabel btnCargarPregunta;
	
	private ControladorPanelListaPreguntas controlador;
	private JLabel btnBorrarSugerida;
	private JLabel btnModificarSugerida;
	private JLabel btnOpcionesSugeridas;
	private JLabel btnOpcionesCargadas;
	private JLabel btnModificarCargada;
	
	private JPanel panel;
	
	private JPanel navbarSugeridas;
	private JPanel navbarCargadas;
	
	private JLabel btnInfoPartPorCategSug;
	private JLabel btnInfoCompPregSug;
	private JLabel btnInfoTops1Sug;
	private JLabel btnInfoPartPorCategCar;
	private JLabel btnInfoComparPregCar;
	private JLabel btnInfoTops1Car;
	private JLabel fondoNavbarCargadas;
	
	/**
	 * Create the panel.
	 */
	public PanelListaPreguntas(ControladorPanelListaPreguntas controlador) {
		this.setControlador(controlador);
		
		//---------------------------------- Tabbed Pane ----------------------------------//
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBackground(new Color(255,255,255,0));
		tabbedPane.setSize(557, 489);
		
		String header[] = {"id","Pregunta","Categoria","validada"};
		
		this.setModeloTablaSugerida(new DefaultTableModel(null, header)); // con el null se crea la tabla vacia, primero son los datos que va a tener la tabla
		this.setModeloTablaCargadas(new DefaultTableModel(null, header));
		
		//---------------------------- Panel de preguntas sugeridas--------------------------//
		
		JPanel panelSugeridas = new JPanel();
		panelSugeridas.setBackground(Color.WHITE);
		tabbedPane.addTab("Preguntas sugeridas", null, panelSugeridas, null);
		panelSugeridas.setLayout(null);
		
		//--------------------------- Scroll pane --------------------------------//
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 68, 532, 382);
		panelSugeridas.add(scrollPane);
		
		//-------------------------- Tabla con preguntas sugeridas -----------------//
		
		tablaSugeridas = new JTable(this.getModeloTablaSugerida());
		tablaSugeridas.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		tablaSugeridas.setVisible(true);
		tablaSugeridas.setDefaultEditor(Object.class, null);  //No permite las ediciones
		tablaSugeridas.getColumnModel().getColumn(0).setMaxWidth(0);  //Para que no aparezca el id
		tablaSugeridas.getColumnModel().getColumn(0).setMinWidth(0);  //Para que no se pueda achicar ni expandir la columna
		tablaSugeridas.getColumnModel().getColumn(0).setPreferredWidth(0);  //La columna queda ineditable
		//-----------------------------------------------------------------------------------------------------------//
		tablaSugeridas.getColumnModel().getColumn(3).setMaxWidth(0);  //Para que no aparezca el id
		tablaSugeridas.getColumnModel().getColumn(3).setMinWidth(0);  //Para que no se pueda achicar ni expandir la columna
		//tablaSugeridas.getColumnModel().getColumn(3).setPreferredWidth(0);  //La columna queda ineditable
		//----------------------------------------------------------------------------------------------------------//
		tablaSugeridas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); //No permite seleccionar mas de una fila
		tablaSugeridas.getSelectionModel().addListSelectionListener(this.getControlador());  //Habilita el boton solo si selecciono una fila
		
		tablaSugeridas.getColumnModel().getColumn(2).setMaxWidth(115);
		
		scrollPane.setViewportView(tablaSugeridas);
		
		navbarSugeridas = new JPanel();
		navbarSugeridas.setBackground(new Color(255, 0, 153,0));
		navbarSugeridas.setBounds(0, 20, 552, 40);
		panelSugeridas.add(navbarSugeridas);
		navbarSugeridas.setLayout(null);
		
		//----------------------------Boton para modificar una pregunta o las opciones de la pregunta seleccionada (SUGERIDA)---------------------------//
		
		btnModificarSugerida = new JLabel();
		btnModificarSugerida.setForeground(new Color(255, 255, 255));
		btnModificarSugerida.setBounds(92, 10, 75, 20);
		navbarSugeridas.add(btnModificarSugerida);
		btnModificarSugerida.setHorizontalAlignment(SwingConstants.CENTER);
		btnModificarSugerida.setText("Modificar");
		btnModificarSugerida.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btnModificarSugerida.addMouseListener((MouseListener) getControlador());
		
		//----------------------------Boton para mostrar las opciones de la pregunta seleccionada de la pesta�a sugeridas----------------------//
		
		btnOpcionesSugeridas = new JLabel();
		btnOpcionesSugeridas.setForeground(new Color(255, 255, 255));
		btnOpcionesSugeridas.setBounds(15, 10, 67, 20);
		navbarSugeridas.add(btnOpcionesSugeridas);
		btnOpcionesSugeridas.setHorizontalAlignment(SwingConstants.CENTER);
		btnOpcionesSugeridas.setText("Opciones");
		btnOpcionesSugeridas.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btnOpcionesSugeridas.addMouseListener((MouseListener) getControlador());
		
		//-----------------------------Boton borrar pregunta sugerida---------------------------//
		
		btnBorrarSugerida = new JLabel();
		btnBorrarSugerida.setForeground(new Color(255, 255, 255));
		btnBorrarSugerida.setBounds(240, 10, 52, 20);
		navbarSugeridas.add(btnBorrarSugerida);
		btnBorrarSugerida.setHorizontalAlignment(SwingConstants.CENTER);
		btnBorrarSugerida.setText("Borrar");
		btnBorrarSugerida.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btnBorrarSugerida.addMouseListener((MouseListener) getControlador());
		
		//------------------------- Boton para volver desde la pantalla de preguntas sugeridas-------------------------------//
		
		btnCargarPregunta = new JLabel();
		btnCargarPregunta.setForeground(new Color(255, 255, 255));
		btnCargarPregunta.setBounds(177, 10, 53, 20);
		navbarSugeridas.add(btnCargarPregunta);
		btnCargarPregunta.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btnCargarPregunta.setHorizontalAlignment(SwingConstants.CENTER);
		btnCargarPregunta.setText("Cargar");
		btnCargarPregunta.setBackground(new Color(255,255,255,255));
		btnCargarPregunta.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));  					//elimino los bordes
		btnCargarPregunta.addMouseListener((MouseListener) getControlador());
		
		//----------- Boton para ver el informe sobre las partidas que se jugaron por modo de juego (pantalla sugeridas)-----------// 
		
		btnInfoPartPorCategSug = new JLabel();
		btnInfoPartPorCategSug.setForeground(new Color(255, 255, 255));
		btnInfoPartPorCategSug.setBounds(302, 10, 52, 20);
		navbarSugeridas.add(btnInfoPartPorCategSug);
		btnInfoPartPorCategSug.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btnInfoPartPorCategSug.setHorizontalAlignment(SwingConstants.CENTER);
		btnInfoPartPorCategSug.setText("I.part");  									//Modificar
		btnInfoPartPorCategSug.setBackground(new Color(255,255,255,255));
		btnInfoPartPorCategSug.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnInfoPartPorCategSug.addMouseListener((MouseListener) getControlador());
		
		//----------- Boton para ver el informe sobre la comparacion entre preguntas sugeridas y cargadas por modo de juego (pantalla sugeridas)-------------//
		
		btnInfoCompPregSug = new JLabel();
		btnInfoCompPregSug.setForeground(new Color(255, 255, 255));
		btnInfoCompPregSug.setBounds(364, 10, 52, 19);
		navbarSugeridas.add(btnInfoCompPregSug);
		btnInfoCompPregSug.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btnInfoCompPregSug.setHorizontalAlignment(SwingConstants.CENTER);
		btnInfoCompPregSug.setText("I.comp");											//Modificar  
		btnInfoCompPregSug.setBackground(new Color(255,255,255,255));
		btnInfoCompPregSug.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnInfoCompPregSug.addMouseListener((MouseListener) getControlador());
		
		//--------------------- Boton para ver el informe sobre los top1 por modo de juego (pantalla sugeridas)---------------------------//
		
		btnInfoTops1Sug = new JLabel();
		btnInfoTops1Sug.setForeground(new Color(255, 255, 255));
		btnInfoTops1Sug.setBounds(426, 10, 52, 19);
		navbarSugeridas.add(btnInfoTops1Sug);
		btnInfoTops1Sug.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btnInfoTops1Sug.setHorizontalAlignment(SwingConstants.CENTER);
		btnInfoTops1Sug.setText("I.top1");													//Modificar
		btnInfoTops1Sug.setBackground(new Color(255,255,255,255));
		btnInfoTops1Sug.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		
		JLabel fondoNavbarSugeridas = new JLabel("");
		fondoNavbarSugeridas.setIcon(new ImageIcon("assets\\navbar.png"));
		fondoNavbarSugeridas.setHorizontalAlignment(SwingConstants.CENTER);
		fondoNavbarSugeridas.setBounds(0, 0, 552, 40);
		navbarSugeridas.add(fondoNavbarSugeridas);
		btnInfoTops1Sug.addMouseListener((MouseListener) getControlador());
		
		//--------------------------- Panel preguntas cargadas ----------------------//
		
		JPanel panelCargadas = new JPanel();
		panelCargadas.setBackground(Color.WHITE);
		tabbedPane.addTab("Preguntas cargadas", null, panelCargadas, null);
		panelCargadas.setLayout(null);
		
		//---------------------------Segundo scroll panel--------------------------------------//
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 68, 532, 382);
		panelCargadas.add(scrollPane_1);
		
		//--------------------------Tabla con preguntas cargadas------------------------------//
		
		tablaCargadas = new JTable(this.getModeloTablaCargadas());
		tablaCargadas.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		tablaCargadas.setVisible(true);
		tablaCargadas.setDefaultEditor(Object.class, null);  //No permite las ediciones
		tablaCargadas.getColumnModel().getColumn(0).setMaxWidth(0);  //Para que no aparezca el id
		tablaCargadas.getColumnModel().getColumn(0).setMinWidth(0);  //Para que no se pueda achicar ni expandir la columna
		tablaCargadas.getColumnModel().getColumn(0).setPreferredWidth(0);  //La columna queda ineditable
		//-----------------------------------------------------------------------------------//
		tablaCargadas.getColumnModel().getColumn(3).setMaxWidth(0);  //Para que no aparezca el id
		tablaCargadas.getColumnModel().getColumn(3).setMinWidth(0);  //Para que no se pueda achicar ni expandir la columna
		tablaCargadas.getColumnModel().getColumn(3).setPreferredWidth(0);  //La columna queda ineditable
		//-----------------------------------------------------------------------------------//
		tablaCargadas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); //No permite seleccionar mas de una fila
		tablaCargadas.getSelectionModel().addListSelectionListener(this.getControlador());  //Habilita el boton solo si selecciono una fila
		
		tablaCargadas.getColumnModel().getColumn(2).setMaxWidth(115);
		
		scrollPane_1.setViewportView(tablaCargadas);
		
		navbarCargadas = new JPanel();
		navbarCargadas.setBackground(new Color(255, 0, 153,0));
		navbarCargadas.setBounds(0, 20, 552, 40);
		panelCargadas.add(navbarCargadas);
		navbarCargadas.setLayout(null);
		
		//----------------------------Boton para mostrar las opciones de la pregunta seleccionada de la pesta�a cargadas----------------------//
		
		btnOpcionesCargadas = new JLabel();
		btnOpcionesCargadas.setForeground(new Color(255, 255, 255));
		btnOpcionesCargadas.setBounds(15, 10, 67, 20);
		navbarCargadas.add(btnOpcionesCargadas);
		btnOpcionesCargadas.setHorizontalAlignment(SwingConstants.CENTER);
		btnOpcionesCargadas.setText("Opciones");
		btnOpcionesCargadas.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btnOpcionesCargadas.addMouseListener((MouseListener) getControlador());
		
		//----------------------------Boton para modificar una pregunta o las opciones de la pregunta seleccionada (CARGADA)---------------------------//
		
		btnModificarCargada = new JLabel();
		btnModificarCargada.setForeground(new Color(255, 255, 255));
		btnModificarCargada.setBounds(92, 10, 75, 20);
		navbarCargadas.add(btnModificarCargada);
		btnModificarCargada.setHorizontalAlignment(SwingConstants.CENTER);
		btnModificarCargada.setText("Modificar");
		btnModificarCargada.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btnModificarCargada.addMouseListener((MouseListener) getControlador());
		
		//----------------------------Boton borrar pregunta cargada-----------------------------//
		
		btnBorrarPregunta = new JLabel();
		btnBorrarPregunta.setForeground(new Color(255, 255, 255));
		btnBorrarPregunta.setBounds(177, 10, 52, 20);
		navbarCargadas.add(btnBorrarPregunta);
		btnBorrarPregunta.setHorizontalAlignment(SwingConstants.CENTER);
		btnBorrarPregunta.setText("Borrar");
		btnBorrarPregunta.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btnBorrarPregunta.addMouseListener((MouseListener) getControlador());
		
		//----------- Boton para ver el informe sobre las partidas que se jugaron por modo de juego (pantalla cargadas)-----------//
		
		btnInfoPartPorCategCar = new JLabel();
		btnInfoPartPorCategCar.setForeground(new Color(255, 255, 255));
		btnInfoPartPorCategCar.setBounds(239, 10, 52, 20);
		navbarCargadas.add(btnInfoPartPorCategCar);
		btnInfoPartPorCategCar.setHorizontalAlignment(SwingConstants.CENTER);
		btnInfoPartPorCategCar.setText("I.part");											//Modificar
		btnInfoPartPorCategCar.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btnInfoPartPorCategCar.addMouseListener((MouseListener) getControlador());

		
		//----------- Boton para ver el informe sobre la comparacion entre preguntas sugeridas y cargadas por modo de juego (pantalla sugeridas)-------------//
		
		btnInfoComparPregCar = new JLabel();
		btnInfoComparPregCar.setForeground(new Color(255, 255, 255));
		btnInfoComparPregCar.setBounds(301, 10, 52, 20);
		navbarCargadas.add(btnInfoComparPregCar);
		btnInfoComparPregCar.setHorizontalAlignment(SwingConstants.CENTER);
		btnInfoComparPregCar.setText("I.comp");												//Modificar
		btnInfoComparPregCar.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btnInfoComparPregCar.addMouseListener((MouseListener) getControlador());
		
		//--------------------- Boton para ver el informe sobre los top1 por modo de juego (pantalla sugeridas)---------------------------//
		
		btnInfoTops1Car = new JLabel();
		btnInfoTops1Car.setForeground(new Color(255, 255, 255));
		btnInfoTops1Car.setBounds(367, 10, 52, 20);
		navbarCargadas.add(btnInfoTops1Car);
		btnInfoTops1Car.setHorizontalAlignment(SwingConstants.CENTER);
		btnInfoTops1Car.setText("I.top1");														//Modificar
		btnInfoTops1Car.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btnInfoTops1Car.addMouseListener((MouseListener) getControlador());
		
		fondoNavbarCargadas = new JLabel("");
		fondoNavbarCargadas.setIcon(new ImageIcon("assets\\navbar.png"));
		fondoNavbarCargadas.setHorizontalAlignment(SwingConstants.CENTER);
		fondoNavbarCargadas.setBounds(0, 0, 552, 40);
		navbarCargadas.add(fondoNavbarCargadas);
		
		
		add(tabbedPane);
		
		panel = new JPanel();
		panel.setBounds(0, 0, 557, 489);
		add(panel);

	}

	public void setearTablaCargadas(ArrayList<Pregunta> preguntasCargadas) {

		for (Pregunta pregunta : preguntasCargadas) {
			Object [] row = {String.valueOf(pregunta.getId_pregunta()), pregunta.getDesarrolloPregunta(), pregunta.getCategoria(), String.valueOf(pregunta.getValidada())};
			this.getModeloTablaCargadas().addRow(row);
		}
	}
	
	public void setearTablaSugeridas(ArrayList<Pregunta> preguntasSugeridas) {

		for (Pregunta pregunta : preguntasSugeridas) {
			Object [] row = {String.valueOf(pregunta.getId_pregunta()), pregunta.getDesarrolloPregunta(), pregunta.getCategoria(), String.valueOf(pregunta.getValidada())};
			this.getModeloTablaSugerida().addRow(row);
		}
	}
	
	public DefaultTableModel getModeloTablaSugerida() {
		return modeloTablaSugerida;
	}

	public void setModeloTablaSugerida(DefaultTableModel modeloTablaSugerida) {
		this.modeloTablaSugerida = modeloTablaSugerida;
	}

	public ControladorPanelListaPreguntas getControlador() {
		return controlador;
	}

	public void setControlador(ControladorPanelListaPreguntas controlador) {
		this.controlador = controlador;
	}

	public DefaultTableModel getModeloTablaCargadas() {
		return modeloTablaCargadas;
	}

	public void setModeloTablaCargadas(DefaultTableModel modeloTablaCargadas) {
		this.modeloTablaCargadas = modeloTablaCargadas;
	}

	public JLabel getBtnCargarPregunta() {
		return btnCargarPregunta;
	}

	public void setBtnCargarPregunta(JLabel btnCargar) {
		this.btnCargarPregunta = btnCargar;
	}

	public JLabel getBtnBorrarPregunta() {
		return btnBorrarPregunta;
	}

	public void setBtnBorrarPregunta(JLabel btnBorrar) {
		this.btnBorrarPregunta = btnBorrar;
	}

	public JTable getTablaSugeridas() {
		return tablaSugeridas;
	}

	public void setTablaSugeridas(JTable tablaSugeridas) {
		this.tablaSugeridas = tablaSugeridas;
	}

	public JTable getTablaCargadas() {
		return tablaCargadas;
	}

	public void setTablaCargadas(JTable tablaCargadas) {
		this.tablaCargadas = tablaCargadas;
	}

	public JLabel getBtnBorrarSugerida() {
		return btnBorrarSugerida;
	}

	public void setBtnBorrarSugerida(JLabel btnBorrarSugerida) {
		this.btnBorrarSugerida = btnBorrarSugerida;
	}

	public JLabel getBtnModificarSugerida() {
		return btnModificarSugerida;
	}

	public void setBtnModificarSugerida(JLabel btnModificarSugerida) {
		this.btnModificarSugerida = btnModificarSugerida;
	}

	public JLabel getBtnOpcionesSugeridas() {
		return btnOpcionesSugeridas;
	}

	public void setBtnOpcionesSugeridas(JLabel btnOpcionesSugeridas) {
		this.btnOpcionesSugeridas = btnOpcionesSugeridas;
	}

	public JLabel getBtnOpcionesCargadas() {
		return btnOpcionesCargadas;
	}

	public void setBtnOpcionesCargadas(JLabel btnOpcionesCargadas) {
		this.btnOpcionesCargadas = btnOpcionesCargadas;
	}

	public JLabel getBtnModificarCargada() {
		return btnModificarCargada;
	}

	public void setBtnModificarCargada(JLabel btnModificarCargada) {
		this.btnModificarCargada = btnModificarCargada;
	}

	public JPanel getNavbarSugeridas() {
		return navbarSugeridas;
	}

	public void setNavbarSugeridas(JPanel navbarSugeridas) {
		this.navbarSugeridas = navbarSugeridas;
	}

	public JLabel getBtnInfoPartPorCategSug() {
		return btnInfoPartPorCategSug;
	}

	public void setBtnInfoPartPorCategSug(JLabel btnInfoPartPorCategSug) {
		this.btnInfoPartPorCategSug = btnInfoPartPorCategSug;
	}

	public JLabel getBtnInfoCompPregSug() {
		return btnInfoCompPregSug;
	}

	public void setBtnInfoCompPregSug(JLabel btnInfoCompPregSug) {
		this.btnInfoCompPregSug = btnInfoCompPregSug;
	}

	public JLabel getBtnInfoTops1Sug() {
		return btnInfoTops1Sug;
	}

	public void setBtnInfoTops1Sug(JLabel btnInfoTops1Sug) {
		this.btnInfoTops1Sug = btnInfoTops1Sug;
	}

	public JLabel getBtnInfoPartPorCategCar() {
		return btnInfoPartPorCategCar;
	}

	public void setBtnInfoPartPorCategCar(JLabel btnInfoPartPorCategCar) {
		this.btnInfoPartPorCategCar = btnInfoPartPorCategCar;
	}

	public JLabel getBtnInfoComparPregCar() {
		return btnInfoComparPregCar;
	}

	public void setBtnInfoComparPregCar(JLabel btnInfoComparPregCar) {
		this.btnInfoComparPregCar = btnInfoComparPregCar;
	}

	public JLabel getBtnInfoTops1Car() {
		return btnInfoTops1Car;
	}

	public void setBtnInfoTops1Car(JLabel btnInfoTops1Car) {
		this.btnInfoTops1Car = btnInfoTops1Car;
	}
	
	
}
