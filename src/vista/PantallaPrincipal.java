package vista;

import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import controlador.ControladorPantallaPrincipal;
import modelo.UsuarioSesion;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.Font;

public class PantallaPrincipal extends JFrame {
	
	private static final long serialVersionUID = 6L;
	
	private JPanel contentPane;
	private ControladorPantallaPrincipal controlador;
	private JLabel btnPerfil;
	private JLabel btnInicio;
	private JLabel btnSugerirPregunta;
	private JLabel btnCerrarSesion;
	private JLabel lblBienvenidoUsuario;
	private JLabel lblNombreUsuario;
	private JLabel btnCerrar;
	private JLabel btnMinimizar;
	private UsuarioSesion usuarioSesion;

	private JTextArea lblRanking;

	private String estadoBtn = "desactivados";
	/**
	 * Create the frame.
	 */
	public PantallaPrincipal(ControladorPantallaPrincipal controlador) {
		this.setControlador(controlador);
		
		setTitle("Preguntados - Inicio");
		setIconImage(Toolkit.getDefaultToolkit().getImage("assets\\icono.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 870, 590);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane); //super.setContentPane(contentPane);
		setUndecorated(true);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		setResizable(false);
		
		btnCerrar = new JLabel("X");
		btnCerrar.setBounds(821, 0, 49, 39);
		btnCerrar.setHorizontalAlignment(SwingConstants.CENTER);
		btnCerrar.setForeground(new Color(255, 255, 255));
		btnCerrar.setFont(new Font("Arial", Font.BOLD, 14));
		btnCerrar.setBackground(new Color(204, 51, 51));
		btnCerrar.addMouseListener(getControlador());
		
		btnMinimizar = new JLabel("\u2212");
		btnMinimizar.setHorizontalAlignment(SwingConstants.CENTER);
		btnMinimizar.setForeground(new Color(255, 255, 255));
		btnMinimizar.setFont(new Font("Arial", Font.BOLD, 24));
		btnMinimizar.addMouseListener(getControlador());
		btnMinimizar.setBounds(775, 0, 49, 39);
		contentPane.add(btnMinimizar);
		contentPane.add(btnCerrar);
		
		//---------------------------------------------------------MENU---------------------------------------------------------//
		
		JPanel panelMenu = new JPanel();
		panelMenu.setBackground(Color.WHITE);
		panelMenu.setBounds(0, 0, 196, 590);
		getContentPane().add(panelMenu);
		panelMenu.setLayout(null);
		
		//----------------------------LBL QUE CONTENDRA EL NOMBRE DEL USUARIO
		lblBienvenidoUsuario = new JLabel("ˇBienvenido!");
		lblBienvenidoUsuario.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblBienvenidoUsuario.setVerticalAlignment(SwingConstants.TOP);
		lblBienvenidoUsuario.setBounds(18, 105, 160, 19);
		panelMenu.add(lblBienvenidoUsuario);
		
		lblNombreUsuario = new JLabel();
		lblNombreUsuario.setVerticalAlignment(SwingConstants.TOP);
		lblNombreUsuario.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNombreUsuario.setBounds(18, 125, 160, 19);
		panelMenu.add(lblNombreUsuario);
		
		lblRanking = new JTextArea();
		lblRanking.setAlignmentY(SwingConstants.TOP);
		lblRanking.setEditable(false);
		lblRanking.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblRanking.setBounds(18, 155, 168, 250);
		panelMenu.add(lblRanking);
		
		JLabel tituloPreguntados = new JLabel("");
		tituloPreguntados.setIcon(new ImageIcon("assets\\logoSidebar.png"));
		tituloPreguntados.setBounds(10, 30, 176, 50);
		panelMenu.add(tituloPreguntados);
		
		//----------------------------BOTONES----------------------------//
		
		//----------------------------BOTON INICIO
		btnInicio = new JLabel("");
		btnInicio.setBackground(new Color(255,255,255));
		btnInicio.setIcon(new ImageIcon("assets\\botones\\sidebar\\" + getEstadoBtn() + "\\menuBtnInicio.png"));
		btnInicio.setBounds(18, 460, 160, 30);
		btnInicio.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnInicio.addMouseListener(getControlador());
		panelMenu.add(btnInicio);
		
		//----------------------------BOTON PERFIL
		btnPerfil = new JLabel("");
		btnPerfil.setBackground(new Color(255,255,255));
		btnPerfil.setIcon(new ImageIcon("assets\\botones\\sidebar\\" + getEstadoBtn() + "\\menuBtnPerfil.png"));
		btnPerfil.setBounds(18, 500, 160, 30);
		btnPerfil.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnPerfil.addMouseListener(getControlador());
		panelMenu.add(btnPerfil);
		
		//----------------------------BOTON SUGERIR PREGUNTA
		btnSugerirPregunta = new JLabel("");
		btnSugerirPregunta.setBackground(new Color(255,255,255));
		btnSugerirPregunta.setBounds(18, 500, 160, 30);
		btnSugerirPregunta.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnSugerirPregunta.addMouseListener(getControlador());
		btnSugerirPregunta.setVisible(false);
		panelMenu.add(btnSugerirPregunta);
		
		//----------------------------BOTON CERRAR SESION		
		btnCerrarSesion = new JLabel("");
		btnCerrarSesion.setBackground(new Color(255,255,255));
		btnCerrarSesion.setIcon(new ImageIcon("assets\\botones\\sidebar\\" + getEstadoBtn() + "\\menuBtnCerrarSesion.png"));
		btnCerrarSesion.setBounds(18, 540, 160, 30);
		btnCerrarSesion.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		btnCerrarSesion.addMouseListener(getControlador());
		panelMenu.add(btnCerrarSesion);
     	
	//---------------------------------------------------------IMG DE FONDO---------------------------------------------------------//
     	JLabel imgFondo = new JLabel("");
     	imgFondo.setIcon(new ImageIcon("assets\\fondo-full.jpg"));
     	imgFondo.setBounds(0, 0, 870, 590);
     	contentPane.add(imgFondo);
     	
     	contentPane.setComponentZOrder(imgFondo, 3);
     	
     	JLabel mousetouched = new JLabel("");
     	mousetouched.setBounds(0, 0, 870, 590);
     	contentPane.add(mousetouched);
		
		mousetouched.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				dragged(e);
			}
		});
		mousetouched.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				pressed(e);
			}
		});
	}

	//---------------------------------------------------------METODOS---------------------------------------------------------//
	public void setearPanel(JPanel panel) {
		contentPane.add(panel);
		contentPane.setComponentZOrder(panel,0);
	}
	
	public void setearBotones(Boolean admin) {
		if (admin) {
			this.getBtnSugerirPregunta().setIcon(new ImageIcon("assets\\botones\\menuBtnCargarPregunta.png"));
		}else {
			this.getBtnSugerirPregunta().setIcon(new ImageIcon("assets\\botones\\menuBtnSugerirPregunta.png"));
		}
	}
	
	int xx, xy;
	protected void pressed(MouseEvent e) {
			xx = e.getX();
			xy = e.getY();
		}
	
	protected void dragged(MouseEvent e) {
			int x = e.getXOnScreen();
			int y = e.getYOnScreen();
			
			this.setLocation(x-xx, y-xy);
	}
	
	//---------------------------------------------------------GETTERS Y SETTERS---------------------------------------------------------//
	public ControladorPantallaPrincipal getControlador() {
		return controlador;
	}

	public void setControlador(ControladorPantallaPrincipal controlador) {
		this.controlador = controlador;
	}

	public JLabel getBtnPerfil() {
		return btnPerfil;
	}

	public void setBtnPerfil(JLabel btnPerfil) {
		this.btnPerfil = btnPerfil;
	}

	public JLabel getBtnCerrarSesion() {
		return btnCerrarSesion;
	}

	public void setBtnCerrarSesion(JLabel btnCerrarSesion) {
		this.btnCerrarSesion = btnCerrarSesion;
	}

	public JLabel getBtnInicio() {
		return btnInicio;
	}

	public void setBtnInicio(JLabel btnInicio) {
		this.btnInicio = btnInicio;
	}

	public JLabel getBtnSugerirPregunta() {
		return btnSugerirPregunta;
	}

	public void setBtnSugerirPregunta(JLabel btnSugerirPregunta) {
		this.btnSugerirPregunta = btnSugerirPregunta;
	}

	public UsuarioSesion getUsuarioSesion() {
		return usuarioSesion;
	}

	public void setUsuarioSesion(UsuarioSesion usuarioSesion) {
		this.usuarioSesion = usuarioSesion;
	}

	public JLabel getBtnMinimizar() {
		return btnMinimizar;
	}

	public void setBtnMinimizar(JLabel btnMinimizar) {
		this.btnMinimizar = btnMinimizar;
	}

	public JLabel getBtnCerrar() {
		return btnCerrar;
	}

	public void setBtnCerrar(JLabel btnCerrar) {
		this.btnCerrar = btnCerrar;
	}

	public JTextArea getLblRanking() {
		return lblRanking;
	}

	public void setLblRanking(JTextArea lblRanking) {
		this.lblRanking = lblRanking;
	}

	public JLabel getLblNombreUsuario() {
		return lblNombreUsuario;
	}

	public void setLblNombreUsuario(JLabel lblNombreUsuario) {
		this.lblNombreUsuario = lblNombreUsuario;
	}

	public String getEstadoBtn() {
		return estadoBtn;
	}

	public void setEstadoBtn(String estadoBtn) {
		this.estadoBtn = estadoBtn;
	}
	
}