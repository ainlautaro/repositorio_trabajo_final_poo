package controlador;

import vista.PantallaJuego;
import vista.PantallaPrincipal;

import java.awt.Cursor;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.security.SecureRandom;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import modelo.OpcionPregunta;
import modelo.OpcionPreguntaDAO;
import modelo.Pregunta;
import modelo.PreguntaDAO;
import modelo.Ranking;
import modelo.UsuarioSesion;

public class ControladorPantallaJuego implements ActionListener, MouseListener{

	private PantallaJuego pantallaJuego;
	private String modoJuego;
	private ControladorPanelInicio controladorPanelInicio;
	private PreguntaDAO pregDAO = new PreguntaDAO();
	private OpcionPreguntaDAO opDAO = new OpcionPreguntaDAO();
	private ArrayList<Pregunta> preguntasJuego;
	private Pregunta preguntaActual;
	private OpcionPregunta op1Actual;
	private OpcionPregunta op2Actual;
	private OpcionPregunta op3Actual;
	private OpcionPregunta op4Actual;
	
	private Integer cantRespuestasCorrectas = 0;
	private Integer cantRespuestasIncorrectas = 0;
	private Integer cantVidas = 3;
	private Integer gano = 0;
	private Integer perdio = 0;
	
	private Ranking ranking_usuario;

	private ControladorPantallaPrincipal controladorPantallaPrincipal;
	
	public ControladorPantallaJuego(String modoJuego) {
		super();
		this.pantallaJuego = new PantallaJuego(this);
		this.pantallaJuego.setVisible(true);
		this.setModoJuego(modoJuego);
		
		cerrarJuego();
		setPreguntasJuego(pregDAO.getAllPorCategoria(modoJuego));
		setearPregunta();
	}
	
	public void setearPregunta() {

		SecureRandom r = new SecureRandom();
		Integer indice = r.nextInt(getPreguntasJuego().size());

		this.getPantallaJuego().setearPregunta(getPreguntasJuego().get(indice).getDesarrolloPregunta());
		
		this.setPreguntaActual(getPreguntasJuego().get(indice));
		this.setearOpciones();
	}

	public void setearOpciones(){
		ArrayList<OpcionPregunta> opciones = getOpDAO().getAllWhereIdPregunta(this.getPreguntaActual().getId_pregunta());
		
		this.getPantallaJuego().getBtnOpcion1().setText(opciones.get(0).getOpcion_pregunta());
		this.setOp1Actual(opciones.get(0));
		
		this.getPantallaJuego().getBtnOpcion2().setText(opciones.get(1).getOpcion_pregunta());
		this.setOp2Actual(opciones.get(1));
		
		this.getPantallaJuego().getBtnOpcion3().setText(opciones.get(2).getOpcion_pregunta());
		this.setOp3Actual(opciones.get(2));
		
		this.getPantallaJuego().getBtnOpcion4().setText(opciones.get(3).getOpcion_pregunta());
		this.setOp4Actual(opciones.get(3));
		
	}

	
	@Override
	public void actionPerformed(ActionEvent e) {
		/*
		 * Color verde: new Color(53,212,82)
		 * Color rojo: new Color(212,79,53)
		 */
		ArrayList<OpcionPregunta> opciones = getOpDAO().getAllWhereIdPregunta(this.getPreguntaActual().getId_pregunta());
		//	OPCION 1
		if (e.getSource().equals(getPantallaJuego().getBtnOpcion1())) {
			if (this.getOp1Actual().getCorrecta()) {	
				this.pantallaJuego.opcionCorrecta(getPantallaJuego().getBtnOpcion1());
				setCantRespuestasCorrectas(getCantRespuestasCorrectas()+1);
			}else {
				setCantVidas(getCantVidas()-1);
				pantallaJuego.opcionIncorrecta(opciones, getCantVidas());
				setCantRespuestasIncorrectas(getCantRespuestasIncorrectas()+1);
			}
		}
		
		//	OPCION 2
		if (e.getSource().equals(getPantallaJuego().getBtnOpcion2())) {
			if (this.getOp2Actual().getCorrecta()) {
				this.pantallaJuego.opcionCorrecta(getPantallaJuego().getBtnOpcion2());
				setCantRespuestasCorrectas(getCantRespuestasCorrectas()+1);
			}else {
				setCantVidas(getCantVidas()-1);
				pantallaJuego.opcionIncorrecta(opciones, getCantVidas());
				setCantRespuestasIncorrectas(getCantRespuestasIncorrectas()+1);
			}
		}
		
		//	OPCION 3
		if (e.getSource().equals(getPantallaJuego().getBtnOpcion3())) {
			if (this.getOp3Actual().getCorrecta()) {
				this.pantallaJuego.opcionCorrecta(getPantallaJuego().getBtnOpcion3());
				setCantRespuestasCorrectas(getCantRespuestasCorrectas()+1);
			}else {
				setCantVidas(getCantVidas()-1);
				pantallaJuego.opcionIncorrecta(opciones, getCantVidas());
				setCantRespuestasIncorrectas(getCantRespuestasIncorrectas()+1);
			}
		}
		
		//	OPCION 4
		if (e.getSource().equals(getPantallaJuego().getBtnOpcion4())) {
			if (this.getOp4Actual().getCorrecta()) {
				this.pantallaJuego.opcionCorrecta(getPantallaJuego().getBtnOpcion4());
				setCantRespuestasCorrectas(getCantRespuestasCorrectas()+1);
			}else {
				setCantVidas(getCantVidas()-1);
				pantallaJuego.opcionIncorrecta(opciones, getCantVidas());
				setCantRespuestasIncorrectas(getCantRespuestasIncorrectas()+1);
			}
		}
		
		siguientePregunta();
		
	}
	
	public void siguientePregunta() {
		
		Thread seteaPreguntaNueva = new Thread() {
				
			public void run() {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
					
				if (getCantVidas() == 0) {
					getPantallaJuego().getPanelJuego().setVisible(false);
					getPantallaJuego().getPanelFinPartida().setVisible(true);
					getPantallaJuego().setearPuntajeFinal(getCantRespuestasCorrectas(), getCantRespuestasIncorrectas(), getPreguntasJuego().size()-1, getCantVidas());
					

					setRanking_usuario(new Ranking(-1,UsuarioSesion.getInstance().getId_usuario(),gano , perdio+1, getModoJuego()));
					System.out.println("Termino el juego, Lo siento... perdiste :(");

					getRanking_usuario().registrarPartida(ranking_usuario);
					
				}else {
					
					if (getPreguntasJuego().size() != 1) {
						
							getPreguntasJuego().remove(getPreguntaActual());
							setearPregunta();

					}else {
						getPantallaJuego().getPanelJuego().setVisible(false);
						getPantallaJuego().getPanelFinPartida().setVisible(true);
						getPantallaJuego().setearPuntajeFinal(getCantRespuestasCorrectas(), getCantRespuestasIncorrectas(), getPreguntasJuego().size()-1, getCantVidas());
						
						setRanking_usuario(new Ranking(-1,UsuarioSesion.getInstance().getId_usuario(),gano+1 , perdio, getModoJuego()));
						System.out.println("Termino el juego, Ganaste!!!");
						ranking_usuario.registrarPartida(ranking_usuario);
					} 			
				}
			}
		};
		
		seteaPreguntaNueva.start();
		
	}
	
	public void cerrarJuego() {
		
		try {
			getPantallaJuego().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			getPantallaJuego().addWindowListener(new WindowAdapter() {
				public void	windowClosing (WindowEvent e) {
					controladorPantallaPrincipal = new ControladorPantallaPrincipal();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource().equals(getPantallaJuego().getBtnVolverInicio())) {
			this.getPantallaJuego().dispose();
			controladorPantallaPrincipal = new ControladorPantallaPrincipal();
		}
		
		if (e.getSource().equals(this.getPantallaJuego().getBtnCerrar())) {
			int resp = JOptionPane.showConfirmDialog(null, "Si abandona la partida, se dar� por perdida �Desea continuar?", modoJuego, JOptionPane.YES_NO_OPTION);
			
			if (resp == 0) {
				setRanking_usuario(new Ranking(-1, UsuarioSesion.getInstance().getId_usuario(), gano, perdio+1, getModoJuego()));
				getRanking_usuario().registrarPartida(ranking_usuario);
				this.getPantallaJuego().dispose();
				controladorPantallaPrincipal = new ControladorPantallaPrincipal();
			}
			
		}
		
		if (e.getSource().equals(this.getPantallaJuego().getBtnMinimizar())) {
			this.getPantallaJuego().setExtendedState(Frame.ICONIFIED);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
		this.getPantallaJuego().getBtnVolverInicio().setCursor(cursor);
		this.getPantallaJuego().getBtnCerrar().setCursor(cursor);
		this.getPantallaJuego().getBtnMinimizar().setCursor(cursor);
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	

	public void setPantallaPrincipal(PantallaPrincipal pantallaPrincipal) {
	}

	public PantallaJuego getPantallaJuego() {
		return pantallaJuego;
	}

	public void setPantallaJuego(PantallaJuego pantallaJuego) {
		this.pantallaJuego = pantallaJuego;
	}

	public String getModoJuego() {
		return modoJuego;
	}

	public void setModoJuego(String modoJuego) {
		this.modoJuego = modoJuego;
	}

	public PreguntaDAO getPregDAO() {
		return pregDAO;
	}

	public void setPregDAO(PreguntaDAO pregDAO) {
		this.pregDAO = pregDAO;
	}

	public OpcionPreguntaDAO getOpDAO() {
		return opDAO;
	}

	public void setOpDAO(OpcionPreguntaDAO opDAO) {
		this.opDAO = opDAO;
	}

	public Pregunta getPreguntaActual() {
		return preguntaActual;
	}

	public void setPreguntaActual(Pregunta preguntaActual) {
		this.preguntaActual = preguntaActual;
	}

	public OpcionPregunta getOp1Actual() {
		return op1Actual;
	}

	public void setOp1Actual(OpcionPregunta op1Actual) {
		this.op1Actual = op1Actual;
	}

	public OpcionPregunta getOp2Actual() {
		return op2Actual;
	}

	public void setOp2Actual(OpcionPregunta op2Actual) {
		this.op2Actual = op2Actual;
	}

	public OpcionPregunta getOp3Actual() {
		return op3Actual;
	}

	public void setOp3Actual(OpcionPregunta op3Actual) {
		this.op3Actual = op3Actual;
	}

	public OpcionPregunta getOp4Actual() {
		return op4Actual;
	}

	public void setOp4Actual(OpcionPregunta op4Actual) {
		this.op4Actual = op4Actual;
	}

	public ArrayList<Pregunta> getPreguntasJuego() {
		return preguntasJuego;
	}

	public void setPreguntasJuego(ArrayList<Pregunta> preguntasJuego) {
		this.preguntasJuego = preguntasJuego;
	}
	
	public Integer getCantRespuestasCorrectas() {
		return cantRespuestasCorrectas;
	}

	public void setCantRespuestasCorrectas(Integer cantRespuestasCorrectas) {
		this.cantRespuestasCorrectas = cantRespuestasCorrectas;
	}

	public Integer getCantRespuestasIncorrectas() {
		return cantRespuestasIncorrectas;
	}

	public void setCantRespuestasIncorrectas(Integer cantRespuestasIncorrectas) {
		this.cantRespuestasIncorrectas = cantRespuestasIncorrectas;
	}

	public Integer getCantVidas() {
		return cantVidas;
	}

	public void setCantVidas(Integer cantVidas) {
		this.cantVidas = cantVidas;
	}

	public Ranking getRanking_usuario() {
		return ranking_usuario;
	}

	public void setRanking_usuario(Ranking ranking_usuario) {
		this.ranking_usuario = ranking_usuario;
	}

	public ControladorPanelInicio getControladorPanelInicio() {
		return controladorPanelInicio;
	}

	public void setControladorPanelInicio(ControladorPanelInicio controladorPanelInicio) {
		this.controladorPanelInicio = controladorPanelInicio;
	}

	public ControladorPantallaPrincipal getControladorPantallaPrincipal() {
		return controladorPantallaPrincipal;
	}

	public void setControladorPantallaPrincipal(ControladorPantallaPrincipal controladorPantallaPrincipal) {
		this.controladorPantallaPrincipal = controladorPantallaPrincipal;
	}
	

}