package controlador;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import modelo.OpcionPregunta;
import modelo.OpcionPreguntaDAO;
import modelo.Pregunta;
import modelo.PreguntaDAO;
import modelo.Reportes;
import vista.PanelListaPreguntas;

public class ControladorPanelListaPreguntas implements ListSelectionListener, MouseListener{
	
	private PanelListaPreguntas panelListaPreguntas;
	private PreguntaDAO pregDAO;
	private OpcionPreguntaDAO opDAO;
	private ControladorModalModificarPregunta modificar;
	private ArrayList<Pregunta> preguntasSugeridas;
	private ArrayList<Pregunta> preguntasCargadas;
	
	public ControladorPanelListaPreguntas() {
		this.panelListaPreguntas = new PanelListaPreguntas(this);
		this.panelListaPreguntas.setVisible(true);
		this.pregDAO = new PreguntaDAO();
		this.opDAO = new OpcionPreguntaDAO();

		limpiarTabla();
		llenarTablas();
		
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
		//--------------------------------BOTON CARGAR PREGUNTA--------------------------------//
		if (e.getSource().equals(this.getPanelListaPreguntas().getBtnCargarPregunta())) {   //debe modificar el atributo validada de la pregunta seleccionada a true
			Integer indice  = this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(); 
			if (indice == -1) {
				JOptionPane.showMessageDialog(this.getPanelListaPreguntas(), "Debe seleccionar una fila para realizar esta acci�n", "ERROR", JOptionPane.ERROR_MESSAGE);
			}else {
				Integer descicion = JOptionPane.showConfirmDialog(this.getPanelListaPreguntas(), "�Est� seguro que desea cargar la pregunta?", "Carga Pregunta", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
				if (descicion == JOptionPane.YES_OPTION) {
					Integer  id 		= Integer.valueOf(this.getPanelListaPreguntas().getTablaSugeridas().getValueAt(this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(), 0).toString()); //Obtengo el id de la columna seleccionada, la columna es marcada con el numero
					String   desarrollo = this.getPanelListaPreguntas().getTablaSugeridas().getValueAt(this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(), 1).toString();
					String   categoria  = this.getPanelListaPreguntas().getTablaSugeridas().getValueAt(this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(), 2).toString();
					Pregunta pregunta   = new Pregunta(id, desarrollo, categoria, true);
					pregDAO.update(pregunta);
					JOptionPane.showMessageDialog(this.getPanelListaPreguntas(), "Pregunta cargada correctamente", "Carga Pregunta", JOptionPane.INFORMATION_MESSAGE);
		
					limpiarTabla();
					llenarTablas();
				}
			}
		}
		
		//--------------------------------BOTON BORRAR PREGUNTA (CARGADA)--------------------------------//
		if (e.getSource().equals(this.getPanelListaPreguntas().getBtnBorrarPregunta())) {  //debe modificar el atributo validada de la pregunta seleccionada a false
			Integer indice  = this.getPanelListaPreguntas().getTablaCargadas().getSelectedRow(); 
			if (indice == -1) {
				JOptionPane.showMessageDialog(this.getPanelListaPreguntas(), "Debe seleccionar una fila para realizar esta acci�n", "ERROR", JOptionPane.ERROR_MESSAGE);
			}else {
				Integer descicion = JOptionPane.showConfirmDialog(this.getPanelListaPreguntas(), "�Est� seguro que desea borrar la pregunta?", "Borrar Pregunta", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
				if (descicion == JOptionPane.YES_OPTION) {
				Integer  id 	    = Integer.valueOf(this.getPanelListaPreguntas().getTablaCargadas().getValueAt(this.getPanelListaPreguntas().getTablaCargadas().getSelectedRow(), 0).toString());//Obtengo el id de la columna seleccionada, la columna es marcada con el numero
				String   desarrollo = this.getPanelListaPreguntas().getTablaCargadas().getValueAt(this.getPanelListaPreguntas().getTablaCargadas().getSelectedRow(), 1).toString();
				String   categoria  = this.getPanelListaPreguntas().getTablaCargadas().getValueAt(this.getPanelListaPreguntas().getTablaCargadas().getSelectedRow(), 2).toString();
				Pregunta pregunta   = new Pregunta(id, desarrollo, categoria, false);
				borrarPregunta(pregunta);
				JOptionPane.showMessageDialog(this.getPanelListaPreguntas(), "Pregunta borrada correctamente", "Borrar Pregunta", JOptionPane.INFORMATION_MESSAGE);			
		
				limpiarTabla();
				llenarTablas();
				}
			}
		}
		
		//--------------------------------BOTON BORRAR PREGUNTA (SUGERIDA)--------------------------------//
		if (e.getSource().equals(this.getPanelListaPreguntas().getBtnBorrarSugerida())) {
			Integer indice  = this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(); 
			if (indice == -1) {
				JOptionPane.showMessageDialog(this.getPanelListaPreguntas(), "Debe seleccionar una fila para realizar esta acci�n", "ERROR", JOptionPane.ERROR_MESSAGE);
			}else {
				Integer descicion = JOptionPane.showConfirmDialog(this.getPanelListaPreguntas(), "�Est� seguro que desea borrar la pregunta?", "Borrar Pregunta", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
				if (descicion == JOptionPane.YES_OPTION) {
					Integer  id 		= Integer.valueOf(this.getPanelListaPreguntas().getTablaSugeridas().getValueAt(this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(), 0).toString()); //Obtengo el id de la columna seleccionada, la columna es marcada con el numero
					String   desarrollo = this.getPanelListaPreguntas().getTablaSugeridas().getValueAt(this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(), 1).toString();
					String   categoria  = this.getPanelListaPreguntas().getTablaSugeridas().getValueAt(this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(), 2).toString();
					Pregunta pregunta   = new Pregunta(id, desarrollo, categoria, true);
					borrarPregunta(pregunta);
					JOptionPane.showMessageDialog(this.getPanelListaPreguntas(), "Pregunta borrada correctamente", "Borrar Pregunta", JOptionPane.INFORMATION_MESSAGE);
		
					limpiarTabla();
					llenarTablas();
				}
			}
		}
		
		//--------------------------------BOTON LISTAR OPCIONES PREGUNTA (CARGADA)--------------------------------//
		if(e.getSource().equals(this.getPanelListaPreguntas().getBtnOpcionesCargadas())) {
			Integer indice  = this.getPanelListaPreguntas().getTablaCargadas().getSelectedRow(); 
			if (indice == -1) {
				JOptionPane.showMessageDialog(this.getPanelListaPreguntas(), "Debe seleccionar una fila para realizar esta acci�n", "ERROR", JOptionPane.ERROR_MESSAGE);
			}else {
				Integer id 		    = Integer.valueOf(this.getPanelListaPreguntas().getTablaCargadas().getValueAt(this.getPanelListaPreguntas().getTablaCargadas().getSelectedRow(), 0).toString());
				String  desarrollo  = this.getPanelListaPreguntas().getTablaCargadas().getValueAt(this.getPanelListaPreguntas().getTablaCargadas().getSelectedRow(), 1).toString();
				String  categoria   = this.getPanelListaPreguntas().getTablaCargadas().getValueAt(this.getPanelListaPreguntas().getTablaCargadas().getSelectedRow(), 2).toString();
				mostrarOpciones(id, desarrollo, categoria);
			}
		}
		
		//--------------------------------BOTON LISTAR OPCIONES PREGUNTA (SUGERIDA)--------------------------------//
		if (e.getSource().equals(this.getPanelListaPreguntas().getBtnOpcionesSugeridas())) {
			Integer indice  = this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(); 
			if (indice == -1) {
				JOptionPane.showMessageDialog(this.getPanelListaPreguntas(), "Debe seleccionar una fila para realizar esta acci�n", "ERROR", JOptionPane.ERROR_MESSAGE);
			}else {
				Integer id 		   = Integer.valueOf(this.getPanelListaPreguntas().getTablaSugeridas().getValueAt(this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(), 0).toString()); //Obtengo el id de la columna seleccionada, la columna es marcada con el numero
				String  desarrollo = this.getPanelListaPreguntas().getTablaSugeridas().getValueAt(this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(), 1).toString();
				String  categoria  = this.getPanelListaPreguntas().getTablaSugeridas().getValueAt(this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(), 2).toString();
				mostrarOpciones(id, desarrollo, categoria);
			}
		}
		
		//--------------------------------BOTON MODIFICAR PREGUNTA (SUGERIDA)--------------------------------//
		if (e.getSource().equals(this.getPanelListaPreguntas().getBtnModificarSugerida())) {
			Integer indice  = this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(); 
			if (indice == -1) {
				JOptionPane.showMessageDialog(this.getPanelListaPreguntas(), "Debe seleccionar una fila para realizar esta acci�n", "ERROR", JOptionPane.ERROR_MESSAGE);
			}else {
				Integer id 		   = Integer.valueOf(this.getPanelListaPreguntas().getTablaSugeridas().getValueAt(this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(), 0).toString());
				ArrayList<OpcionPregunta> opciones = getOpDAO().getAllWhereIdPregunta(id);
				this.modificar = new ControladorModalModificarPregunta(id);
				this.getModificar().getPantalla().getTxtPregunta().setText(this.getPanelListaPreguntas().getTablaSugeridas().getValueAt(this.getPanelListaPreguntas().getTablaSugeridas().getSelectedRow(), 1).toString());
				this.getModificar().getPantalla().getTxtOp1().setText(opciones.get(0).getOpcion_pregunta());
				this.getModificar().getPantalla().getTxtOp2().setText(opciones.get(1).getOpcion_pregunta());
				this.getModificar().getPantalla().getTxtOp3().setText(opciones.get(2).getOpcion_pregunta());
				this.getModificar().getPantalla().getTxtOp4().setText(opciones.get(3).getOpcion_pregunta());
				
				this.getModificar().getPantalla().setVisible(true);
			}
			
			limpiarTabla();
			llenarTablas();
		}
		
		//-------------------------------BOTON MODIFICAR PREGUNTA (CARGADA)---------------------------------//
		if (e.getSource().equals(this.getPanelListaPreguntas().getBtnModificarCargada())) {			
			Integer indice  = this.getPanelListaPreguntas().getTablaCargadas().getSelectedRow(); 
			if (indice == -1) {
				JOptionPane.showMessageDialog(this.getPanelListaPreguntas(), "Debe seleccionar una fila para realizar esta acci�n", "ERROR", JOptionPane.ERROR_MESSAGE);
			}else {
				Integer id 		   = Integer.valueOf(this.getPanelListaPreguntas().getTablaCargadas().getValueAt(this.getPanelListaPreguntas().getTablaCargadas().getSelectedRow(), 0).toString());
				ArrayList<OpcionPregunta> opciones = getOpDAO().getAllWhereIdPregunta(id);
				this.modificar = new ControladorModalModificarPregunta(id);
				this.getModificar().getPantalla().getTxtPregunta().setText(this.getPanelListaPreguntas().getTablaCargadas().getValueAt(this.getPanelListaPreguntas().getTablaCargadas().getSelectedRow(), 1).toString());
				this.getModificar().getPantalla().getTxtOp1().setText(opciones.get(0).getOpcion_pregunta());
				this.getModificar().getPantalla().getTxtOp2().setText(opciones.get(1).getOpcion_pregunta());
				this.getModificar().getPantalla().getTxtOp3().setText(opciones.get(2).getOpcion_pregunta());
				this.getModificar().getPantalla().getTxtOp4().setText(opciones.get(3).getOpcion_pregunta());
				
				this.getModificar().getPantalla().setVisible(true);
			}
			
			limpiarTabla();
			llenarTablas();
		}
		
		//-----------------------------BOTONES PARA MOSTRAR INFORME SOBRE LAS PARTIDAS QUE SE JUGARON POR MODO DE JUEGO--------------------------------//
		if (e.getSource().equals(this.getPanelListaPreguntas().getBtnInfoPartPorCategCar()) || e.getSource().equals(this.getPanelListaPreguntas().getBtnInfoPartPorCategSug())) {
			Reportes re = new Reportes();
			re.reporteJuegosCategoria();
		}
		
		//-------------------BOTONES PARA MOSTRAR INFORME SOBRE LA COMPARACION ENTRE LAS PREGUNTAS SUGERIDAS Y CARGADAS POR MODO DE JUEGO------------------//
		if (e.getSource().equals(this.getPanelListaPreguntas().getBtnInfoComparPregCar()) || e.getSource().equals(this.getPanelListaPreguntas().getBtnInfoCompPregSug())) {
			Reportes re = new Reportes();
			re.reportePreguntasPorModo();
		}
		
		//-------------------BOTONES PARA MOSTRAR INFORME SOBRE LOS MEJORES JUGADORES POR MODO DE JUEGO-----------------------//
		if (e.getSource().equals(this.getPanelListaPreguntas().getBtnInfoTops1Car()) || e.getSource().equals(this.getPanelListaPreguntas().getBtnInfoTops1Sug())) {
			Reportes re = new Reportes();
			re.reporteMejoresJugadores();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
		
		if (e.getSource().equals(getPanelListaPreguntas().getBtnCargarPregunta())) {
			this.getPanelListaPreguntas().getBtnCargarPregunta().setCursor(cursor);
			this.getPanelListaPreguntas().getBtnCargarPregunta().setForeground(new Color(0,0,0));
		}
		
		if (e.getSource().equals(getPanelListaPreguntas().getBtnBorrarPregunta())) {
			this.getPanelListaPreguntas().getBtnBorrarPregunta().setCursor(cursor);
			this.getPanelListaPreguntas().getBtnBorrarPregunta().setForeground(new Color(0,0,0));	
		}
		
		if (e.getSource().equals(getPanelListaPreguntas().getBtnBorrarSugerida())) {
			this.getPanelListaPreguntas().getBtnBorrarSugerida().setCursor(cursor);
			this.getPanelListaPreguntas().getBtnBorrarSugerida().setForeground(new Color(0,0,0));	
		}
		
		if (e.getSource().equals(getPanelListaPreguntas().getBtnModificarCargada())) {
			this.getPanelListaPreguntas().getBtnModificarCargada().setCursor(cursor);
			this.getPanelListaPreguntas().getBtnModificarCargada().setForeground(new Color(0,0,0));	
		}
		
		if (e.getSource().equals(getPanelListaPreguntas().getBtnModificarSugerida())) {
			this.getPanelListaPreguntas().getBtnModificarSugerida().setCursor(cursor);
			this.getPanelListaPreguntas().getBtnModificarSugerida().setForeground(new Color(0,0,0));
		}
		
		if (e.getSource().equals(getPanelListaPreguntas().getBtnOpcionesCargadas())) {
			this.getPanelListaPreguntas().getBtnOpcionesCargadas().setCursor(cursor);
			this.getPanelListaPreguntas().getBtnOpcionesCargadas().setForeground(new Color(0,0,0));
		}
		
		if (e.getSource().equals(getPanelListaPreguntas().getBtnOpcionesSugeridas())) {
			this.getPanelListaPreguntas().getBtnOpcionesSugeridas().setCursor(cursor);
			this.getPanelListaPreguntas().getBtnOpcionesSugeridas().setForeground(new Color(0,0,0));
		}
		
		
		
		if (e.getSource().equals(getPanelListaPreguntas().getBtnInfoCompPregSug())) {
			this.getPanelListaPreguntas().getBtnInfoCompPregSug().setCursor(cursor);
			this.getPanelListaPreguntas().getBtnInfoCompPregSug().setForeground(new Color(0,0,0));
		}
		
		if (e.getSource().equals(getPanelListaPreguntas().getBtnInfoPartPorCategSug())) {
			this.getPanelListaPreguntas().getBtnInfoPartPorCategSug().setCursor(cursor);
			this.getPanelListaPreguntas().getBtnInfoPartPorCategSug().setForeground(new Color(0,0,0));
		}
		
		if (e.getSource().equals(getPanelListaPreguntas().getBtnInfoTops1Sug())) {
			this.getPanelListaPreguntas().getBtnInfoTops1Sug().setCursor(cursor);
			this.getPanelListaPreguntas().getBtnInfoTops1Sug().setForeground(new Color(0,0,0));
		}
		
		if (e.getSource().equals(getPanelListaPreguntas().getBtnInfoPartPorCategCar())) {
			this.getPanelListaPreguntas().getBtnInfoPartPorCategCar().setCursor(cursor);
			this.getPanelListaPreguntas().getBtnInfoPartPorCategCar().setForeground(new Color(0,0,0));
		}
		
		if (e.getSource().equals(getPanelListaPreguntas().getBtnInfoComparPregCar())) {
			this.getPanelListaPreguntas().getBtnInfoComparPregCar().setCursor(cursor);
			this.getPanelListaPreguntas().getBtnInfoComparPregCar().setForeground(new Color(0,0,0));
		}
		
		if (e.getSource().equals(getPanelListaPreguntas().getBtnInfoTops1Car())) {
			this.getPanelListaPreguntas().getBtnInfoTops1Car().setCursor(cursor);
			this.getPanelListaPreguntas().getBtnInfoTops1Car().setForeground(new Color(0,0,0));
		}
	
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
		this.getPanelListaPreguntas().getBtnCargarPregunta().setForeground(new Color(255,255,255));
		
		this.getPanelListaPreguntas().getBtnBorrarPregunta().setForeground(new Color(255,255,255));
		
		this.getPanelListaPreguntas().getBtnBorrarSugerida().setForeground(new Color(255,255,255));
		
		this.getPanelListaPreguntas().getBtnModificarCargada().setForeground(new Color(255,255,255));
		
		this.getPanelListaPreguntas().getBtnModificarSugerida().setForeground(new Color(255,255,255));
		
		this.getPanelListaPreguntas().getBtnOpcionesCargadas().setForeground(new Color(255,255,255));
		
		this.getPanelListaPreguntas().getBtnOpcionesSugeridas().setForeground(new Color(255,255,255));
		
		
		
		this.getPanelListaPreguntas().getBtnInfoComparPregCar().setForeground(new Color(255,255,255));
		this.getPanelListaPreguntas().getBtnInfoPartPorCategCar().setForeground(new Color(255,255,255));
		this.getPanelListaPreguntas().getBtnInfoTops1Car().setForeground(new Color(255,255,255));
		
		this.getPanelListaPreguntas().getBtnInfoCompPregSug().setForeground(new Color(255,255,255));
		this.getPanelListaPreguntas().getBtnInfoPartPorCategSug().setForeground(new Color(255,255,255));
		this.getPanelListaPreguntas().getBtnInfoTops1Sug().setForeground(new Color(255,255,255));
		
	}

	private void llenarTablas() {
		setPreguntasCargadas(pregDAO.getAllPorValidada(true));
		setPreguntasSugeridas(pregDAO.getAllPorValidada(false));
		
		this.getPanelListaPreguntas().setearTablaCargadas(preguntasCargadas);
		this.getPanelListaPreguntas().setearTablaSugeridas(preguntasSugeridas);
	}
	
	private void limpiarTabla() {
		this.getPanelListaPreguntas().getModeloTablaSugerida().setRowCount(0);
    	this.getPanelListaPreguntas().getModeloTablaCargadas().setRowCount(0);
	}
	
	public void borrarPregunta(Pregunta pregunta) {
		
		ArrayList<OpcionPregunta> opciones = getOpDAO().getAllWhereIdPregunta(pregunta.getId_pregunta());
		for (OpcionPregunta opcionPregunta : opciones) {
			opDAO.delete(opcionPregunta);
		}
		pregDAO.delete(pregunta);
		
	}
	
	public void mostrarOpciones(Integer idPregunta, String desarrollo, String categoria) {
		ArrayList<OpcionPregunta> opciones = getOpDAO().getAllWhereIdPregunta(idPregunta);
		String mnsj = "Pregunta: " + desarrollo + "\n" +
					  "Categoria: " + categoria + "\n\n" +
					  "Opciones: \n\n";
		for (int i = 0; i < opciones.size(); i++) {
			if (opciones.get(i).getCorrecta()) {
				mnsj = mnsj +  "- " + opciones.get(i).getOpcion_pregunta() + "\t (Respuesta correcta)" + "\n";
			} else {
			mnsj = mnsj + "- " + opciones.get(i).getOpcion_pregunta() + "\n";
			}
		}
		JOptionPane.showMessageDialog(null, mnsj, "Informaci�n completa", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public ArrayList<Pregunta> getPreguntasSugeridas() {
		return preguntasSugeridas;
	}

	public void setPreguntasSugeridas(ArrayList<Pregunta> preguntasSugeridas) {
		this.preguntasSugeridas = preguntasSugeridas;
	}

	public ArrayList<Pregunta> getPreguntasCargadas() {
		return preguntasCargadas;
	}

	public void setPreguntasCargadas(ArrayList<Pregunta> preguntasCargadas) {
		this.preguntasCargadas = preguntasCargadas;
	}
	
	public PanelListaPreguntas getPanelListaPreguntas() {
		return panelListaPreguntas;
	}

	public void setPanelListaPreguntas(PanelListaPreguntas panelListaPreguntas) {
		this.panelListaPreguntas = panelListaPreguntas;
	}

	public PreguntaDAO getPregDAO() {
		return pregDAO;
	}

	public void setPregDAO(PreguntaDAO pregDAO) {
		this.pregDAO = pregDAO;
	}

	public OpcionPreguntaDAO getOpDAO() {
		return opDAO;
	}

	public void setOpDAO(OpcionPreguntaDAO opDAO) {
		this.opDAO = opDAO;
	}

	public ControladorModalModificarPregunta getModificar() {
		return modificar;
	}

	public void setModificar(ControladorModalModificarPregunta modificar) {
		this.modificar = modificar;
	}


}
