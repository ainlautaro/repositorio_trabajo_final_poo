package controlador;

import java.awt.Cursor;
import java.awt.Frame;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import modelo.Database;
import modelo.UsuarioRanking;
import modelo.UsuarioSesion;
import vista.PantallaPrincipal;

public class ControladorPantallaPrincipal implements MouseListener{
	
	private PantallaPrincipal pantallaContenedorPaneles;
	
	private ControladorPanelInicio controladorPI;
	private ControladorPanelPerfilUsuario controladorPFU;
	private ControladorPanelSugerirPregunta controladorPSP;
	private ControladorPantallaLogin controladorPantallaLogin;
	private ControladorPanelListaPreguntas controladorPLP;
	
	private UsuarioSesion usuarioSesion;
	private UsuarioRanking usuarioRanking = new UsuarioRanking(UsuarioSesion.getInstance().getNombre());
	
	public ControladorPantallaPrincipal() {
		super();
		this.pantallaContenedorPaneles = new PantallaPrincipal(this);
		this.getPantallaContenedorPaneles().setVisible(true);
		
		controladorPI = new ControladorPanelInicio(pantallaContenedorPaneles);
		pantallaContenedorPaneles.setearPanel(controladorPI.getPanelInicio());
		
		this.getPantallaContenedorPaneles().setearBotones(UsuarioSesion.getInstance().getAdmin());
		this.getPantallaContenedorPaneles().getLblNombreUsuario().setText(UsuarioSesion.getInstance().getNombre());
		this.getPantallaContenedorPaneles().getLblRanking().setText(rankingUsuarioSesion());
	}

	
	public String rankingUsuarioSesion() {
		ArrayList<UsuarioRanking> tabla = new ArrayList<>();
		
		tabla = getUsuarioRanking().posicionRanking(getUsuarioRanking().getNombre());
		
		String ranking = "";
		
		for (UsuarioRanking usuarioRanking : tabla) {

			ranking = (ranking  +
					   usuarioRanking.getModo_juego() + ": #" + Integer.toString(usuarioRanking.getRanking()) +
					   System.lineSeparator() +
					   "PG: " + usuarioRanking.getPartidas_ganadas() + " " + "PP: " +usuarioRanking.getPartidas_perdidas() + 
					   System.lineSeparator() + 
					   System.lineSeparator());
		}
		
		return ranking;

	}
	
	//----------------------------METODOS MOUSE LISTENER----------------------------//
	
	@Override
	public void mouseEntered(MouseEvent e) {	//Se usa para indicar que el puntero del mouse entro al area del objeto.
		// TODO Auto-generated method stub
			Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
			getPantallaContenedorPaneles().getBtnInicio().setCursor(cursor);
			getPantallaContenedorPaneles().getBtnPerfil().setCursor(cursor);
			getPantallaContenedorPaneles().getBtnSugerirPregunta().setCursor(cursor);
			getPantallaContenedorPaneles().getBtnCerrarSesion().setCursor(cursor);
			getPantallaContenedorPaneles().getBtnCerrar().setCursor(cursor);
			getPantallaContenedorPaneles().getBtnMinimizar().setCursor(cursor);
	}

	@Override
	public void mouseExited(MouseEvent e) {		//Se usa para indicar que el puntero del mouse salio del area del objeto
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {	//Se usa para indicar que se presiono el objeto
		
		if (e.getSource().equals(this.getPantallaContenedorPaneles().getBtnCerrar())) {
			this.getPantallaContenedorPaneles().dispose();
		}
		
		if (e.getSource().equals(this.getPantallaContenedorPaneles().getBtnMinimizar())) {
			this.getPantallaContenedorPaneles().setExtendedState(Frame.ICONIFIED);
		}
		
		//IF PARA EL BTN INICIO
			if (e.getSource().equals(getPantallaContenedorPaneles().getBtnInicio())) {
				
				controladorPI.getPanelInicio().setVisible(true);
				
				if (controladorPFU != null) {
					controladorPFU.getPanelPerfilUsuario().setVisible(false);
				}
				
				if (controladorPSP != null) {
					controladorPSP.getPanelSugerirPregunta().setVisible(false);
				}
				
				if (controladorPLP != null) {
					controladorPLP.getPanelListaPreguntas().setVisible(false);
				}
				
				this.getPantallaContenedorPaneles().getBtnSugerirPregunta().setVisible(false);
				this.getPantallaContenedorPaneles().getBtnInicio().setBounds(18, 460, 160, 30);
				this.getPantallaContenedorPaneles().getBtnPerfil().setBounds(18, 500, 160, 30);
			}
		
		//IF PARA EL BTN PERFIL
		if (e.getSource().equals(getPantallaContenedorPaneles().getBtnPerfil())) {
			
			if(controladorPFU == null) {
				controladorPFU = new ControladorPanelPerfilUsuario(pantallaContenedorPaneles);
			}
			
			this.getPantallaContenedorPaneles().add(controladorPFU.getPanelPerfilUsuario());
			pantallaContenedorPaneles.setearPanel(controladorPFU.getPanelPerfilUsuario());
			
			controladorPFU.getPanelPerfilUsuario().setVisible(true);
			controladorPI.getPanelInicio().setVisible(false);
			
			if (controladorPSP != null) {
				controladorPSP.getPanelSugerirPregunta().setVisible(false);
			}
			
			if (controladorPLP != null) {
				controladorPLP.getPanelListaPreguntas().setVisible(false);
			}
			
			this.getPantallaContenedorPaneles().getBtnSugerirPregunta().setVisible(true);
			this.getPantallaContenedorPaneles().getBtnInicio().setBounds(18, 420, 160, 30);
			this.getPantallaContenedorPaneles().getBtnPerfil().setBounds(18, 460, 160, 30);
		}
		
		//IF PARA EL BTN SUGERIR PREGUNTA
		if (!UsuarioSesion.getInstance().getAdmin()) {
			if (e.getSource().equals(getPantallaContenedorPaneles().getBtnSugerirPregunta())) {
				
				if (controladorPSP == null) {
					controladorPSP = new ControladorPanelSugerirPregunta();
				}
				
				this.getPantallaContenedorPaneles().add(controladorPSP.getPanelSugerirPregunta());
				pantallaContenedorPaneles.setearPanel(controladorPSP.getPanelSugerirPregunta());
				
				controladorPSP.getPanelSugerirPregunta().setVisible(true);
				controladorPI.getPanelInicio().setVisible(false);
				controladorPFU.getPanelPerfilUsuario().setVisible(false);

			}
			
			//IF PARA EL BTN CERRAR SESION
			if (e.getSource().equals(getPantallaContenedorPaneles().getBtnCerrarSesion())) {
				pantallaContenedorPaneles.dispose();
				controladorPantallaLogin = new ControladorPantallaLogin();
			}
		}else {
			if (e.getSource().equals(getPantallaContenedorPaneles().getBtnSugerirPregunta())) {
				
				if (controladorPLP == null) {
					controladorPLP = new ControladorPanelListaPreguntas();
				}
				
				this.getPantallaContenedorPaneles().add(controladorPLP.getPanelListaPreguntas());
				pantallaContenedorPaneles.setearPanel(controladorPLP.getPanelListaPreguntas());
				
				controladorPLP.getPanelListaPreguntas().setVisible(true);
				controladorPI.getPanelInicio().setVisible(false);
				controladorPFU.getPanelPerfilUsuario().setVisible(false);

			}
			
			//IF PARA EL BTN CERRAR SESION
			if (e.getSource().equals(getPantallaContenedorPaneles().getBtnCerrarSesion())) {
				pantallaContenedorPaneles.dispose();
				controladorPantallaLogin = new ControladorPantallaLogin();
				Database.getInstance().cerrarConexion();
			}
		}
		
}
	
	@Override
	public void mouseReleased(MouseEvent e) {	//se usa para indicar que el mouse solt� el objeto
		// TODO Auto-generated method stub
		
	}
	
	@Override 
	public void mouseClicked(MouseEvent e) {	//se usa para indicar que el mouse realizo un clic
		// TODO Auto-generated method stub
		
	}
	
	//----------------------------GETTERS Y SETTERS----------------------------//
	public PantallaPrincipal getPantallaContenedorPaneles() {
		return pantallaContenedorPaneles;
	}

	public void setPantallaContenedorPaneles(PantallaPrincipal pantallaContenedorPaneles) {
		this.pantallaContenedorPaneles = pantallaContenedorPaneles;
	}

	public ControladorPantallaLogin getControladorPantallaLogin() {
		return controladorPantallaLogin;
	}

	public void setControladorPantallaLogin(ControladorPantallaLogin controladorPantallaLogin) {
		this.controladorPantallaLogin = controladorPantallaLogin;
	}



	public UsuarioSesion getUsuarioSesion() {
		return usuarioSesion;
	}



	public void setUsuarioSesion(UsuarioSesion usuarioSesion) {
		this.usuarioSesion = usuarioSesion;
	}



	public ControladorPanelListaPreguntas getControladorPLP() {
		return controladorPLP;
	}



	public void setControladorPLP(ControladorPanelListaPreguntas controladorPLP) {
		this.controladorPLP = controladorPLP;
	}


	public UsuarioRanking getUsuarioRanking() {
		return usuarioRanking;
	}


	public void setUsuarioRanking(UsuarioRanking usuarioRanking) {
		this.usuarioRanking = usuarioRanking;
	}

}
