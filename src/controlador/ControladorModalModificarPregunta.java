package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Optional;

import javax.swing.JOptionPane;

import modelo.Administrador;
import modelo.OpcionPregunta;
import modelo.OpcionPreguntaDAO;
import modelo.Pregunta;
import modelo.PreguntaDAO;
import modelo.UsuarioSesion;
import vista.ModalModificarPregunta;

public class ControladorModalModificarPregunta implements ActionListener {

	private ModalModificarPregunta pantalla;
	private UsuarioSesion usuarioSesion;
	private Administrador admin = new Administrador(UsuarioSesion.getInstance().getId_usuario(), 
													UsuarioSesion.getInstance().getNombre(), 
													UsuarioSesion.getInstance().getPassword(), 
													UsuarioSesion.getInstance().getEmail(), 
													UsuarioSesion.getInstance().getFotoPerfil(), 
													UsuarioSesion.getInstance().getAdmin());
	private PreguntaDAO pregDAO;
	private OpcionPreguntaDAO opcPregDAO;
	private Integer idPregunta;
	
	public ControladorModalModificarPregunta(Integer idPregunta) {
		this.setIdPregunta(idPregunta);
		this.pantalla = new ModalModificarPregunta(this);
		
		this.pregDAO = new PreguntaDAO();
		this.opcPregDAO = new OpcionPreguntaDAO();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String pregunta = this.getPantalla().getTxtPregunta().getText();
		String opcion1	= this.getPantalla().getTxtOp1().getText();
		String opcion2  = this.getPantalla().getTxtOp2().getText();
		String opcion3  = this.getPantalla().getTxtOp3().getText();
		String opcion4  = this.getPantalla().getTxtOp4().getText();
		
		if (e.getSource().equals(this.getPantalla().getBtnGuardar())) {
			
			Optional<Pregunta> preg = this.getPregDAO().get(idPregunta);
			
			Pregunta preguntaModificada = new Pregunta(preg.get().getId_pregunta(), 
													   pregunta, 
													   preg.get().getCategoria(), 
													   preg.get().getValidada());
			
			ArrayList<OpcionPregunta> opciones = getOpcPregDAO().getAllWhereIdPregunta(idPregunta);
			
			OpcionPregunta op1 = new OpcionPregunta(opciones.get(0).getId_opcion(),
													opciones.get(0).getPregunta_id(), 
													opcion1, 
													opciones.get(0).getCorrecta());
			
			OpcionPregunta op2 = new OpcionPregunta(opciones.get(1).getId_opcion(),
													opciones.get(1).getPregunta_id(), 
													opcion2, 
													opciones.get(1).getCorrecta());
			
			OpcionPregunta op3 = new OpcionPregunta(opciones.get(2).getId_opcion(),
													opciones.get(2).getPregunta_id(), 
													opcion3, 
													opciones.get(2).getCorrecta());
			
			OpcionPregunta op4 = new OpcionPregunta(opciones.get(3).getId_opcion(),
													opciones.get(3).getPregunta_id(), 
													opcion4, 
													opciones.get(3).getCorrecta());
			
			getAdmin().actualizarPregunta(preguntaModificada, op1, op2, op3, op4);
			
			JOptionPane.showMessageDialog(this.getPantalla(), "La modificación fue realizada correctamente", "Pregunta modificada", JOptionPane.INFORMATION_MESSAGE);
			getPantalla().dispose();
		}
		
		if (e.getSource().equals(this.getPantalla().getBtnCancelar())) {
			getPantalla().dispose();
		}
	}
	
	public void modificacionRealizada() {
		
	}

	public ModalModificarPregunta getPantalla() {
		return pantalla;
	}

	public void setPantalla(ModalModificarPregunta pantalla) {
		this.pantalla = pantalla;
	}

	public PreguntaDAO getPregDAO() {
		return pregDAO;
	}

	public void setPregDAO(PreguntaDAO pregDAO) {
		this.pregDAO = pregDAO;
	}

	public OpcionPreguntaDAO getOpcPregDAO() {
		return opcPregDAO;
	}

	public void setOpcPregDAO(OpcionPreguntaDAO opcPregDAO) {
		this.opcPregDAO = opcPregDAO;
	}

	public Integer getIdPregunta() {
		return idPregunta;
	}

	public void setIdPregunta(Integer idPregunta) {
		this.idPregunta = idPregunta;
	}

	public UsuarioSesion getUsuarioSesion() {
		return usuarioSesion;
	}

	public void setUsuarioSesion(UsuarioSesion usuarioSesion) {
		this.usuarioSesion = usuarioSesion;
	}

	public Administrador getAdmin() {
		return admin;
	}

	public void setAdmin(Administrador admin) {
		this.admin = admin;
	}
}
