package controlador;

import java.util.ArrayList;
import modelo.RankingDAO;
import modelo.UsuarioRanking;
import vista.ModalRanking;

public class ControladorModalRanking {
	private ModalRanking modal;
	private RankingDAO rkgDao;
	
	private ArrayList<UsuarioRanking> arrayArte;
	private ArrayList<UsuarioRanking> arrayCiencia;
	private ArrayList<UsuarioRanking> arrayEntretenimiento;
	private ArrayList<UsuarioRanking> arrayDeportes;
	private ArrayList<UsuarioRanking> arrayHistoria;
	private ArrayList<UsuarioRanking> arrayGeografia;
	private ArrayList<UsuarioRanking> arrayRandom;

	
	public ControladorModalRanking() {
		this.modal = new ModalRanking(this);
		this.rkgDao = new RankingDAO();
		llenarTablas();
	this.modal.setVisible(true);


	}

	private void llenarTablas() {
		setArrayArte(rkgDao.getCategoria("Arte"));
		setArrayCiencia(rkgDao.getCategoria("Ciencia"));
		setArrayEntretenimiento(rkgDao.getCategoria("Entretenimiento"));
		setArrayDeportes(rkgDao.getCategoria("Deportes"));
		setArrayHistoria(rkgDao.getCategoria("Historia"));
		setArrayGeografia(rkgDao.getCategoria("Geografia"));
		setArrayRandom(rkgDao.getCategoria("Random"));

		
		
		this.getModal().setearTablaArte(arrayArte);
		this.getModal().setearTablaCiencia(arrayCiencia);
		this.getModal().setearTablaEntretenimiento(arrayEntretenimiento);
		this.getModal().setearTablaDeporte(arrayDeportes);
		this.getModal().setearTablaHistoria(arrayHistoria);
		this.getModal().setearTablaGeografia(arrayGeografia);
		this.getModal().setearTablaRandom(arrayRandom);
	}

	public RankingDAO getRkgDao() {
		return rkgDao;
	}
	public void setRkgDao(RankingDAO rkgDao) {
		this.rkgDao = rkgDao;
	}
	public ArrayList<UsuarioRanking> getArrayArte() {
		return arrayArte;
	}
	public void setArrayArte(ArrayList<UsuarioRanking> arrayArte) {
		this.arrayArte = arrayArte;
	}
	public ArrayList<UsuarioRanking> getArrayCiencia() {
		return arrayCiencia;
	}
	public void setArrayCiencia(ArrayList<UsuarioRanking> arrayCiencia) {
		this.arrayCiencia = arrayCiencia;
	}
	public ArrayList<UsuarioRanking> getArrayEntretenimiento() {
		return arrayEntretenimiento;
	}
	public void setArrayEntretenimiento(ArrayList<UsuarioRanking> arrayEntretenimiento) {
		this.arrayEntretenimiento = arrayEntretenimiento;
	}
	public ArrayList<UsuarioRanking> getArrayDeportes() {
		return arrayDeportes;
	}
	public void setArrayDeportes(ArrayList<UsuarioRanking> arrayDeportes) {
		this.arrayDeportes = arrayDeportes;
	}
	public ArrayList<UsuarioRanking> getArrayHistoria() {
		return arrayHistoria;
	}
	public void setArrayHistoria(ArrayList<UsuarioRanking> arrayHistoria) {
		this.arrayHistoria = arrayHistoria;
	}
	public ArrayList<UsuarioRanking> getArrayGeografia() {
		return arrayGeografia;
	}
	public void setArrayGeografia(ArrayList<UsuarioRanking> arrayGeografia) {
		this.arrayGeografia = arrayGeografia;
	}
	public ArrayList<UsuarioRanking> getArrayRandom() {
		return arrayRandom;
	}
	public void setArrayRandom(ArrayList<UsuarioRanking> arrayRandom) {
		this.arrayRandom = arrayRandom;
	}
	public ModalRanking getModal() {
		return modal;
	}
	public void setModal(ModalRanking modal) {
		this.modal = modal;
	}
	
}