package controlador;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;

import modelo.ExcepcionUsuarioYaRegistrado;
import modelo.Ranking;
import modelo.RankingDAO;
import modelo.ExcepcionFormatoCorreoIncorrecto;
import modelo.Usuario;
import modelo.UsuarioDAO;
import vista.PantallaRegistro;

public class ControladorPantallaRegistro implements ActionListener, MouseListener {

	private PantallaRegistro vistaPantallaRegistro;
	private ControladorPantallaLogin controladorLogin;
	private Usuario usuario;
	private UsuarioDAO usuarioDAO;
	private RankingDAO rankingDAO;
	private Ranking ranking;
	private Integer numero = 0;

	public ControladorPantallaRegistro() {
		super();
		this.vistaPantallaRegistro = new PantallaRegistro(this);
		this.vistaPantallaRegistro.setVisible(true);
		
		this.usuarioDAO = new UsuarioDAO();
		this.rankingDAO = new RankingDAO();
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// se determina si el boton presionado es "REGISTRARSE"
		if (e.getSource().equals(getVistaPantallaRegistro().getBtnRegistrarse())) {
			
			//Integer id_usuario = getUsuarioDAO().maxID()+1;
			String nombre = getVistaPantallaRegistro().getTextFieldUsuario().getText();
			String email = getVistaPantallaRegistro().getTextFieldEmail().getText();
			String confirmarEmail = getVistaPantallaRegistro().getTextFieldConfirmarEmail().getText();
			String password = getVistaPantallaRegistro().getTextFieldPassword().getText();
			String confirmarPassword = getVistaPantallaRegistro().getTextFieldConfirmarPassword().getText();
			//String fotoPerfil = "";

			if (nombre.equals("")) {
				this.getVistaPantallaRegistro().getTextFieldUsuario().setBorder(new LineBorder(Color.RED));
				vistaPantallaRegistro.getUsuarioObligatorio().setVisible(true);
			} else {
				this.getVistaPantallaRegistro().getTextFieldUsuario().setBorder(new LineBorder(Color.GREEN));
				vistaPantallaRegistro.getUsuarioObligatorio().setVisible(false);
			}

			if (!email.equals(confirmarEmail) || email.equals("")) {
				this.getVistaPantallaRegistro().getTextFieldConfirmarEmail().setBorder(new LineBorder(Color.RED));
				this.getVistaPantallaRegistro().getTextFieldEmail().setBorder(new LineBorder(Color.RED));
				vistaPantallaRegistro.getEmailDistinto().setVisible(true);
			} else {
				this.getVistaPantallaRegistro().getTextFieldConfirmarEmail().setBorder(new LineBorder(Color.GREEN));
				this.getVistaPantallaRegistro().getTextFieldEmail().setBorder(new LineBorder(Color.GREEN));
				vistaPantallaRegistro.getEmailDistinto().setVisible(false);
			}

			if (!password.equals(confirmarPassword) || password.equals("")) {
				this.getVistaPantallaRegistro().getTextFieldConfirmarPassword().setBorder(new LineBorder(Color.RED));
				this.getVistaPantallaRegistro().getTextFieldPassword().setBorder(new LineBorder(Color.RED));
				vistaPantallaRegistro.getPasswordDistinto().setVisible(true);
			} else {
				this.getVistaPantallaRegistro().getTextFieldConfirmarPassword().setBorder(new LineBorder(Color.GREEN));
				this.getVistaPantallaRegistro().getTextFieldPassword().setBorder(new LineBorder(Color.GREEN));
				vistaPantallaRegistro.getPasswordDistinto().setVisible(false);
			}

			if (email.equals(confirmarEmail) && !email.equals("") && password.equals(confirmarPassword) && !password.equals("") && !nombre.equals("")) {
				
				try {
					try {
						this.setUsuario(new Usuario(nombre, password, email));
						vistaPantallaRegistro.getErrorEmail().setVisible(false);
						usuarioDAO.save(usuario);
						setRanking(new Ranking(usuario.buscaID(usuario.getNombre()), numero, numero, "No inicializo"));
						rankingDAO.save(ranking);
						JOptionPane.showMessageDialog(this.getVistaPantallaRegistro(), "Usuario registrado con �xito", "Registro de usuario", JOptionPane.INFORMATION_MESSAGE);
					} catch (ExcepcionFormatoCorreoIncorrecto e1) {
						this.getVistaPantallaRegistro().getTextFieldConfirmarEmail().setBorder(new LineBorder(Color.RED));
						this.getVistaPantallaRegistro().getTextFieldEmail().setBorder(new LineBorder(Color.RED));
						vistaPantallaRegistro.getEmailDistinto().setVisible(true);
						vistaPantallaRegistro.getErrorEmail().setVisible(true);
					}
				} catch (ExcepcionUsuarioYaRegistrado e1) {
					this.getVistaPantallaRegistro().getTextFieldUsuario().setBorder(new LineBorder(Color.RED));
					vistaPantallaRegistro.getUsuarioObligatorio().setVisible(true);
				}
			}		
		}

		// se determina si el boton presionado es "BtnVolverLogin"
		if (e.getSource().equals(getVistaPantallaRegistro().getBtnVolverLogin())) {
			getVistaPantallaRegistro().setVisible(false);
			controladorLogin = new ControladorPantallaLogin();
			this.getVistaPantallaRegistro().getTextFieldConfirmarEmail().setBorder(new LineBorder(Color.RED));
			this.getVistaPantallaRegistro().getTextFieldConfirmarPassword().setBorder(new LineBorder(Color.RED));
			
			
		}

	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource().equals(this.getVistaPantallaRegistro().getBtnCerrar())) {
			this.getVistaPantallaRegistro().dispose();
		}
		
		if (e.getSource().equals(this.getVistaPantallaRegistro().getBtnMinimizar())) {
			this.getVistaPantallaRegistro().setExtendedState(Frame.ICONIFIED);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
		
		if (e.getSource().equals(this.getVistaPantallaRegistro().getBtnCerrar())) {
			this.getVistaPantallaRegistro().getBtnCerrar().setCursor(cursor);
			this.getVistaPantallaRegistro().getBtnCerrar().setBackground(Color.red);
		}
		
		if (e.getSource().equals(this.getVistaPantallaRegistro().getBtnMinimizar())) {
			this.getVistaPantallaRegistro().getBtnMinimizar().setCursor(cursor);
		}
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource().equals(this.getVistaPantallaRegistro().getBtnCerrar())) {
			this.getVistaPantallaRegistro().getBtnCerrar().setBackground(null);
		}
		
		if (e.getSource().equals(this.getVistaPantallaRegistro().getBtnMinimizar())) {
			this.getVistaPantallaRegistro().getBtnMinimizar().setBackground(null);
		}
	}

	public PantallaRegistro getVistaPantallaRegistro() {
		return vistaPantallaRegistro;
	}

	public void setVistaPantallaRegistro(PantallaRegistro vistaPantallaRegistro) {
		this.vistaPantallaRegistro = vistaPantallaRegistro;
	}

	public ControladorPantallaLogin getControladorLogin() {
		return controladorLogin;
	}

	public void setControladorLogin(ControladorPantallaLogin controladorLogin) {
		this.controladorLogin = controladorLogin;
	}
	public UsuarioDAO getUsuarioDAO() {
		return usuarioDAO;
	}

	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public RankingDAO getRankingDAO() {
		return rankingDAO;
	}

	public void setRankingDAO(RankingDAO rankingDAO) {
		this.rankingDAO = rankingDAO;
	}

	public Ranking getRanking() {
		return ranking;
	}

	public void setRanking(Ranking ranking) {
		this.ranking = ranking;
	}

}
