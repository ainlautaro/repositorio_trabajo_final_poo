package controlador;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import vista.PanelInicio;
import vista.PantallaPrincipal;

public class ControladorPanelInicio implements MouseListener {
	
	private PanelInicio panelInicio;
	private ControladorPantallaJuego controladorPantallaJuego;
	private PantallaPrincipal pantallaContenedorPaneles;
	private ControladorModalRanking controladorMOD;
	
	public ControladorPanelInicio(PantallaPrincipal pantallaContenedorPaneles) {
		super();
		this.panelInicio = new PanelInicio(this);
		
		this.setPantallaContenedorPaneles(pantallaContenedorPaneles);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
		getPanelInicio().getBtnArte().setCursor(cursor);
		getPanelInicio().getBtnCine().setCursor(cursor);
		getPanelInicio().getBtnDeporte().setCursor(cursor);
		getPanelInicio().getBtnGeografia().setCursor(cursor);
		getPanelInicio().getBtnHistoria().setCursor(cursor);
		getPanelInicio().getBtnCiencia().setCursor(cursor);
		getPanelInicio().getBtnRandom().setCursor(cursor);
		getPanelInicio().getBtnRanking().setCursor(cursor);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource().equals(getPanelInicio().getBtnArte())) {
			
			controladorPantallaJuego = new ControladorPantallaJuego("Arte");
			this.pantallaContenedorPaneles.dispose();
			System.out.println("Se ingreso al modo de Arte");
			
		}
		
		if (e.getSource().equals(getPanelInicio().getBtnCine())) {
			
			this.pantallaContenedorPaneles.dispose();
			controladorPantallaJuego = new ControladorPantallaJuego("Entretenimiento");
			System.out.println("Se ingreso al modo de Entretenimiento");
			
		}
		
		if (e.getSource().equals(getPanelInicio().getBtnDeporte())) {
			
			controladorPantallaJuego = new ControladorPantallaJuego("Deportes");
			this.pantallaContenedorPaneles.dispose();
			System.out.println("Se ingreso al modo de Deportes");
			
		}
		
		if (e.getSource().equals(getPanelInicio().getBtnGeografia())) {
			
			controladorPantallaJuego = new ControladorPantallaJuego("Geografia");
			this.pantallaContenedorPaneles.dispose();
			System.out.println("Se ingreso al modo de Geografia");
			
		}
		
		if (e.getSource().equals(getPanelInicio().getBtnHistoria())) {
			
			controladorPantallaJuego = new ControladorPantallaJuego("Historia");
			this.pantallaContenedorPaneles.dispose();
			System.out.println("Se ingreso al modo de Historia");
			
		}
		
		if (e.getSource().equals(getPanelInicio().getBtnCiencia())) {
			
			controladorPantallaJuego = new ControladorPantallaJuego("Ciencia");
			this.pantallaContenedorPaneles.dispose();
			System.out.println("Se ingreso al modo de Ciencia");
			
		}
		
		if (e.getSource().equals(getPanelInicio().getBtnRandom())) {
			
			controladorPantallaJuego = new ControladorPantallaJuego("Random");
			this.pantallaContenedorPaneles.dispose();
			System.out.println("Se ingreso al modo de Random");
			
		}
		if(e.getSource().equals(getPanelInicio().getBtnRanking())) {
			controladorMOD = new ControladorModalRanking();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public PanelInicio getPanelInicio() {
		return panelInicio;
	}

	public void setPanelInicio(PanelInicio panelInicio) {
		this.panelInicio = panelInicio;
	}

	public ControladorPantallaJuego getControladorPantallaJuego() {
		return controladorPantallaJuego;
	}

	public void setControladorPantallaJuego(ControladorPantallaJuego controladorPantallaJuego) {
		this.controladorPantallaJuego = controladorPantallaJuego;
	}

	public PantallaPrincipal getPantallaContenedorPaneles() {
		return pantallaContenedorPaneles;
	}

	public void setPantallaContenedorPaneles(PantallaPrincipal pantallaContenedorPaneles) {
		this.pantallaContenedorPaneles = pantallaContenedorPaneles;
	}

	public ControladorModalRanking getControladorMOD() {
		return controladorMOD;
	}

	public void setControladorMOD(ControladorModalRanking controladorMOD) {
		this.controladorMOD = controladorMOD;
	}
}
