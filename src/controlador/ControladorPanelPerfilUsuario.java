 package controlador;
 
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.border.LineBorder;
import modelo.RankingDAO;
import modelo.Usuario;
import modelo.UsuarioDAO;
import modelo.UsuarioRanking;
import modelo.UsuarioSesion;
import vista.PanelPerfilUsuario;
import vista.PantallaPrincipal;



public class ControladorPanelPerfilUsuario implements MouseListener, ActionListener{
	
	private PanelPerfilUsuario panelPerfilUsuario;
	private UsuarioDAO usuarioDAO;
	private UsuarioSesion usuarioSesion;
	private ControladorPantallaPrincipal controladorPP;
	private ControladorPantallaLogin controladorPantallaLogin;
	private PantallaPrincipal pantallaContenedorPaneles;
	private Usuario usuario;
	private RankingDAO rkgDAO = new RankingDAO();


	public ControladorPanelPerfilUsuario(PantallaPrincipal pantallaContenedorPaneles) {
		this.panelPerfilUsuario = new PanelPerfilUsuario(this);
		this.setPantallaContenedorPaneles(pantallaContenedorPaneles);


		
		this.panelPerfilUsuario.setVisible(true);
		this.usuarioDAO =  new UsuarioDAO();
	}	
	
	public void actionPerformed(ActionEvent e) {
		//SELECCION QUE COMPRUEBA SI EL BOTON ES "ACTUALIZAR DATOS"
		if(e.getSource().equals(getPanelPerfilUsuario().getBtnActualizarDatos())) {

			if(UsuarioSesion.getInstance().getPassword().equals(panelPerfilUsuario.panelAviso())) {
			//Integer id_usuario = getUsuario().buscaId(UsuarioSesion.getInstance().getNombre());
			String nombre = getPanelPerfilUsuario().getTextFieldNombreUsuario().getText();
			
			String password = getPanelPerfilUsuario().getTextFieldPassword().getText();
			String confirmarPassword = getPanelPerfilUsuario().getTextFieldConfirmarPassword().getText();
			
			String email = getPanelPerfilUsuario().getTextFieldEmail().getText();
			String confirmarEmail = getPanelPerfilUsuario().getTextFieldConfirmarEmail().getText();
			
			if(nombre.equals("")) {
				this.getPanelPerfilUsuario().getTextFieldNombreUsuario().setBorder(new LineBorder(Color.RED));
			}else {
				this.getPanelPerfilUsuario().getTextFieldNombreUsuario().setBorder(new LineBorder(Color.GREEN));
			}
			
			if(password.equals("") || !password.equals(confirmarPassword)) {
				this.getPanelPerfilUsuario().getTextFieldPassword().setBorder(new LineBorder(Color.RED));
				this.getPanelPerfilUsuario().getTextFieldConfirmarPassword().setBorder(new LineBorder(Color.RED));
			}else {
				this.getPanelPerfilUsuario().getTextFieldPassword().setBorder(new LineBorder(Color.GREEN));
				this.getPanelPerfilUsuario().getTextFieldConfirmarPassword().setBorder(new LineBorder(Color.GREEN));
			}
			
			if(email.equals("") || !email.equals(confirmarEmail) ) {
				this.getPanelPerfilUsuario().getTextFieldEmail().setBorder(new LineBorder(Color.RED));
				this.getPanelPerfilUsuario().getTextFieldConfirmarEmail().setBorder(new LineBorder(Color.RED));
			}else {
				this.getPanelPerfilUsuario().getTextFieldEmail().setBorder(new LineBorder(Color.GREEN));
				this.getPanelPerfilUsuario().getTextFieldConfirmarEmail().setBorder(new LineBorder(Color.GREEN));			
			}
			
			if (email.equals(confirmarEmail) && !email.equals("") && password.equals(confirmarPassword) && !password.equals("") && !nombre.equals("")) {
					usuario = new Usuario(UsuarioSesion.getInstance().getId_usuario(), 
													   nombre, 
													   password, 
													   email, 
													   UsuarioSesion.getInstance().getFotoPerfil(), 
													   UsuarioSesion.getInstance().getAdmin());
			
				usuario.actualizaDatos(usuario);
				this.getPantallaContenedorPaneles().dispose();
				controladorPantallaLogin = new ControladorPantallaLogin();
			}
			}
		}

	}
	

	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
		getPanelPerfilUsuario().getBtnActualizarDatos().setCursor(cursor);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	public PanelPerfilUsuario getPanelPerfilUsuario() {
		return panelPerfilUsuario;
	}

	public void setPanelPerfilUsuario(PanelPerfilUsuario panelPerfilUsuario) {
		this.panelPerfilUsuario = panelPerfilUsuario;
	}

	public UsuarioDAO getUsuarioDAO() {
		return usuarioDAO;
	}

	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}

	public UsuarioSesion getUsuarioSesion() {
		return usuarioSesion;
	}

	public void setUsuarioSesion(UsuarioSesion usuarioSesion) {
		this.usuarioSesion = usuarioSesion;
	}

	public ControladorPantallaPrincipal getControladorPP() {
		return controladorPP;
	}

	public void setControladorPP(ControladorPantallaPrincipal controladorPP) {
		this.controladorPP = controladorPP;
	}

	public ControladorPantallaLogin getControladorPantallaLogin() {
		return controladorPantallaLogin;
	}

	public void setControladorPantallaLogin(ControladorPantallaLogin controladorPantallaLogin) {
		this.controladorPantallaLogin = controladorPantallaLogin;
	}

	public PantallaPrincipal getPantallaContenedorPaneles() {
		return pantallaContenedorPaneles;
	}

	public void setPantallaContenedorPaneles(PantallaPrincipal pantallaContenedorPaneles) {
		this.pantallaContenedorPaneles = pantallaContenedorPaneles;
	}

	public RankingDAO getRkgDAO() {
		return rkgDAO;
	}

	public void setRkgDAO(RankingDAO rkgDAO) {
		this.rkgDAO = rkgDAO;
	}

}
