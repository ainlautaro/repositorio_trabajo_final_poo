package controlador;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JOptionPane;

import modelo.Administrador;
import modelo.OpcionPregunta;
import modelo.OpcionPreguntaDAO;
import modelo.Pregunta;
import modelo.PreguntaDAO;
import modelo.Usuario;
import modelo.UsuarioSesion;
import vista.PanelSugerirPregunta;
import vista.PantallaPrincipal;

public class ControladorPanelSugerirPregunta implements MouseListener{

	private PantallaPrincipal pantallaPrincipal;
	private PanelSugerirPregunta panelSugerirPregunta;
	
	private PreguntaDAO pregDAO;
	private OpcionPreguntaDAO opcPregDAO;
	
	private UsuarioSesion usuarioSesion;
	private Usuario usuario;
	private Administrador admin;

	public ControladorPanelSugerirPregunta() {
		super();
		this.setPanelSugerirPregunta(new PanelSugerirPregunta(this));
		
		pregDAO = new PreguntaDAO();
		opcPregDAO = new OpcionPreguntaDAO();
		
		if (UsuarioSesion.getInstance().getAdmin()) {
			admin = new Administrador(UsuarioSesion.getInstance().getId_usuario(), UsuarioSesion.getInstance().getNombre(), 
    				  				  UsuarioSesion.getInstance().getPassword(),   UsuarioSesion.getInstance().getEmail(), 
    				  				  UsuarioSesion.getInstance().getFotoPerfil(), UsuarioSesion.getInstance().getAdmin());
			this.panelSugerirPregunta.getBtnListarPreguntas().setVisible(true);
		}else {
			usuario = new Usuario(UsuarioSesion.getInstance().getId_usuario(), UsuarioSesion.getInstance().getNombre(), 
				      			  UsuarioSesion.getInstance().getPassword(),   UsuarioSesion.getInstance().getEmail(), 
				      			  UsuarioSesion.getInstance().getFotoPerfil(), UsuarioSesion.getInstance().getAdmin());
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
		// Pregunta
		Pregunta pregunta = new Pregunta(getPregDAO().maxID()+1, 
										 getPanelSugerirPregunta().getTextFieldPreguntaSugerida().getText(), 
										 (String) getPanelSugerirPregunta().getCategoriaPregunta().getSelectedItem(), 
										 false);
		
		//----------------------------------------- Opciones Pregunta
		// Opcion 1
		OpcionPregunta opcion1 = new OpcionPregunta(getOpcPregDAO().maxID()+1, 
													getPregDAO().maxID()+1, 
													getPanelSugerirPregunta().getTextFieldOpcion1().getText(), 
													(getPanelSugerirPregunta().getCheckOP1().isSelected())?true:false);
		
		// Opcion 2
		OpcionPregunta opcion2 = new OpcionPregunta(getOpcPregDAO().maxID()+1, 
													getPregDAO().maxID()+1, 
													getPanelSugerirPregunta().getTextFieldOpcion2().getText(), 
													(getPanelSugerirPregunta().getCheckOP2().isSelected())?true:false);
		
		// Opcion 3
		OpcionPregunta opcion3 = new OpcionPregunta(getOpcPregDAO().maxID()+1, 
													getPregDAO().maxID()+1, 
													getPanelSugerirPregunta().getTextFieldOpcion3().getText(), 
													(getPanelSugerirPregunta().getCheckOP3().isSelected())?true:false);
		
		// Opcion 4
		OpcionPregunta opcion4 = new OpcionPregunta(getOpcPregDAO().maxID()+1, 
													getPregDAO().maxID()+1, 
													getPanelSugerirPregunta().getTextFieldOpcion4().getText(), 
													(getPanelSugerirPregunta().getCheckOP4().isSelected())?true:false);
		
		
		if (e.getSource().equals(this.getPanelSugerirPregunta().getBtnSugerirPregunta())) {
			
			if (UsuarioSesion.getInstance().getAdmin()) {
				getAdmin().cargarPregunta(pregunta, opcion1, opcion2, opcion3, opcion4);
			}else {
				getUsuario().cargarPregunta(pregunta, opcion1, opcion2, opcion3, opcion4);
			}
			
			resetarCampos();
		}
		
	}
	
	public void resetarCampos() {
		int resp = JOptionPane.showConfirmDialog(null, "Se ha sugerido la pregunta!", null, JOptionPane.CLOSED_OPTION);
		if (resp == 0) {
			this.getPanelSugerirPregunta().getTextFieldPreguntaSugerida().setText("");
			this.getPanelSugerirPregunta().getCategoriaPregunta().setSelectedIndex(0);
			this.getPanelSugerirPregunta().getTextFieldOpcion1().setText("");
			this.getPanelSugerirPregunta().getTextFieldOpcion2().setText("");
			this.getPanelSugerirPregunta().getTextFieldOpcion3().setText("");
			this.getPanelSugerirPregunta().getTextFieldOpcion4().setText("");
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
		this.getPanelSugerirPregunta().getBtnSugerirPregunta().setCursor(cursor);
		this.getPanelSugerirPregunta().getBtnListarPreguntas().setCursor(cursor);
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public PanelSugerirPregunta getPanelSugerirPregunta() {
		return panelSugerirPregunta;
	}

	public void setPanelSugerirPregunta(PanelSugerirPregunta panelSugerirPregunta) {
		this.panelSugerirPregunta = panelSugerirPregunta;
	}

	public PreguntaDAO getPregDAO() {
		return pregDAO;
	}

	public void setPregDAO(PreguntaDAO pregDAO) {
		this.pregDAO = pregDAO;
	}

	public OpcionPreguntaDAO getOpcPregDAO() {
		return opcPregDAO;
	}

	public void setOpcPregDAO(OpcionPreguntaDAO opcPregDAO) {
		this.opcPregDAO = opcPregDAO;
	}

	public UsuarioSesion getUsuarioSesion() {
		return usuarioSesion;
	}

	public void setUsuarioSesion(UsuarioSesion usuarioSesion) {
		this.usuarioSesion = usuarioSesion;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Administrador getAdmin() {
		return admin;
	}

	public void setAdmin(Administrador admin) {
		this.admin = admin;
	}
	public PantallaPrincipal getPantallaPrincipal() {
		return pantallaPrincipal;
	}

	public void setPantallaPrincipal(PantallaPrincipal pantallaPrincipal) {
		this.pantallaPrincipal = pantallaPrincipal;
	}
}
