package controlador;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import javax.swing.border.LineBorder;
import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerException;
import modelo.ExcepcionUsuarioNoRegistrado;
import modelo.Usuario;
import modelo.UsuarioSesion;
import vista.PantallaLogin;

public class ControladorPantallaLogin implements ActionListener, MouseListener {

	private PantallaLogin vistaPantallaLogin;
	private Usuario usuario;
	private UsuarioSesion usuarioSesion;

	private ControladorPantallaRegistro controladorPantallaRegistro;
	private ControladorPantallaPrincipal controladorPantallaContenedorPaneles;

	private BasicPlayer player = new BasicPlayer();
	
	public ControladorPantallaLogin() {
		super();
		this.vistaPantallaLogin = new PantallaLogin(this);
		this.usuarioSesion = new UsuarioSesion();

		this.vistaPantallaLogin.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource().equals(getVistaPantallaLogin().getBtnIniciarSesion())) {
			String nombre = getVistaPantallaLogin().getTextFieldUsuario().getText();
			String password = getVistaPantallaLogin().getTextFieldPassword().getText();

			if (nombre.isEmpty()) {
				this.getVistaPantallaLogin().getTextFieldUsuario().setBorder(new LineBorder(Color.RED));
				this.getVistaPantallaLogin().getLblNombreObligatorio().setVisible(true);
			} else {
				this.getVistaPantallaLogin().getTextFieldUsuario().setBorder(new LineBorder(Color.GREEN));
				this.getVistaPantallaLogin().getLblNombreObligatorio().setVisible(false);

			}

			if (password.isEmpty()) {
				this.getVistaPantallaLogin().getTextFieldPassword().setBorder(new LineBorder(Color.RED));
				this.getVistaPantallaLogin().getLblPasswordObligatorio().setVisible(true);
			} else {
				this.getVistaPantallaLogin().getTextFieldPassword().setBorder(new LineBorder(Color.GREEN));
				this.getVistaPantallaLogin().getLblPasswordObligatorio().setVisible(false);
			}

			if (!nombre.isEmpty() && !password.isEmpty()) {
				try {
					usuario = new Usuario(nombre, password);
					usuarioSesion.sesionIniciada(usuario);

				} catch (ExcepcionUsuarioNoRegistrado e1) {
					this.getVistaPantallaLogin().getTextFieldUsuario().setBorder(new LineBorder(Color.RED));
					this.getVistaPantallaLogin().getTextFieldPassword().setBorder(new LineBorder(Color.RED));
					System.out.println(e1.getMessage());
				}
			}

			if (usuario != null) {
				getVistaPantallaLogin().dispose();
				controladorPantallaContenedorPaneles = new ControladorPantallaPrincipal();
				
				try {
					player.open(new File("assets/sonidos/inicioSesion.mp3"));
					player.play(); 
				} catch (BasicPlayerException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}

		if (e.getSource().equals(getVistaPantallaLogin().getBtnRegistrarse())) {
			getVistaPantallaLogin().dispose();
			controladorPantallaRegistro = new ControladorPantallaRegistro();
		}

	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource().equals(this.getVistaPantallaLogin().getBtnCerrar())) {
			this.getVistaPantallaLogin().dispose();
		}
		
		if (e.getSource().equals(this.getVistaPantallaLogin().getBtnMinimizar())) {
			this.getVistaPantallaLogin().setExtendedState(Frame.ICONIFIED);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
		this.getVistaPantallaLogin().getBtnCerrar().setCursor(cursor);
		this.getVistaPantallaLogin().getBtnMinimizar().setCursor(cursor);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public PantallaLogin getVistaPantallaLogin() {
		return vistaPantallaLogin;
	}

	public void setVistaPantallaLogin(PantallaLogin vistaPantallaLogin) {
		this.vistaPantallaLogin = vistaPantallaLogin;
	}

	public ControladorPantallaRegistro getControladorPantallaRegistro() {
		return controladorPantallaRegistro;
	}

	public void setControladorPantallaRegistro(ControladorPantallaRegistro controladorPantallaRegistro) {
		this.controladorPantallaRegistro = controladorPantallaRegistro;
	}

	public ControladorPantallaPrincipal getControladorPantallaContenedorPaneles() {
		return controladorPantallaContenedorPaneles;
	}

	public void setControladorPantallaContenedorPaneles(
			ControladorPantallaPrincipal controladorPantallaContenedorPaneles) {
		this.controladorPantallaContenedorPaneles = controladorPantallaContenedorPaneles;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public UsuarioSesion getUsuarioSesion() {
		return usuarioSesion;
	}

	public void setUsuarioSesion(UsuarioSesion usuarioSesion) {
		this.usuarioSesion = usuarioSesion;
	}

}
